package com.sg.emoney.core.model.message;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class RegistrationRequest extends RestRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	@NotNull
	@NotEmpty
	private String name;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	@NotNull
	@NotEmpty
	private String email;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	@NotNull
	@NotEmpty
	private String phoneNo;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	@NotNull
	@NotEmpty
	private String birthOfDate;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	@NotNull
	@NotEmpty
	private String sex;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getBirthOfDate() {
		return birthOfDate;
	}
	public void setBirthOfDate(String birthOfDate) {
		this.birthOfDate = birthOfDate;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}	
	
}

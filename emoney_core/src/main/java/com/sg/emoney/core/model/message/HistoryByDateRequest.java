package com.sg.emoney.core.model.message;

import java.util.Date;

public class HistoryByDateRequest extends RestRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date dateFrom;
	private Date dateTo;
	/**
	 * size
	 */
	private int qty;
	/**
	 * page
	 */
	private int order;
	
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	

}

package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class TransferOnUsResponseInq extends RestResponse {

	/**
system
	 */
	private static final long serialVersionUID = 2514945108193431524L;
	private String type;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountFromName;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountTo;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountToName;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String amount;
	private String description;
	private String currency;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccountFromName() {
		return accountFromName;
	}
	public void setAccountFromName(String accountFromName) {
		this.accountFromName = accountFromName;
	}
	public String getAccountTo() {
		return accountTo;
	}
	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}
	public String getAccountToName() {
		return accountToName;
	}
	public void setAccountToName(String accountToName) {
		this.accountToName = accountToName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}

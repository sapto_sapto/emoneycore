package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class HoldingBalanceHistoryRequest extends HistoryRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String purposeId;

	public String getPurposeId() {
		return purposeId;
	}

	public void setPurposeId(String purposeId) {
		this.purposeId = purposeId;
	}
	
	
}

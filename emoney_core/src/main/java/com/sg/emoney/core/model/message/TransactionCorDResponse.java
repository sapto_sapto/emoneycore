package com.sg.emoney.core.model.message;

import com.sg.emoney.core.model.core.entity.Balance;

public class TransactionCorDResponse extends RestResponse {

	/**
system
	 */
	private static final long serialVersionUID = 1L;
	private Balance balance;
	private String accountName;
	public Balance getBalance() {
		return balance;
	}
	public void setBalance(Balance balance) {
		this.balance = balance;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
}

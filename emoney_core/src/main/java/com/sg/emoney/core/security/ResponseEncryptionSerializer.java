package com.sg.emoney.core.security;

import java.io.IOException;
import java.security.PublicKey;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.service.SecurityService;
import com.sg.emoney.core.service.SecurityServiceImpl;

public class ResponseEncryptionSerializer extends JsonSerializer<String> {

	private SecurityService securityService;
	
	public ResponseEncryptionSerializer() {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		securityService = new SecurityServiceImpl();
	}

	@Override
	public void serialize(String value, JsonGenerator jgen,
			SerializerProvider arg2) throws IOException,
			JsonProcessingException {
/*
		if(securityService != null){
			String plainText = value;
			String chiperText = "";
			
			if(plainText != null){
				try {
					PublicKey publicKey = securityService.createPublicKey(BaseConstant.PUBLIC_EXPONENT, BaseConstant.PUBLIC_MODULUS);
					chiperText = securityService.encrypt(plainText, publicKey);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			jgen.writeString(chiperText);
		} else {
			jgen.writeString(value);
		}*/
		
		jgen.writeString(value);
	}
}

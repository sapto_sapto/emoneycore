package com.sg.emoney.core.service;

import com.sg.emoney.core.model.core.entity.AccountPublicKey;

public interface AccountPublicKeyService {

	void save(AccountPublicKey accPublicKey);

	AccountPublicKey findByAccount(String accountNo);

}

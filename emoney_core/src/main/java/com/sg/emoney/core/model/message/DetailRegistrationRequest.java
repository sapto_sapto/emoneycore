package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class DetailRegistrationRequest extends RestRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String maritalStatus;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String job;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String religion;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String identityNo;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String identityType;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String motherMaidenName ;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String address;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String city;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String province;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String country;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String postalCode;
	
	private String photo;
	
	private String photoCardID;
	
	private String signature; 
	
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getIdentityType() {
		return identityType;
	}
	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}
	public String getMotherMaidenName() {
		return motherMaidenName;
	}
	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getPhotoCardID() {
		return photoCardID;
	}
	public void setPhotoCardID(String photoCardID) {
		this.photoCardID = photoCardID;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	
}

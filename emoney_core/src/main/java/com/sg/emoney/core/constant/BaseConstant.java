package com.sg.emoney.core.constant;

import java.text.SimpleDateFormat;


public class BaseConstant {
	public static final String SUCCESS_RESPONSE_CODE = "00";
	public static final String PUBLIC_KEY_MODULUS = "pubKeyMod";
	public static final String PUBLIC_KEY_EXPONENT = "pubKeyEx";
	public static final String PRIVATE_KEY = "privateKey";
	public static final String PRIVATE_KEY_EXPONENT = "privateKeyEx";
	
	public static final String PRIVATE_MODULUS = "a37add6f7bb76d435116b22631710e2865d7891cba670e56f1a4b715b2a5e8180218a6deb779b51337cdb37a73397d264dc11467008f3293952154b4d0deb03d";
	public static final String PRIVATE_EXPONENT = "46226be56c1a215203f3ccb1fdfc16aad62645b8816fbd0327e0167bc48cd0a8eba0e0ba814aa65d26a4aef49b67899d153ff207bce466150ef409e6d4f5b101";

	public static final String PUBLIC_MODULUS = "a37add6f7bb76d435116b22631710e2865d7891cba670e56f1a4b715b2a5e8180218a6deb779b51337cdb37a73397d264dc11467008f3293952154b4d0deb03d";
	public static final String PUBLIC_EXPONENT = "10001";

	public static final SimpleDateFormat DB_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");
	public static final SimpleDateFormat FULL_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	
	public static final String CURRENCY_ID = "IDR";
	public static final String ACCOUNT_REG_REFNO = "1";
	public static final String DEBET_CODE = "D";
	public static final String CREDIT_CODE = "C";
	
	public static String ACCOUNTNO_PREFIX = "800";
	public static int MAX_RANDOM = 9999999;
	public static int MIN_RANDOM = 1;
	public static String RANDOM_LENGTH = "7";
	public static String MERCHANT_CODE_DEFAULT = "DEFAULT";
	public static String TRX_CODE_DEFAULT = "000000";
	public static Object CHANNEL_BACKOFFICE = "BO";
	public static String REQUEST_CLASS = "OBJ_CLASS";
}

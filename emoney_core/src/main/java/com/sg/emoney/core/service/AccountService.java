package com.sg.emoney.core.service;

import java.security.NoSuchAlgorithmException;

import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.message.AccountInquiryRequest;
import com.sg.emoney.core.model.message.AccountInquiryResponse;
import com.sg.emoney.core.model.message.AccountUpdateRequest;
import com.sg.emoney.core.model.message.AccountUpdateResponse;
import com.sg.emoney.core.model.message.CorporateRegistrationRequest;
import com.sg.emoney.core.model.message.DecryptAccountRequest;
import com.sg.emoney.core.model.message.DecryptAccountResponse;
import com.sg.emoney.core.model.message.EmailUpdateRequest;
import com.sg.emoney.core.model.message.EmailUpdateResponse;
import com.sg.emoney.core.model.message.LevelUpdateRequest;
import com.sg.emoney.core.model.message.LevelUpdateResponse;
import com.sg.emoney.core.model.message.PhoneNoUpdateRequest;
import com.sg.emoney.core.model.message.PhoneNoUpdateResponse;
import com.sg.emoney.core.model.message.RegistrationRequest;
import com.sg.emoney.core.model.message.RegistrationResponse;
import com.sg.emoney.core.model.message.RestRequest;
import com.sg.emoney.core.model.message.RestResponse;

public interface AccountService {

	RegistrationResponse register(RegistrationRequest registrationRequest) throws NoSuchAlgorithmException;

	PhoneNoUpdateResponse updatePhoneNo(
			PhoneNoUpdateRequest phoneNoUpdateRequest);

	EmailUpdateResponse updateEmail(EmailUpdateRequest emailUpdateRequest);

	AccountUpdateResponse updateStatusAccount(
			AccountUpdateRequest accountUpdateRequest);

	Account findByAccountNo(String cif);

	AccountInquiryResponse inquiryAccount(
			AccountInquiryRequest accountInquiryRequest);

	DecryptAccountResponse decryptAccount(DecryptAccountRequest decryptAccountRequest);
	
	void save(Account acccount);

	LevelUpdateResponse updateLevel(LevelUpdateRequest levelUpdateRequest);

	AccountInquiryResponse inquiryAccountFull(
			AccountInquiryRequest accountInquiryRequest);

	RegistrationResponse corporateRegister(
			CorporateRegistrationRequest registrationRequest) throws NoSuchAlgorithmException;
	
	String getNameByAccountNo(String cif) throws Exception;
	
	RestResponse migrateData(RestRequest request);
}

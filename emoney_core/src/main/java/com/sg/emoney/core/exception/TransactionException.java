package com.sg.emoney.core.exception;

public class TransactionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransactionException() {
		super("Error transaction happens");
	}
	
	public TransactionException(String message) {
		super("Transaction Exception with RC : "+message);
	}
}

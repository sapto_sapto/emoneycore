package com.sg.emoney.core.model.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"code"})})
public class TrxCode extends BaseEntity {
	
	@Id
	@Pattern(regexp="[0-9]*")
	private String code;
	
	@Column
	private String name;
	
	@Column
	@NotNull
	@NotEmpty
	private String description;
	
	@Column
	@NotNull
	@NotEmpty
	private String status;
	
	public TrxCode() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public TrxCode(String code, String name, String description, String status) {
		super();
		this.code = code;
		this.name = name;
		this.description = description;
		this.status = status;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

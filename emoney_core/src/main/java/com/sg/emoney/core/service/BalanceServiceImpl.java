package com.sg.emoney.core.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.model.BalanceSpesifications;
import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.Balance;
import com.sg.emoney.core.model.core.repository.BalanceRepository;

@Service
public class BalanceServiceImpl implements BalanceService {
	
	@Autowired
	private BalanceRepository balanceRepository;
	
	@Autowired
	private AccountService accountService;
	
	@Value("${limit.credit.max}")
	private BigDecimal limitCreditMax;
	
	@Transactional(isolation= Isolation.SERIALIZABLE)
	@Override
	public void save(Balance balance) {
		balanceRepository.save(balance);
	}

	@Override
	public List<Balance> findByAccountWithPaging(int qty, int order, String accountNo) {
		final Account account = accountService.findByAccountNo(accountNo);
		Page<Balance> balancesPage = balanceRepository.findAll(BalanceSpesifications.balanceByAccountSpec(account),
				BalanceSpesifications.pageBalance(order, qty));
		return  balancesPage.getContent();
	}

	@Override
	public List<Balance> findByAccountWithPaging(int qty, int order,
			Date dateFrom, Date dateTo, String accountNo) {
		final Account account = accountService.findByAccountNo(accountNo);
		Page<Balance> balancesPage = balanceRepository.findAll(BalanceSpesifications.balanceBySubmitDate(dateFrom, dateTo, account),
				BalanceSpesifications.pageBalance(order, qty));
		return  balancesPage.getContent();
	}

	@Override
	public List<Balance> findAllbyRefNo(String referenceID) {
//		String date = BaseConstant.DB_DATE_FORMAT.format(new Date());
//		Date startDate = BaseConstant.FULL_DATE_FORMAT.parse(date +" 00:00:00");
//		Date endDate = BaseConstant.FULL_DATE_FORMAT.parse(date +" 23:59:59");
		return balanceRepository.findByRefNo(referenceID);
	}
	
	@Transactional(isolation= Isolation.SERIALIZABLE)
	@Override
	public void saveBatch(List<Balance> blncIterable) {
		balanceRepository.save(blncIterable);
	}

	@Override
	public boolean isMonthlyLimitCreditExceeded(String accountNo, String level,
			BigDecimal amount) {
		if(level.equals(Account.ACCOUNT_LEVEL.BASIC.toString()) || level.equals(Account.ACCOUNT_LEVEL.REGULAR.toString())){
			Calendar cal = Calendar.getInstance();
			Calendar thisMonth = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 1);
			BigDecimal creditInMonth = balanceRepository.getSumCreditAmountByAccountNoAndMonth(accountNo, thisMonth.getTime());
			if(creditInMonth == null){
				creditInMonth = new BigDecimal(0);
			}
			creditInMonth = creditInMonth.add(amount);
			if(creditInMonth.compareTo(limitCreditMax) > 0){
				return true;
			}
		}
		return false;
	}
	
	
}

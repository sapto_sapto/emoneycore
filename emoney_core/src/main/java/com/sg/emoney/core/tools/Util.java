package com.sg.emoney.core.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service(value="utilService")
public class Util {
	public static Date formatDateTime(String s){
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		if (s == null || s == "") {
			return null;
		}
		try {
			Date date;
			date = sf.parse(s);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static Date formatDate(String s){
		SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");
		if (s == null || s == "") {
			return null;
		}
		try {
			Date date;
			date = sf.parse(s);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getNowDate(){
		SimpleDateFormat sf = new SimpleDateFormat("ddMMyy");
		Date date = new Date();
		String now = sf.format(date);
		return now;
	}
	
	public static File convert(MultipartFile file) throws Exception
	{    
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}

	public static String getStringDate(Date date) {
		SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");
		if (date == null) {
			return null;
		}
		String dateStr;
		dateStr = sf.format(date);
		return dateStr;
	}
	
	public static ObjectMapper objectMapper = new ObjectMapper();
	
	public static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
}

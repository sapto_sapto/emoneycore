package com.sg.emoney.core.service;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.model.core.entity.AccountPublicKey;
import com.sg.emoney.core.model.secure.entity.AccountKey;

@Service("securityService")
public class SecurityServiceImpl implements SecurityService {
 
	/**
	 * String to hold name of algorithm.
	 */
	public static final String ALGORITHM = "RSA";

	/**
	 * Number of bytes to generate public and private key.
	 */
	public static final int BITS = 512;
	
	/**
	 * Number of hash iterations
	 */
	public static final int HASH_ITERATIONS = 10;

	
	/* (non-Javadoc)
	 * @see com.mlpt.siefe.vas.service.SecurityService#getKey()
	 */
	@Autowired
	private AccountPrivateKeyService accountPrivateKeyService;
	
	@Autowired
	private	AccountPublicKeyService accountPublicKeyService;
	
	public Map<String, Object> getKey() throws NoSuchAlgorithmException {
		KeyPairGenerator keyGen;

		keyGen = KeyPairGenerator.getInstance(ALGORITHM);
		keyGen.initialize(BITS);
		KeyPair key = keyGen.generateKeyPair();
		
		RSAPrivateCrtKey crtKey = (RSAPrivateCrtKey) key.getPrivate();
		System.out.println(crtKey);
		
		KeyFactory keyFac = KeyFactory.getInstance(ALGORITHM);
		RSAPublicKeySpec pbSpec;
		RSAPrivateKeySpec prSpec;
		Map<String, Object> keyMap = new HashMap<String, Object>();
		try {
			pbSpec = keyFac.getKeySpec(key.getPublic(), RSAPublicKeySpec.class);
			prSpec = keyFac.getKeySpec(key.getPrivate(), RSAPrivateKeySpec.class);
			System.out.println(prSpec);
			keyMap.put(BaseConstant.PUBLIC_KEY_MODULUS, pbSpec.getModulus()
					.toString(16)); // put modulus (hexaDecimal) in map
			keyMap.put(BaseConstant.PUBLIC_KEY_EXPONENT, pbSpec
					.getPublicExponent().toString(16)); // put public exponent
														// (hexaDecimal) in map
			keyMap.put(BaseConstant.PRIVATE_KEY, prSpec.getModulus()
					.toString(16));
			keyMap.put(BaseConstant.PRIVATE_KEY_EXPONENT, prSpec.getPrivateExponent().toString(16));
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return keyMap;
	}

	/* (non-Javadoc)
	 * @see com.mlpt.siefe.vas.service.SecurityService#decrypt(java.lang.String, java.security.PrivateKey)
	 */

	public String decrypt(String text, PrivateKey privateKey)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		// TODO Auto-generated method stub
		if(text == null){
			return text;
		}
		text = text.replace("\n", "").replace("\r", "").replace("\t", "").trim();

		// convert hexString to BigInteger
		BigInteger bigInteger = new BigInteger(text, 16);

		// convert BigInteger to unsigned byte
		byte[] cipherText = integerToOctetString(bigInteger, BITS / 8);

		byte[] decryptedText;

		Cipher cipher;
		cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		decryptedText = cipher.doFinal(cipherText);
		return (new String(decryptedText));
	}

	/**
	 * Encodes the given value as a unsigned Big Endian within an octet string
	 * of octetStringSize bytes.
	 * 
	 * @param i
	 *            the integer to encode
	 * @param octetStringSize
	 *            the number of octets in the octetString returned
	 * @return the encoding of i
	 * @throws IllegalArgumentException
	 *             if the given integer i is negative
	 * @throws IllegalArgumentException
	 *             if the octetStringSize is zero or lower
	 * @throws IllegalArgumentException
	 *             if the given BigInteger does not fit into octetStringSize
	 *             bytes
	 */
	public static byte[] integerToOctetString(final BigInteger i,
			final int octetStringSize) {

		// throws NullPointerException if i = null
		if (i.signum() < 0) {
			throw new IllegalArgumentException(
					"argument i should not be negative");
		}

		if (octetStringSize <= 0) {
			throw new IllegalArgumentException("octetStringSize argument ("
					+ octetStringSize
					+ ") should be higher than 0 to store any integer");
		}

		if (i.bitLength() > octetStringSize * Byte.SIZE) {
			throw new IllegalArgumentException("argument i (" + i
					+ ") does not fit into " + octetStringSize + " octets");
		}

		final byte[] signedEncoding = i.toByteArray();
		final int signedEncodingLength = signedEncoding.length;

		if (signedEncodingLength == octetStringSize) {
			return signedEncoding;
		}

		final byte[] unsignedEncoding = new byte[octetStringSize];
		if (signedEncoding[0] == (byte) 0x00) {
			// skip first padding byte to create a possitive signed encoding for
			// this number
			System.arraycopy(signedEncoding, 1, unsignedEncoding,
					octetStringSize - signedEncodingLength + 1,
					signedEncodingLength - 1);

		} else {
			System.arraycopy(signedEncoding, 0, unsignedEncoding,
					octetStringSize - signedEncodingLength,
					signedEncodingLength);
		}
		return unsignedEncoding;
	}
	/* (non-Javadoc)
	 * @see com.mlpt.siefe.vas.service.SecurityService#encrypt(java.lang.String, java.security.PublicKey)
	 */

	public String encrypt(String text, PublicKey publicKey) {
		if(text == null){
			return text;
		}
		byte[] cipherText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			// encrypt the plain text using the public key
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			cipherText = cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		BigInteger bi = new BigInteger(1, cipherText);
		return bi.toString(16);
	}

	/* (non-Javadoc)
	 * @see com.mlpt.siefe.vas.service.SecurityService#createPublicKey(java.lang.String, java.lang.String)
	 */

	public PublicKey createPublicKey(String publicExponent, String modulus) {
		KeyFactory keyFac = null;
		RSAPublicKey publicKey = null;
		modulus = modulus.replace("\n", "").replace("\r", "").replace("\t", "")
				.trim();
		publicExponent = publicExponent.replace("\n", "").replace("\r", "")
				.replace("\t", "").trim();
		try {
			keyFac = KeyFactory.getInstance(ALGORITHM);
			publicKey = (RSAPublicKey) keyFac
					.generatePublic(new RSAPublicKeySpec(new BigInteger(
							modulus, 16), new BigInteger(publicExponent, 16)));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return publicKey;
	}
	
	/* (non-Javadoc)
	 * @see com.mlpt.siefe.vas.service.SecurityService#createPrivateKey(java.lang.String, java.lang.String)
	 */

	public PrivateKey createPrivateKey(String exponent, String modulus) {
		KeyFactory keyFac = null;
		RSAPrivateKey privateKey = null;
		modulus = modulus.replace("\n", "").replace("\r", "").replace("\t", "").trim();
		exponent = exponent.replace("\n", "").replace("\r", "").replace("\t", "").trim();
		
		try {
			keyFac = KeyFactory.getInstance(ALGORITHM);
			KeySpec keySpec = new RSAPrivateKeySpec(new BigInteger(modulus, 16), new BigInteger(exponent, 16));
			privateKey = (RSAPrivateKey) keyFac.generatePrivate(keySpec);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return privateKey;
	}

	@Override
	public String encryptByAccount(String string, String accountNo) {
		AccountPublicKey accountPublicKey = accountPublicKeyService.findByAccount(accountNo);
		PublicKey publicKey = this.createPublicKey(accountPublicKey.getExponentKey(), accountPublicKey.getModulusKey());
		return this.encrypt(string, publicKey);
	}

	@Override
	public String decryptByAccount(String string, String accountNo) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		AccountKey accountKey = accountPrivateKeyService.findByAccountNo(accountNo);
		PrivateKey privateKey = this.createPrivateKey(accountKey.getExponentPrivate(), accountKey.getModulusPrivate());
		return this.decrypt(string, privateKey);
	}


	/*public String encryptAES(String plainText) {
		AesCipherService chiper = new AesCipherService();
		byte[] key = Base64.decode(keyAES);
		
		byte[] secretByte = CodecSupport.toBytes(plainText);
		ByteSource encrypted = chiper.encrypt(secretByte, key);
		return encrypted.toString();
	}

	public String decryptAES(String chiperText) {
		AesCipherService chiper = new AesCipherService();
		byte[] key = Base64.decode(keyAES);
		
		byte[] encryptedByte = Base64.decode(chiperText);
		ByteSource decrypted = chiper.decrypt(encryptedByte, key);
		String plainText = CodecSupport.toString(decrypted.getBytes());
		return plainText;
	}

	public String encryptSHA1(String plainText, String salt) {
		Hash hash = new Sha1Hash(plainText, salt, HASH_ITERATIONS);
		return hash.toBase64();
	}*/
	
}

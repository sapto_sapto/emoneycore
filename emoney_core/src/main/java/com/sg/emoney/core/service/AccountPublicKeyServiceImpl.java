package com.sg.emoney.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.model.core.entity.AccountPublicKey;
import com.sg.emoney.core.model.core.repository.AccountPublicKeyRepository;

@Service
public class AccountPublicKeyServiceImpl implements AccountPublicKeyService {
	
	@Autowired
	private AccountPublicKeyRepository accountPublicKeyRepository;
	
	@Transactional("prodTransactionManager")
	@Override
	public void save(AccountPublicKey accPublicKey) {
		accountPublicKeyRepository.save(accPublicKey);
	}

	@Transactional("prodTransactionManager")
	@Override
	public AccountPublicKey findByAccount(String accountNo) {
		return accountPublicKeyRepository.findByAccount(accountNo);
	}

}

package com.sg.emoney.core.model.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.AuditEntity;

@Repository
public interface AuditEntityRepository extends CrudRepository<AuditEntity, Integer> {

}

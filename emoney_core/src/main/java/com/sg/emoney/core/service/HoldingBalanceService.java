package com.sg.emoney.core.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.sg.emoney.core.model.core.entity.HoldingBalance;

public interface HoldingBalanceService {

	BigDecimal getSumHoldingBalance(String accountNo);

	void save(HoldingBalance holdingBalance);

	void delete(HoldingBalance holdingBalance);

	HoldingBalance findbyRefNo(String referenceID, String accountNo);

	BigDecimal getSumHoldingBalance(String cif, String purposeId);

	List<HoldingBalance> findAll(String cif, String purposeId);

	void saveBatch(Iterable<HoldingBalance> saveHoldBalances);

	List<HoldingBalance> findAll(String cif, String purposeId, String status);

	List<HoldingBalance> findByAccountWithPaging(int qty, int order, String cif, String purposeId);

	List<HoldingBalance> findByAccountWithPaging(int qty, int order,
			Date dateFrom, Date dateTo, String cif, String purposeId);

}

package com.sg.emoney.core.model.core.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.HoldingBalance;

@Repository
public interface HoldingBalanceRepository extends JpaRepository<HoldingBalance, String>, JpaSpecificationExecutor<HoldingBalance>{
	@Query("SELECT SUM(hb.debetAmount) as total FROM HoldingBalance hb WHERE hb.account.accountNo = :accountNo AND hb.status = 'HOLD'")
	BigDecimal getSumHoldingBalance(@Param(value = "accountNo") String accountNo);

	@Query("SELECT hb FROM HoldingBalance hb WHERE hb.refnum = :refnum AND hb.status != 'CANCELED'"
			+ " AND hb.account.accountNo = :accountNo")
	HoldingBalance findByRefNo(@Param(value = "refnum") String refnum, @Param(value = "accountNo") String accountNo);

	@Query("SELECT SUM(hb.debetAmount) as total FROM HoldingBalance hb WHERE hb.account.accountNo = :accountNo "
			+ "AND hb.purposeId = :purposeId AND hb.status = 'HOLD'")
	BigDecimal getSumHoldingBalance(@Param(value = "accountNo")String accountNo, @Param(value = "purposeId") String purposeId);

	@Query("SELECT hb FROM HoldingBalance hb WHERE hb.purposeId = :purposeId AND hb.status = 'HOLD'"
			+ " AND hb.account.accountNo = :accountNo")
	List<HoldingBalance> findAll(@Param(value = "accountNo") String accountNo, @Param(value = "purposeId") String purposeId);

	@Query("SELECT hb FROM HoldingBalance hb WHERE hb.purposeId = :purposeId AND hb.status = :status"
			+ " AND hb.account.accountNo = :accountNo")
	List<HoldingBalance> findAll(@Param(value = "accountNo") String accountNo, @Param(value = "purposeId") String purposeId,
			@Param(value = "status") String status);

}

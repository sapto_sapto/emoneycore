package com.sg.emoney.core.security;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.service.SecurityService;
import com.sg.emoney.core.service.SecurityServiceImpl;

public class RequestDecryptionDeserializer extends JsonDeserializer<String> {

	private SecurityService securityService;
	
	public RequestDecryptionDeserializer() {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		securityService = new SecurityServiceImpl();

	}
	
	@Override
	public String deserialize(JsonParser jp, DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		String plainText = "";
		/*String chiperText = jp.getValueAsString();
		if(securityService != null){
			if(chiperText != null){
				try {
					PrivateKey privateKey = securityService.createPrivateKey(BaseConstant.PRIVATE_EXPONENT, BaseConstant.PRIVATE_MODULUS);
					plainText = securityService.decrypt(chiperText, privateKey);
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalBlockSizeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			plainText = chiperText;
		}
		
		return plainText;*/
		return  jp.getValueAsString();
	}
	
}

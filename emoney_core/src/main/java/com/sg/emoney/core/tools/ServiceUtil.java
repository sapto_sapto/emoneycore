package com.sg.emoney.core.tools;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.tomcat.util.codec.binary.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.emoney.core.constant.BaseConstant;


public class ServiceUtil {
	
	public static String extractStringURL(String value){
		String[] data = value.split("/");
		String response = "bo" ;
		for (int i = 0; i < data.length; i++) {
			String string = data[i];
			response+=string;
		}
		return response.split(";")[0];
	}
	
	public static String formatDoubleToAmount(String amount){
		return amount.replaceAll("\\.0*(?!\\w)", "");
	}
	
	public static String formatAmount(String amount){
		DecimalFormat df = new DecimalFormat("#,000");
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		amount = df.format( Double.parseDouble(amount));
		return amount;
	}
	
	public static String formatAmountISO(String amount){
		return amount.replaceFirst("^0+(?!0.$)", "");
	}
	
	public static String getAccountAlias(String name, String accountNo){
		int accLength = accountNo.length();
		return name + accountNo.substring(accLength-4);
	}

	public static String getPcode(String accountNo, String pcode){
		String accountType = accountNo.substring(0,1);
		String pcodeType = "00";
		
		if(accountType.equals("S") || accountType.equals("W")){
			pcodeType = "10";
		}else if(accountType.equals("D") || accountType.equals("E")){
			pcodeType = "20";
		}
		
		return pcode.substring(0, 2) + pcodeType + pcode.substring(4);
	}
	
	public static List<String> getAllFields(Class<?> clazz){
		List<String> list = new ArrayList<String>();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			list.add(field.getName());
		}
		
		//check superclass
		while((clazz = clazz.getSuperclass())!=null){
			fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				list.add(field.getName());
			}
		}
		
		return list;
	}
	
	public static Field findField(Class<?> clazz, String fieldName) throws NoSuchFieldException {
		Class<?> current = clazz;
		do {
			try {
				return current.getDeclaredField(fieldName);
			} catch (Exception e) {
			}
		} while ((current = current.getSuperclass()) != null);
		throw new NoSuchFieldException();
	}
	
	public static void cloneObject(Object source, Object destination){
		Class<?> clazzSrc = source.getClass();
		Class<?> clazzDest = destination.getClass();
		
		List<String> listFieldDest = getAllFields(clazzDest);
		for (String string : listFieldDest) {
			try {
				Field fieldDest = findField(clazzDest, string);
				Field fieldSrc = findField(clazzSrc, string);
				
				if(fieldSrc != null){
					try {
						fieldSrc.setAccessible(true);
						fieldDest.setAccessible(true);
						fieldDest.set(destination, fieldSrc.get(source));
					} catch (IllegalArgumentException e) {
						
					} catch (IllegalAccessException e) {
						
					}
				}
			} catch (NoSuchFieldException e) {

			}
		}
	}
	
	public static String paddedMPin(String mpin, String charPadded, boolean after){
		
		int length = mpin.length();
		if(after){
			for(int i=16;i>length;i--){
				mpin += charPadded;
			}
		}else{
			for(int i=16;i>length;i--){
				mpin = charPadded + mpin;
			}
		}
		
		return mpin;
	}
	
	public static synchronized String generateReffNum(String prefix)
	{
		String reffnum = prefix + System.currentTimeMillis();
		return reffnum;
	}
	
	/**
	 * Returns a psuedo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimim value
	 * @param max Maximim value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randInt(int min, int max) {

	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	public static synchronized String generateAccNo() {
		int max = BaseConstant.MAX_RANDOM;
		int min = BaseConstant.MIN_RANDOM;
	    int randomNum = randInt(min, max);
	    String rdm = BaseConstant.ACCOUNTNO_PREFIX + String.format ("%0"+BaseConstant.RANDOM_LENGTH+"d", randomNum);
	    return rdm;
	}
	
	public static String byteToString(byte[] data){
		if(data!=null){
			return new String(Base64.encodeBase64(data));
		}else{
			return null;
		}
	}
	
	public static byte[] stringToByte(String data){
		if(data!=null){
			byte[] dt = Base64.decodeBase64(data);
			return dt;
		}else{
			return null;
		}
	}
	
	public static String getValueFromJson(String json, String column) {
		JsonNode node;
		try {
			node = new ObjectMapper().readTree(json);
			String value = node.get(column).asText();
			return value;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
//	public static void main(String args[]){
//		System.out.println(generateAccNo());
//	}
}

package com.sg.emoney.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@Order(1)
public class RestSecurityConfig extends WebSecurityConfigurerAdapter {

	// two service below added future needs, accomodate security
	@Autowired
	private RestAuthenticationEntryPoint authenticationEntryPoint;
	@Autowired
	private RestAccessDeniedHandler restAccessDeniedHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//future needs as mentioned above
		/*http.antMatcher("/CMS/**").authorizeRequests().anyRequest()
				.authenticated().and().exceptionHandling()
				.authenticationEntryPoint(authenticationEntryPoint)
				.accessDeniedHandler(restAccessDeniedHandler).and().csrf()
				.disable();*/
		 http
	      .csrf().disable()
	      .authorizeRequests()
	        .antMatchers(HttpMethod.POST, "/api/**").authenticated()
	        .antMatchers(HttpMethod.GET, "/api/**").authenticated()
	        .antMatchers(HttpMethod.PUT, "/api/**").authenticated()
	        .antMatchers(HttpMethod.DELETE, "/api/**").authenticated()
	        .antMatchers(HttpMethod.POST, "/conf/**").authenticated()
	        .antMatchers(HttpMethod.GET, "/conf/**").authenticated()
	        .antMatchers(HttpMethod.PUT, "/conf/**").authenticated()
	        .antMatchers(HttpMethod.DELETE, "/conf/**").authenticated()
	        .anyRequest().permitAll()
	        .and()
	      .httpBasic().and()
	      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}
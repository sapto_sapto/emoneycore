package com.sg.emoney.core.model;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.Balance;
import com.sg.emoney.core.model.core.entity.Balance_;
import com.sg.emoney.core.model.core.entity.HoldingBalance;
import com.sg.emoney.core.model.core.entity.HoldingBalance_;
import com.sg.emoney.core.model.core.entity.Balance.BALANCE_STATUS;
import com.sg.emoney.core.model.core.entity.HoldingBalance.BALANCEHOLD_STATUS;

public class BalanceSpesifications {
	public static Specification<Balance> balanceByAccountSpec(final Account account) {
		return new Specification<Balance>() {
			@Override
			public Predicate toPredicate(Root<Balance> root,
					CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Path<Account> pathAccount = root.get(Balance_.account);
				Predicate equalAccount = cb.equal(pathAccount, account);
				Path<String> pathStatus = root.get(Balance_.status);
				Predicate nequalStatus = cb.notEqual(pathStatus, BALANCE_STATUS.REVERSAL.toString());
				Predicate joinPredicate = cb.and(equalAccount,nequalStatus);
								
				return joinPredicate;
			}
		};
	}

	public static Specification<Balance> balanceBySubmitDate(final Date dateFrom, 
            final Date dateTo, final Account account) {
		return new Specification<Balance>() {
			@Override
			public Predicate toPredicate(Root<Balance> root,
					CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Path<Account> path = root.get(Balance_.account);
				Predicate equalAccount = cb.equal(path, account);
				Path<Date> datePath = root.get(Balance_.createDate);
				Predicate dateBetween = cb.between(datePath, dateFrom, dateTo);
				Path<String> pathStatus = root.get(Balance_.status);
				Predicate nequalStatus = cb.notEqual(pathStatus, BALANCE_STATUS.REVERSAL.toString());
				Predicate joinPredicate = cb.and(equalAccount,dateBetween,nequalStatus);
				return joinPredicate;
			}
		};
	}
	
	public static Specification<HoldingBalance> holdingBalanceByAccountSpec(final Account account, final String purposeId) {
		return new Specification<HoldingBalance>() {
			@Override
			public Predicate toPredicate(Root<HoldingBalance> root,
					CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Path<Account> pathAccount = root.get(HoldingBalance_.account);
				Predicate equalAccount = cb.equal(pathAccount, account);
				Path<String> pathStatus = root.get(HoldingBalance_.status);
				Predicate nequalStatus = cb.notEqual(pathStatus, BALANCEHOLD_STATUS.CANCELED.toString());
				Path<String> pathPurpose = root.get(HoldingBalance_.purposeId);
				Predicate equalPurpose = cb.equal(pathPurpose, purposeId);
				Predicate joinPredicate = cb.and(equalAccount,nequalStatus,equalPurpose);
				return joinPredicate;
			}
		};
	}

	public static Specification<HoldingBalance> holdingBalanceBySubmitDate(final Date dateFrom, 
            final Date dateTo, final Account account, final String purposeId) {
		return new Specification<HoldingBalance>() {
			@Override
			public Predicate toPredicate(Root<HoldingBalance> root,
					CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Path<Account> path = root.get(HoldingBalance_.account);
				Predicate equalAccount = cb.equal(path, account);
				Path<Date> datePath = root.get(HoldingBalance_.createDate);
				Predicate dateBetween = cb.between(datePath, dateFrom, dateTo);
				Path<String> pathStatus = root.get(HoldingBalance_.status);
				Predicate nequalStatus = cb.notEqual(pathStatus, BALANCEHOLD_STATUS.CANCELED.toString());
				Path<String> pathPurpose = root.get(HoldingBalance_.purposeId);
				Predicate equalPurpose = cb.equal(pathPurpose, purposeId);
				Predicate joinPredicate = cb.and(equalAccount,dateBetween,nequalStatus,equalPurpose);
				return joinPredicate;
			}
		};
	}

	public static PageRequest pageBalance(int order, int qty) {
		return new PageRequest(order, qty, Direction.DESC, "createDate");
	}
}

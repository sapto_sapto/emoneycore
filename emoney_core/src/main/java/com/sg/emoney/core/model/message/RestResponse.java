package com.sg.emoney.core.model.message;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class RestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	protected String cif;
	protected String responseCode;
	protected String message;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	protected String referenceID;
	
	
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	
	
}

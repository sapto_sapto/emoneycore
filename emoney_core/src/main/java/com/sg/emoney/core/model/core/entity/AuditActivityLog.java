package com.sg.emoney.core.model.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class AuditActivityLog extends BaseEntity{
	private Integer id;
	private String requestLog;
	private String responseLog;
	private String errorMessage;
	private String methodName;
	private String referenceID;
	
	public AuditActivityLog() {
		super();
	}
	
	public AuditActivityLog(String requestLog, String responseLog) {
		super();
		this.requestLog = requestLog;
		this.responseLog = responseLog;
	}

	public AuditActivityLog(String requestLog, String responseLog,
			String errorMessage) {
		super();
		this.requestLog = requestLog;
		this.responseLog = responseLog;
		this.errorMessage = errorMessage;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Lob
	@Column(length=5000000,columnDefinition="longtext")
	public String getRequestLog() {
		return requestLog;
	}
	public void setRequestLog(String requestLog) {
		this.requestLog = requestLog;
	}
	@Lob
	@Column(length=5000000,columnDefinition="longtext")
	public String getResponseLog() {
		return responseLog;
	}
	public void setResponseLog(String responseLog) {
		this.responseLog = responseLog;
	}
	@Lob
	@Column(length=5000000,columnDefinition="longtext")
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Column
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	@Column
	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	
	
}

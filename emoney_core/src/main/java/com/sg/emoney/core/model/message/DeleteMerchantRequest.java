package com.sg.emoney.core.model.message;

import java.util.List;

public class DeleteMerchantRequest extends ConfRestRequest {

	/**
system
	 */
	private static final long serialVersionUID = 3163866679398860075L;

	private List<String> codes;

	public List<String> getCodes() {
		return codes;
	}

	public void setCodes(List<String> codes) {
		this.codes = codes;
	}
	
	
}

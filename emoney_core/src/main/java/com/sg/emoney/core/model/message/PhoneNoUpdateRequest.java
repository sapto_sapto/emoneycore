package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class PhoneNoUpdateRequest extends RestRequest {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String phoneNoRequest;

	public String getPhoneNoRequest() {
		return phoneNoRequest;
	}

	public void setPhoneNoRequest(String phoneNoRequest) {
		this.phoneNoRequest = phoneNoRequest;
	}
	
	
}

package com.sg.emoney.core.model.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"code"})})
public class Merchant extends BaseEntity {
	
	@Id
	@Pattern(regexp="[a-zA-Z0-9]*")
	private String code;
	
	@Column
	private String name;
	
	@Column
	@NotNull
	@NotEmpty
	private String accountSource;
	
	@Column
	@NotNull
	@NotEmpty
	private String status;
	
	public static enum MERCHANT_STATUS{ACTIVE, INACTIVE};
	
	public Merchant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Merchant(String code, String name, String accountSource,
			String status) {
		super();
		this.code = code;
		this.name = name;
		this.accountSource = accountSource;
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAccountSource() {
		return accountSource;
	}
	public void setAccountSource(String accountSource) {
		this.accountSource = accountSource;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

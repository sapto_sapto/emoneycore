package com.sg.emoney.core.model.core.entity;

import java.math.BigDecimal;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.Balance;

@StaticMetamodel( Balance.class )
public class Balance_ extends BaseEntity_{
	public static volatile SingularAttribute<Balance, String> id;
	public static volatile SingularAttribute<Balance, BigDecimal> oldBalance;
	public static volatile SingularAttribute<Balance, String> oldCurrency;
	public static volatile SingularAttribute<Balance, BigDecimal> currentBalance;
	public static volatile SingularAttribute<Balance, String> currentCurrency;
	public static volatile SingularAttribute<Balance, BigDecimal> creditAmount;
	public static volatile SingularAttribute<Balance, String> creditCurrency;
	public static volatile SingularAttribute<Balance, BigDecimal> debetAmount;
	public static volatile SingularAttribute<Balance, String> debetCurrency;
	public static volatile SingularAttribute<Balance, String> status;
	public static volatile SingularAttribute<Balance, String> refnum;
	public static volatile SingularAttribute<Balance, String> description;
	public static volatile SingularAttribute<Balance, Account> account;
	public static volatile SingularAttribute<Balance, Merchant> merchant;
	public static volatile SingularAttribute<Balance, TrxCode> trxCode;
}

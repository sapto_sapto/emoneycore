package com.sg.emoney.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.model.secure.entity.AccountKey;
import com.sg.emoney.core.model.secure.repository.AccountPrivateKeyRepository;

@Service
public class AccountPrivateKeyServiceImpl implements AccountPrivateKeyService {
	@Autowired
	private AccountPrivateKeyRepository accountPrivateKeyRepository;
	
	@Transactional("secondaryTransactionManager")
	@Override
	public void save(AccountKey accPrivateKey) {
		accountPrivateKeyRepository.save(accPrivateKey);
	}

	@Transactional("secondaryTransactionManager")
	@Override
	public AccountKey findByAccountNo(String accountNo) {
		return accountPrivateKeyRepository.findByAccountNo(accountNo);
	}

}

package com.sg.emoney.core.service;

import com.sg.emoney.core.model.message.AddTrxCodeRequest;
import com.sg.emoney.core.model.message.AddTrxCodeResponse;
import com.sg.emoney.core.model.message.DeleteTrxCodeRequest;
import com.sg.emoney.core.model.message.DeleteTrxCodeResponse;

public interface TrxCodeService {

	AddTrxCodeResponse saveTrxCode(AddTrxCodeRequest addTrxCodeRequest);

	DeleteTrxCodeResponse deleteTrxCode(
			DeleteTrxCodeRequest deleteTrxCodeRequest);

}

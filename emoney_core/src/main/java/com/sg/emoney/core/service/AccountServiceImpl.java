package com.sg.emoney.core.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.constant.FrontControllerConstant;
import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.AccountPublicKey;
import com.sg.emoney.core.model.core.entity.Account.ACCOUNT_LEVEL;
import com.sg.emoney.core.model.core.entity.Account.ACCOUNT_STATUS;
import com.sg.emoney.core.model.core.repository.AccountRepository;
import com.sg.emoney.core.model.message.AccountInquiryRequest;
import com.sg.emoney.core.model.message.AccountInquiryResponse;
import com.sg.emoney.core.model.message.AccountUpdateRequest;
import com.sg.emoney.core.model.message.AccountUpdateResponse;
import com.sg.emoney.core.model.message.CorporateRegistrationRequest;
import com.sg.emoney.core.model.message.DecryptAccountRequest;
import com.sg.emoney.core.model.message.DecryptAccountResponse;
import com.sg.emoney.core.model.message.EmailUpdateRequest;
import com.sg.emoney.core.model.message.EmailUpdateResponse;
import com.sg.emoney.core.model.message.LevelUpdateRequest;
import com.sg.emoney.core.model.message.LevelUpdateResponse;
import com.sg.emoney.core.model.message.PhoneNoUpdateRequest;
import com.sg.emoney.core.model.message.PhoneNoUpdateResponse;
import com.sg.emoney.core.model.message.RegistrationRequest;
import com.sg.emoney.core.model.message.RegistrationResponse;
import com.sg.emoney.core.model.message.RestRequest;
import com.sg.emoney.core.model.message.RestResponse;
import com.sg.emoney.core.model.secure.entity.AccountKey;
import com.sg.emoney.core.tools.ServiceUtil;

@Service
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private AccountPrivateKeyService accountPrivateKeyService;
	
	@Autowired
	private	AccountPublicKeyService accountPublicKeyService;
	
	@Autowired
	private SecurityService securityService;
	
	@Autowired
	private HoldingBalanceService holdingBalanceService;
	
	@Transactional(readOnly = false)
	public synchronized RegistrationResponse register(RegistrationRequest registrationRequest) throws NoSuchAlgorithmException {
		RegistrationResponse registrationResponse = new RegistrationResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// generating accountNo
		Account accountReg = new Account();
		String accountNo = this.getNewAccountNo();
		accountReg.setAccountNo(accountNo);
		// generate RSA Key		
		Map<String, Object> key = securityService.getKey();
		String modulusPublicKey = (String) key.get( BaseConstant.PUBLIC_KEY_MODULUS);
		String exponentPublicKey = (String) key.get( BaseConstant.PUBLIC_KEY_EXPONENT);
		String modulusPrivateKey = (String) key.get( BaseConstant.PRIVATE_KEY);
		String exponentPrivateKey = (String) key.get( BaseConstant.PRIVATE_KEY_EXPONENT);
		
		PublicKey publicKey = securityService.createPublicKey(exponentPublicKey, modulusPublicKey);
		
		accountReg.setName(securityService.encrypt(registrationRequest.getName(), publicKey));
		accountReg.setPhoneNo(securityService.encrypt(registrationRequest.getPhoneNo(), publicKey));
		accountReg.setBod(securityService.encrypt(registrationRequest.getBirthOfDate(), publicKey));
		accountReg.setEmail(securityService.encrypt(registrationRequest.getEmail(), publicKey));
		accountReg.setSex(securityService.encrypt(registrationRequest.getSex(), publicKey));
		accountReg.setBalance(new BigDecimal(0));
		accountReg.setLevel(ACCOUNT_LEVEL.BASIC.toString());
		accountReg.setStatus(ACCOUNT_STATUS.ACTIVE.toString());
		accountReg.setCurrency(BaseConstant.CURRENCY_ID);
		accountReg.setCreateDate(new Date());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
	    accountReg.setCreateBy(username);
	    
		AccountPublicKey accPublicKey = new AccountPublicKey(
				accountReg,	modulusPublicKey, exponentPublicKey);
		AccountKey accPrivateKey = new AccountKey(
				accountNo, modulusPrivateKey, exponentPrivateKey);
		
		accountPublicKeyService.save(accPublicKey);
		accountPrivateKeyService.save(accPrivateKey);
	
		registrationResponse.setCif(accountNo);
		registrationResponse.setResponseCode(rc);
		registrationResponse.setReferenceID(registrationRequest.getReferenceID());
		return registrationResponse;
	}

	@Transactional("prodTransactionManager")
	private String getNewAccountNo() {
		String newAccountNo = new String();
		// generate till accountNo is not exist
		while(true){
			newAccountNo = ServiceUtil.generateAccNo();
			Account acc = accountRepository.findOne(newAccountNo);
			if (acc == null){
				break;
			}
		}
		return newAccountNo;
	}

	@Transactional(readOnly = true)
	public PhoneNoUpdateResponse updatePhoneNo(
			PhoneNoUpdateRequest phoneNoUpdateRequest) {
		PhoneNoUpdateResponse phoneNoUpdateResponse = new PhoneNoUpdateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(phoneNoUpdateRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = this.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			Account accCurr = this.findByAccountNo(phoneNoUpdateRequest.getCif());
			accCurr.setPhoneNo(securityService.encryptByAccount(phoneNoUpdateRequest.getPhoneNoRequest(), phoneNoUpdateRequest.getCif()));
			this.save(accCurr);
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		
		phoneNoUpdateResponse.setCif(phoneNoUpdateRequest.getCif());
		phoneNoUpdateResponse.setReferenceID(phoneNoUpdateRequest.getReferenceID());
		phoneNoUpdateResponse.setResponseCode(rc);
		return phoneNoUpdateResponse;
	}

	@Transactional(readOnly = true)
	public EmailUpdateResponse updateEmail(EmailUpdateRequest emailUpdateRequest) {
		EmailUpdateResponse emailUpdateResponse = new EmailUpdateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(emailUpdateRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = this.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			Account accCurr = this.findByAccountNo(emailUpdateRequest.getCif());
			accCurr.setEmail(securityService.encryptByAccount(emailUpdateRequest.getEmail(), emailUpdateRequest.getCif()));
			this.save(accCurr);
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		
		emailUpdateResponse.setCif(emailUpdateRequest.getCif());
		emailUpdateResponse.setReferenceID(emailUpdateRequest.getReferenceID());
		emailUpdateResponse.setResponseCode(rc);
		return emailUpdateResponse;
	}

	@Transactional(readOnly = true)
	public AccountUpdateResponse updateStatusAccount(
			AccountUpdateRequest accountUpdateRequest) {
		AccountUpdateResponse accountUpdateResponse = new AccountUpdateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		
		Account accCurr = this.findByAccountNo(accountUpdateRequest.getCif());
		if (accCurr != null){
			if (accountUpdateRequest.getStatus().equals(ACCOUNT_STATUS.ACTIVE.toString())){
				rc = this.activateAccount(accCurr);
			} else if (accountUpdateRequest.getStatus().equals(ACCOUNT_STATUS.BLOCKED.toString())){
				rc = this.blockAccount(accCurr);
			} else if (accountUpdateRequest.getStatus().equals(ACCOUNT_STATUS.CLOSED.toString())){
				rc = this.closeAccount(accCurr);
			}  else {
				rc = FrontControllerConstant.INVALID_ARGUMENT;
			}
		} else {
			rc = FrontControllerConstant.RC_INVALID_ACCOUNT;
		}
		
		accountUpdateResponse.setCif(accountUpdateRequest.getCif());
		accountUpdateResponse.setReferenceID(accountUpdateRequest.getReferenceID());
		accountUpdateResponse.setResponseCode(rc);
		return accountUpdateResponse;
	}

	private String closeAccount(Account accCurr) {
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (accCurr.getStatus().equals(ACCOUNT_STATUS.ACTIVE.toString())){
			if (accCurr.getBalance().compareTo(BigDecimal.ZERO) == 0){
				accCurr.setStatus(ACCOUNT_STATUS.CLOSED.toString());
				this.save(accCurr);
			} else {
				rc = FrontControllerConstant.RC_BALANCE_EXIST;
			}
		} else if (accCurr.getStatus().equals(ACCOUNT_STATUS.BLOCKED.toString())){
			rc = FrontControllerConstant.RC_BLOCKED_ACCOUNT;
		} else if (accCurr.getStatus().equals(ACCOUNT_STATUS.CLOSED.toString())){
			rc = FrontControllerConstant.RC_CLOSED_ACCOUNT;
		}
		return rc;
	}

	private String blockAccount(Account accCurr) {
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (accCurr.getStatus().equals(ACCOUNT_STATUS.CLOSED.toString())){
			rc = FrontControllerConstant.RC_CLOSED_ACCOUNT;
		} else {
			accCurr.setStatus(ACCOUNT_STATUS.BLOCKED.toString());
			this.save(accCurr);
		}
		return rc;
	}

	private String activateAccount(Account accCurr) {
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (accCurr.getStatus().equals(ACCOUNT_STATUS.CLOSED.toString())){
			rc = FrontControllerConstant.RC_CLOSED_ACCOUNT;
		} else {
			accCurr.setStatus(ACCOUNT_STATUS.ACTIVE.toString());
			this.save(accCurr);
		}
		return rc;
	}

	@Transactional("prodTransactionManager")
	public void save(Account account) {
		accountRepository.save(account);
	}

	@Transactional("prodTransactionManager")
	@Override
	public Account findByAccountNo(String cif) {
		return accountRepository.findOne(cif);
	}

	@Transactional(readOnly = true)
	@Override
	public AccountInquiryResponse inquiryAccount(
			AccountInquiryRequest accountInquiryRequest){
		AccountInquiryResponse accountInquiryResponse = new AccountInquiryResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		Account accCurr = this.findByAccountNo(accountInquiryRequest.getCif());
		if (accCurr != null){
			if (accCurr.getStatus().equals(ACCOUNT_STATUS.ACTIVE.toString())){
				// decrypt Account Request
				DecryptAccountRequest decryptAccountRequest = new DecryptAccountRequest(accCurr);
				DecryptAccountResponse decryptAccountResponse = this.decryptAccount(decryptAccountRequest);
				if (decryptAccountResponse.getResponseCode().equals(rc)){
					Account accDecrypt = decryptAccountResponse.getAccount();
					BigDecimal sumHoldBalance = holdingBalanceService.getSumHoldingBalance(accDecrypt.getAccountNo());
					BigDecimal currentBalance = accDecrypt.getBalance();
					BigDecimal availableBalance = currentBalance.subtract(sumHoldBalance);
					accountInquiryResponse = new AccountInquiryResponse(
							accDecrypt.getAccountNo(), accDecrypt.getName(), accDecrypt.getEmail(), 
							accDecrypt.getPhoneNo(), accDecrypt.getBod(),
							accDecrypt.getSex(), availableBalance.toString(), accDecrypt.getLevel());
				} else {
					rc = decryptAccountResponse.getResponseCode();
				}
			} else if (accCurr.getStatus().equals(ACCOUNT_STATUS.BLOCKED.toString())){
				rc = FrontControllerConstant.RC_BLOCKED_ACCOUNT;
			} else if (accCurr.getStatus().equals(ACCOUNT_STATUS.CLOSED.toString())){
				rc = FrontControllerConstant.RC_CLOSED_ACCOUNT;
			}
		} else {
			rc = FrontControllerConstant.RC_INVALID_ACCOUNT;
		}
		accountInquiryResponse.setCif(accountInquiryRequest.getCif());
		accountInquiryResponse.setReferenceID(accountInquiryRequest.getReferenceID());
		accountInquiryResponse.setResponseCode(rc);
		return accountInquiryResponse;
	}

	@Transactional(readOnly = true)
	public DecryptAccountResponse decryptAccount(DecryptAccountRequest decryptAccountRequest) {
		DecryptAccountResponse decryptAccountResponse = new DecryptAccountResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		
		Account accCurr = decryptAccountRequest.getAccount();
		Account accDecrypt = new Account();
		try {
			accDecrypt.setName(securityService.decryptByAccount(accCurr.getName(), accCurr.getAccountNo()));
			accDecrypt.setPhoneNo(securityService.decryptByAccount(accCurr.getPhoneNo(), accCurr.getAccountNo()));
			accDecrypt.setBod(securityService.decryptByAccount(accCurr.getBod(), accCurr.getAccountNo()));
			accDecrypt.setEmail(securityService.decryptByAccount(accCurr.getEmail(), accCurr.getAccountNo()));
			accDecrypt.setSex(securityService.decryptByAccount(accCurr.getSex(), accCurr.getAccountNo()));
			accDecrypt.setAccountNo(accCurr.getAccountNo());
			accDecrypt.setBalance(accCurr.getBalance());
			accDecrypt.setLevel(accCurr.getLevel());
			decryptAccountResponse.setAccount(accDecrypt);
		} catch (NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException e) {
			rc = FrontControllerConstant.RC_BAD_DECRYPT;
			e.printStackTrace();
		}
		decryptAccountResponse.setResponseCode(rc);
		return decryptAccountResponse;
	}
	
	@Transactional(readOnly = true)
	@Override
	public AccountInquiryResponse inquiryAccountFull(
			AccountInquiryRequest accountInquiryRequest){
		AccountInquiryResponse accountInquiryResponse = new AccountInquiryResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		Account accCurr = this.findByAccountNo(accountInquiryRequest.getCif());
		if (accCurr != null){
			if (accCurr.getStatus().equals(ACCOUNT_STATUS.ACTIVE.toString())){
				// decrypt Account Request
				DecryptAccountRequest decryptAccountRequest = new DecryptAccountRequest(accCurr);
				DecryptAccountResponse decryptAccountResponse = this.decryptAccountFull(decryptAccountRequest);
				if (decryptAccountResponse.getResponseCode().equals(rc)){
					Account accDecrypt = decryptAccountResponse.getAccount();
					BigDecimal sumHoldBalance = holdingBalanceService.getSumHoldingBalance(accDecrypt.getAccountNo());
					BigDecimal currentBalance = accDecrypt.getBalance();
					BigDecimal availableBalance = currentBalance.subtract(sumHoldBalance);
					accountInquiryResponse = new AccountInquiryResponse(
							accDecrypt.getAccountNo(), accDecrypt.getName(), accDecrypt.getEmail(), 
							accDecrypt.getPhoneNo(), accDecrypt.getBod(),accDecrypt.getSex(), 
							availableBalance.toString(), accDecrypt.getLevel(), accDecrypt.getMaritalStatus(),
							accDecrypt.getJob(), accDecrypt.getReligion(), accDecrypt.getIdentityNo(), 
							accDecrypt.getIdentityType(), accDecrypt.getMotherMaidenName(),
							accDecrypt.getAddress(), accDecrypt.getCity(), accDecrypt.getProvince(), 
							accDecrypt.getCountry(), accDecrypt.getPostalCode(), 
							ServiceUtil.byteToString(accDecrypt.getPhoto()), 
							ServiceUtil.byteToString(accDecrypt.getPhotoCardID()),
							ServiceUtil.byteToString(accDecrypt.getSignature()));
				} else {
					rc = decryptAccountResponse.getResponseCode();
				}
			} else if (accCurr.getStatus().equals(ACCOUNT_STATUS.BLOCKED.toString())){
				rc = FrontControllerConstant.RC_BLOCKED_ACCOUNT;
			} else if (accCurr.getStatus().equals(ACCOUNT_STATUS.CLOSED.toString())){
				rc = FrontControllerConstant.RC_CLOSED_ACCOUNT;
			}
		} else {
			rc = FrontControllerConstant.RC_INVALID_ACCOUNT;
		}
		accountInquiryResponse.setCif(accountInquiryRequest.getCif());
		accountInquiryResponse.setReferenceID(accountInquiryRequest.getReferenceID());
		accountInquiryResponse.setResponseCode(rc);
		return accountInquiryResponse;
	}

	@Transactional(readOnly = true)
	public DecryptAccountResponse decryptAccountFull(DecryptAccountRequest decryptAccountRequest) {
		DecryptAccountResponse decryptAccountResponse = new DecryptAccountResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		
		Account accCurr = decryptAccountRequest.getAccount();
		Account accDecrypt = new Account();
		try {
			AccountKey accountKey = accountPrivateKeyService.findByAccountNo(accCurr.getAccountNo());
			PrivateKey privateKey = securityService.createPrivateKey(accountKey.getExponentPrivate(), accountKey.getModulusPrivate());
			accDecrypt.setName(securityService.decrypt(accCurr.getName(), privateKey));
			accDecrypt.setPhoneNo(securityService.decrypt(accCurr.getPhoneNo(), privateKey));
			accDecrypt.setBod(securityService.decrypt(accCurr.getBod(), privateKey));
			accDecrypt.setEmail(securityService.decrypt(accCurr.getEmail(), privateKey));
			accDecrypt.setSex(securityService.decrypt(accCurr.getSex(), privateKey));
			accDecrypt.setMaritalStatus(securityService.decrypt(accCurr.getMaritalStatus(), privateKey));
			accDecrypt.setJob(securityService.decrypt(accCurr.getJob(), privateKey));
			accDecrypt.setReligion(securityService.decrypt(accCurr.getReligion(), privateKey));
			accDecrypt.setIdentityNo(securityService.decrypt(accCurr.getIdentityNo(), privateKey));
			accDecrypt.setIdentityType(securityService.decrypt(accCurr.getIdentityType(), privateKey));
			accDecrypt.setMotherMaidenName(securityService.decrypt(accCurr.getMotherMaidenName(), privateKey));
			accDecrypt.setAddress(securityService.decrypt(accCurr.getAddress(), privateKey));
			accDecrypt.setCity(securityService.decrypt(accCurr.getCity(), privateKey));
			accDecrypt.setProvince(securityService.decrypt(accCurr.getProvince(), privateKey));
			accDecrypt.setCountry(securityService.decrypt(accCurr.getCountry(), privateKey));
			accDecrypt.setPostalCode(securityService.decrypt(accCurr.getPostalCode(), privateKey));
			accDecrypt.setPhoto(accCurr.getPhoto());
			accDecrypt.setPhotoCardID(accCurr.getPhotoCardID());
			accDecrypt.setSignature(accCurr.getSignature());
			accDecrypt.setAccountNo(accCurr.getAccountNo());
			accDecrypt.setBalance(accCurr.getBalance());
			accDecrypt.setLevel(accCurr.getLevel());
			accDecrypt.setCreateDate(accCurr.getCreateDate());
			accDecrypt.setStatus(accCurr.getStatus());
			accDecrypt.setCreateBy(accCurr.getCreateBy());
			accDecrypt.setCurrency(accCurr.getCurrency());
			decryptAccountResponse.setAccount(accDecrypt);
		} catch (NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException e) {
			rc = FrontControllerConstant.RC_BAD_DECRYPT;
			e.printStackTrace();
		}
		decryptAccountResponse.setResponseCode(rc);
		return decryptAccountResponse;
	}

	@Override
	@Transactional(readOnly = true)
	public LevelUpdateResponse updateLevel(LevelUpdateRequest levelUpdateRequest) {
		LevelUpdateResponse levelUpdateResponse = new LevelUpdateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(levelUpdateRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = this.inquiryAccount(accountInquiryRequest);
		
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			// account must be in BASIC Level
			if (accountInquiryResponse.getLevel().equals(ACCOUNT_LEVEL.BASIC.toString()) || accountInquiryResponse.getLevel().equals(ACCOUNT_LEVEL.REGULAR.toString()) || 
					accountInquiryResponse.getLevel().equals(ACCOUNT_LEVEL.PLATINUM.toString())){
				// upgrade level must be to REGULAR Level
				if (levelUpdateRequest.getLevel().equals(ACCOUNT_LEVEL.REGULAR.toString()) ||
						levelUpdateRequest.getLevel().equals(ACCOUNT_LEVEL.PLATINUM.toString())){
					Account accCurr = this.findByAccountNo(levelUpdateRequest.getCif());
					accCurr.setLevel(levelUpdateRequest.getLevel());
					if(levelUpdateRequest.getBirthOfDate() != null)accCurr.setBod(securityService.encryptByAccount(levelUpdateRequest.getBirthOfDate(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getSex().isEmpty())accCurr.setSex(securityService.encryptByAccount(levelUpdateRequest.getSex(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getMaritalStatus().isEmpty())accCurr.setMaritalStatus(securityService.encryptByAccount(levelUpdateRequest.getMaritalStatus(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getJob().isEmpty())accCurr.setJob(securityService.encryptByAccount(levelUpdateRequest.getJob(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getReligion().isEmpty())accCurr.setReligion(securityService.encryptByAccount(levelUpdateRequest.getReligion(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getIdentityNo().isEmpty())accCurr.setIdentityNo(securityService.encryptByAccount(levelUpdateRequest.getIdentityNo(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getIdentityType().isEmpty())accCurr.setIdentityType(securityService.encryptByAccount(levelUpdateRequest.getIdentityType(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getMotherMaidenName().isEmpty())accCurr.setMotherMaidenName(securityService.encryptByAccount(levelUpdateRequest.getMotherMaidenName(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getAddress().isEmpty())accCurr.setAddress(securityService.encryptByAccount(levelUpdateRequest.getAddress(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getCity().isEmpty())accCurr.setCity(securityService.encryptByAccount(levelUpdateRequest.getCity(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getProvince().isEmpty())accCurr.setProvince(securityService.encryptByAccount(levelUpdateRequest.getProvince(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getCountry().isEmpty())accCurr.setCountry(securityService.encryptByAccount(levelUpdateRequest.getCountry(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getPostalCode().isEmpty())accCurr.setPostalCode(securityService.encryptByAccount(levelUpdateRequest.getPostalCode(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getName().isEmpty())accCurr.setName(securityService.encryptByAccount(levelUpdateRequest.getName(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getEmail().isEmpty())accCurr.setEmail(securityService.encryptByAccount(levelUpdateRequest.getEmail(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getPhoneNo().isEmpty())accCurr.setPhoneNo(securityService.encryptByAccount(levelUpdateRequest.getPhoneNo(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getPhoto().isEmpty())accCurr.setPhoto(ServiceUtil.stringToByte(levelUpdateRequest.getPhoto()));
					if(!levelUpdateRequest.getPhotoCardID().isEmpty())accCurr.setPhotoCardID(ServiceUtil.stringToByte(levelUpdateRequest.getPhotoCardID()));
					if(!levelUpdateRequest.getSignature().isEmpty())accCurr.setSignature(ServiceUtil.stringToByte(levelUpdateRequest.getSignature()));
					this.save(accCurr);
				} else {
					rc = FrontControllerConstant.INVALID_ARGUMENT;
				}
			} else if (accountInquiryResponse.getLevel().equals(ACCOUNT_LEVEL.CORPORATE.toString())){
				// upgrade level must be to CORPORATE Level
				if (levelUpdateRequest.getLevel().equals(ACCOUNT_LEVEL.CORPORATE.toString())){
					Account accCurr = this.findByAccountNo(levelUpdateRequest.getCif());
					accCurr.setLevel(levelUpdateRequest.getLevel());
					if(levelUpdateRequest.getBirthOfDate()!= null)accCurr.setBod(securityService.encryptByAccount(levelUpdateRequest.getBirthOfDate(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getSex().isEmpty())accCurr.setSex(securityService.encryptByAccount(levelUpdateRequest.getSex(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getMaritalStatus().isEmpty())accCurr.setMaritalStatus(securityService.encryptByAccount(levelUpdateRequest.getMaritalStatus(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getJob().isEmpty())accCurr.setJob(securityService.encryptByAccount(levelUpdateRequest.getJob(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getReligion().isEmpty())accCurr.setReligion(securityService.encryptByAccount(levelUpdateRequest.getReligion(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getIdentityNo().isEmpty())accCurr.setIdentityNo(securityService.encryptByAccount(levelUpdateRequest.getIdentityNo(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getIdentityType().isEmpty())accCurr.setIdentityType(securityService.encryptByAccount(levelUpdateRequest.getIdentityType(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getMotherMaidenName().isEmpty())accCurr.setMotherMaidenName(securityService.encryptByAccount(levelUpdateRequest.getMotherMaidenName(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getAddress().isEmpty())accCurr.setAddress(securityService.encryptByAccount(levelUpdateRequest.getAddress(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getCity().isEmpty())accCurr.setCity(securityService.encryptByAccount(levelUpdateRequest.getCity(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getProvince().isEmpty())accCurr.setProvince(securityService.encryptByAccount(levelUpdateRequest.getProvince(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getCountry().isEmpty())accCurr.setCountry(securityService.encryptByAccount(levelUpdateRequest.getCountry(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getPostalCode().isEmpty())accCurr.setPostalCode(securityService.encryptByAccount(levelUpdateRequest.getPostalCode(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getName().isEmpty())accCurr.setName(securityService.encryptByAccount(levelUpdateRequest.getName(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getEmail().isEmpty())accCurr.setEmail(securityService.encryptByAccount(levelUpdateRequest.getEmail(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getPhoneNo().isEmpty())accCurr.setPhoneNo(securityService.encryptByAccount(levelUpdateRequest.getPhoneNo(), levelUpdateRequest.getCif()));
					if(!levelUpdateRequest.getPhoto().isEmpty())accCurr.setPhoto(ServiceUtil.stringToByte(levelUpdateRequest.getPhoto()));
					if(!levelUpdateRequest.getPhotoCardID().isEmpty())accCurr.setPhotoCardID(ServiceUtil.stringToByte(levelUpdateRequest.getPhotoCardID()));
					if(!levelUpdateRequest.getSignature().isEmpty())accCurr.setSignature(ServiceUtil.stringToByte(levelUpdateRequest.getSignature()));
					this.save(accCurr);
				} else {
					rc = FrontControllerConstant.INVALID_ARGUMENT;
				}
			} else {
				rc = FrontControllerConstant.RC_IGNORED_REQUEST;
			}
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		
		levelUpdateResponse.setCif(levelUpdateRequest.getCif());
		levelUpdateResponse.setReferenceID(levelUpdateRequest.getReferenceID());
		levelUpdateResponse.setResponseCode(rc);
		return levelUpdateResponse;
	}

	@Override
	@Transactional(readOnly = false)
	public synchronized RegistrationResponse corporateRegister(
			CorporateRegistrationRequest registrationRequest) throws NoSuchAlgorithmException {
		RegistrationResponse registrationResponse = new RegistrationResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// generating accountNo
		if(!registrationRequest.getLevel().equals(Account.ACCOUNT_LEVEL.BASIC.toString())){
			Account accountReg = new Account();
			String accountNo = this.getNewAccountNo();
			accountReg.setAccountNo(accountNo);
			// generate RSA Key		
			Map<String, Object> key = securityService.getKey();
			String modulusPublicKey = (String) key.get( BaseConstant.PUBLIC_KEY_MODULUS);
			String exponentPublicKey = (String) key.get( BaseConstant.PUBLIC_KEY_EXPONENT);
			String modulusPrivateKey = (String) key.get( BaseConstant.PRIVATE_KEY);
			String exponentPrivateKey = (String) key.get( BaseConstant.PRIVATE_KEY_EXPONENT);
			
			PublicKey publicKey = securityService.createPublicKey(exponentPublicKey, modulusPublicKey);
			
			accountReg.setName(securityService.encrypt(registrationRequest.getName(), publicKey));
			accountReg.setPhoneNo(securityService.encrypt(registrationRequest.getPhoneNo(), publicKey));
			accountReg.setBod(securityService.encrypt(registrationRequest.getBirthOfDate(), publicKey));
			accountReg.setEmail(securityService.encrypt(registrationRequest.getEmail(), publicKey));
			accountReg.setSex(securityService.encrypt(registrationRequest.getSex(), publicKey));
			accountReg.setMaritalStatus(securityService.encrypt(registrationRequest.getMaritalStatus(), publicKey));
			accountReg.setJob(securityService.encrypt(registrationRequest.getJob(), publicKey));
			accountReg.setReligion(securityService.encrypt(registrationRequest.getReligion(), publicKey));
			accountReg.setIdentityNo(securityService.encrypt(registrationRequest.getIdentityNo(), publicKey));
			accountReg.setIdentityType(securityService.encrypt(registrationRequest.getIdentityType(), publicKey));
			accountReg.setMotherMaidenName(securityService.encrypt(registrationRequest.getMotherMaidenName(), publicKey));
			accountReg.setAddress(securityService.encrypt(registrationRequest.getAddress(), publicKey));
			accountReg.setCity(securityService.encrypt(registrationRequest.getCity(), publicKey));
			accountReg.setProvince(securityService.encrypt(registrationRequest.getProvince(), publicKey));
			accountReg.setCountry(securityService.encrypt(registrationRequest.getCountry(), publicKey));
			accountReg.setPostalCode(securityService.encrypt(registrationRequest.getPostalCode(), publicKey));
			accountReg.setPhoto(ServiceUtil.stringToByte(registrationRequest.getPhoto()));
			accountReg.setPhotoCardID(ServiceUtil.stringToByte(registrationRequest.getPhotoCardID()));
			accountReg.setSignature(ServiceUtil.stringToByte(registrationRequest.getSignature()));
			accountReg.setBalance(new BigDecimal(0));
			accountReg.setLevel(registrationRequest.getLevel());
			accountReg.setStatus(ACCOUNT_STATUS.ACTIVE.toString());
			accountReg.setCurrency(BaseConstant.CURRENCY_ID);
			accountReg.setCreateDate(new Date());
			
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		    String username = auth.getName(); //get logged in username
		    accountReg.setCreateBy(username);
		    
			AccountPublicKey accPublicKey = new AccountPublicKey(
					accountReg,	modulusPublicKey, exponentPublicKey);
			AccountKey accPrivateKey = new AccountKey(
					accountNo, modulusPrivateKey, exponentPrivateKey);
			
			accountPublicKeyService.save(accPublicKey);
			accountPrivateKeyService.save(accPrivateKey);
		
			registrationResponse.setCif(accountNo);
			registrationResponse.setReferenceID(registrationRequest.getReferenceID());
		}else{
			rc = FrontControllerConstant.INVALID_ARGUMENT;
		}
		registrationResponse.setResponseCode(rc);
		return registrationResponse;
	}

	@Override
	public String getNameByAccountNo(String cif) throws Exception {
		String name = accountRepository.getNameByAccountNo(cif);
		if(name != null){
			name = securityService.decryptByAccount(name, cif);
		}else{
			name = "";
		}
		return name;
	}

	@Override
	public RestResponse migrateData(RestRequest request) {
		
		File file = new File("d:\\data_customer_ovo.txt");
		try {
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.append(""+new Date() + "\n");
			
			List<Account> accounts = accountRepository.getAll();
			for (Account account : accounts) {
				DecryptAccountRequest decryptAccountRequest = new DecryptAccountRequest(account);
				DecryptAccountResponse decryptAccountResponse = this.decryptAccountFull(decryptAccountRequest);
				Account accDecrypt = decryptAccountResponse.getAccount();
				
				String delim = ";";
				fileWriter.append(accDecrypt.getAccountNo() + delim);
				fileWriter.append(accDecrypt.getCreateBy() + delim);
				fileWriter.append(accDecrypt.getCreateDate() + delim);
				fileWriter.append(accDecrypt.getBalance() + delim);
				fileWriter.append(accDecrypt.getBod() + delim);
				fileWriter.append(accDecrypt.getCurrency() + delim);
				fileWriter.append(accDecrypt.getEmail() + delim);
				fileWriter.append(accDecrypt.getLevel() + delim);
				fileWriter.append(accDecrypt.getName() + delim);
				fileWriter.append(accDecrypt.getPhoneNo() + delim);
				fileWriter.append(accDecrypt.getSex() + delim);
				fileWriter.append(accDecrypt.getStatus() + delim);
				fileWriter.append(accDecrypt.getBlob1() + delim);
				fileWriter.append(accDecrypt.getBlob2() + delim);
				fileWriter.append(accDecrypt.getString1() + delim);
				fileWriter.append(accDecrypt.getString2() + delim);
				fileWriter.append(accDecrypt.getAddress() + delim);
				fileWriter.append(accDecrypt.getCity() + delim);
				fileWriter.append(accDecrypt.getCountry() + delim);
				fileWriter.append(accDecrypt.getIdentityNo() + delim);
				fileWriter.append(accDecrypt.getIdentityType() + delim);
				fileWriter.append(accDecrypt.getJob() + delim);
				fileWriter.append(accDecrypt.getMaritalStatus() + delim);
				fileWriter.append(accDecrypt.getMotherMaidenName() + delim);
				fileWriter.append(accDecrypt.getPhoto() + delim);
				fileWriter.append(accDecrypt.getPhotoCardID() + delim);
				fileWriter.append(accDecrypt.getPostalCode() + delim);
				fileWriter.append(accDecrypt.getProvince() + delim);
				fileWriter.append(accDecrypt.getReligion() + delim);
				fileWriter.append(accDecrypt.getSignature() + ";\n");
				fileWriter.flush();
			}
			fileWriter.append(""+new Date() + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		RestResponse response = new RestResponse();
		response.setResponseCode(BaseConstant.SUCCESS_RESPONSE_CODE);
		response.setCif(request.getCif());
		response.setReferenceID(request.getReferenceID());
		
		return response;
	}


}

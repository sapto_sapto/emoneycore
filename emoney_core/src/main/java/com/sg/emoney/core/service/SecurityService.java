package com.sg.emoney.core.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public interface SecurityService {

	/**
	 * Generate server side private and public key
	 */
	public abstract Map<String, Object> getKey()
			throws NoSuchAlgorithmException;

	/**
	 * Decrypt text using given private key
	 * 
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public abstract String decrypt(String text, PrivateKey privateKey)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException;

	/**
	 * Encrypt string using given public key
	 */
	public abstract String encrypt(String text, PublicKey publicKey);

	/**
	 * Generate public key from given modulus and public exponent
	 */
	public abstract PublicKey createPublicKey(String publicExponent,
			String modulus);

	public abstract PrivateKey createPrivateKey(String exponent, String modulus);

	public abstract String encryptByAccount(String string, String accountNo);

	public abstract String decryptByAccount(String string, String accountNo) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException;

}
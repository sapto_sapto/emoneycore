package com.sg.emoney.core.model.core.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity {

	Date createDate;
	String createBy;
	
	private String string1;
	private String string2;
	
	private byte[] blob1;
	private byte[] blob2;
	
	@Column
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column
	public String getCreateBy() {
		return createBy;
	}
	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	@Column
	public String getString1() {
		return string1;
	}
	public void setString1(String string1) {
		this.string1 = string1;
	}
	
	@Column
	public String getString2() {
		return string2;
	}
	public void setString2(String string2) {
		this.string2 = string2;
	}
	
	@Column(length=2000000)
	@Lob @Basic(fetch=FetchType.LAZY)
	public byte[] getBlob1() {
		return blob1;
	}
	public void setBlob1(byte[] blob1) {
		this.blob1 = blob1;
	}
	
	@Column(length=2000000)
	@Lob @Basic(fetch=FetchType.LAZY)
	public byte[] getBlob2() {
		return blob2;
	}
	public void setBlob2(byte[] blob2) {
		this.blob2 = blob2;
	}
	
}

package com.sg.emoney.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.constant.FrontControllerConstant;
import com.sg.emoney.core.model.core.entity.Merchant;
import com.sg.emoney.core.model.core.repository.MerchantRepository;
import com.sg.emoney.core.model.message.AddMerchantRequest;
import com.sg.emoney.core.model.message.AddMerchantResponse;
import com.sg.emoney.core.model.message.DeleteMerchantRequest;
import com.sg.emoney.core.model.message.DeleteMerchantResponse;
import com.sg.emoney.core.model.message.AddMerchantRequest.MERCHANT_ACTION;

@Service
public class MerchantServiceImpl implements MerchantService {
	@Autowired
	MerchantRepository merchantRepository;

	@Transactional(readOnly = false)
	@Override
	public AddMerchantResponse saveMerchant(AddMerchantRequest addMerchantRequest) {
		AddMerchantResponse addMerchantResponse = new AddMerchantResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (addMerchantRequest.getChannel().equals(BaseConstant.CHANNEL_BACKOFFICE)){
			if (addMerchantRequest.getAction().equals(MERCHANT_ACTION.ADD.toString())){
				if (!isExist(addMerchantRequest.getCode())){
					Merchant merchant = new Merchant(
							addMerchantRequest.getCode(),
							addMerchantRequest.getName(),
							addMerchantRequest.getAccountSource(),
							addMerchantRequest.getStatus()
						);
					merchant.setCreateBy(addMerchantRequest.getUsername());
					merchant.setCreateDate(new Date());
					merchantRepository.save(merchant);
				} else {
					rc = FrontControllerConstant.RC_CODE_EXIST;
				}
			} else if (addMerchantRequest.getAction().equals(MERCHANT_ACTION.EDIT.toString())){
				Merchant merchant = new Merchant(
						addMerchantRequest.getCode(),
						addMerchantRequest.getName(),
						addMerchantRequest.getAccountSource(),
						addMerchantRequest.getStatus()
					);
				merchant.setCreateBy(addMerchantRequest.getUsername());
				merchant.setCreateDate(new Date());
				merchantRepository.save(merchant);
			} else {
				rc = FrontControllerConstant.INVALID_ARGUMENT;
			}
		} else {
			rc = FrontControllerConstant.RC_IGNORED_REQUEST;
		}
		
		addMerchantResponse.setUsername(addMerchantRequest.getUsername());
		addMerchantResponse.setResponseCode(rc);
		return addMerchantResponse;
	}

	private boolean isExist(String code) {
		Merchant merchant = merchantRepository.findOne(code);
		if (merchant != null){
			return true;
		}
		return false;
	}

	@Transactional(readOnly = false)
	@Override
	public DeleteMerchantResponse deleteMerchant(
			DeleteMerchantRequest deleteMerchantRequest) {
		DeleteMerchantResponse delMerchantResponse = new DeleteMerchantResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (deleteMerchantRequest.getChannel().equals(BaseConstant.CHANNEL_BACKOFFICE)){
			Iterable<String> codes = deleteMerchantRequest.getCodes();
			List<Merchant> merchants = merchantRepository.findAll(codes);
			Iterable<Merchant> mIterable = merchants;
			merchantRepository.delete(mIterable);
		} else {
			rc = FrontControllerConstant.RC_IGNORED_REQUEST;
		}
		
		delMerchantResponse.setUsername(deleteMerchantRequest.getUsername());
		delMerchantResponse.setResponseCode(rc);
		return delMerchantResponse;
	}

}

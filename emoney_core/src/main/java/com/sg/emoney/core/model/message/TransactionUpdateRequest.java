package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class TransactionUpdateRequest extends RestRequest{

	/**
system
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String amount;
	private String description;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String currency;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String merchantCode;
	
	public enum TRX_TYPE{DEBET, CREDIT}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	

	
}

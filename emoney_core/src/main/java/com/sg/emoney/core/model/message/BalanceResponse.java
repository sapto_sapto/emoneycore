package com.sg.emoney.core.model.message;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class BalanceResponse extends RestResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String nominal;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountID;
	private Date lastUpdate;
	
	public String getNominal() {
		return nominal;
	}
	
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	
	public String getAccountID() {
		return accountID;
	}
	
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
}

package com.sg.emoney.core.model.message;

import com.sg.emoney.core.model.core.entity.Account;

public class DecryptAccountResponse extends RestResponse {

	/**
system
	 */
	private static final long serialVersionUID = 6432204933317341917L;

	Account account;
	
	public DecryptAccountResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DecryptAccountResponse(Account account) {
		super();
		this.account = account;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}

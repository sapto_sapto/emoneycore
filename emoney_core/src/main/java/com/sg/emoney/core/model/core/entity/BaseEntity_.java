package com.sg.emoney.core.model.core.entity;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.sg.emoney.core.model.core.entity.BaseEntity;

@StaticMetamodel( BaseEntity.class )
public class BaseEntity_ {
	public static volatile SingularAttribute<BaseEntity, Date> createDate;
	public static volatile SingularAttribute<BaseEntity, String> createBy;
	public static volatile SingularAttribute<BaseEntity, String> string1;
	public static volatile SingularAttribute<BaseEntity, String> string2;
	public static volatile SingularAttribute<BaseEntity, byte[]> blob1;
	public static volatile SingularAttribute<BaseEntity, byte[]> blob2;
}

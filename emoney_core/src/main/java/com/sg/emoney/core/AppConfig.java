package com.sg.emoney.core;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.emoney.core.aspect.AuditTrail;

/**
 * 
 *
 */
@EnableAutoConfiguration
@Controller
@SpringBootApplication
@ComponentScan("com.sg.emoney.core")
@PropertySource({"classpath:errorcode.properties", "classpath:service.properties"})
public class AppConfig extends WebMvcConfigurerAdapter {
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class);
	}


	@Bean
	public AuditTrail auditTrail() {
		return new AuditTrail();
	}
	
	@Bean
	public RestTemplate restTemplate() throws Exception {
	    RestTemplate rest = new RestTemplate();
	    //this is crucial!
	    rest.getMessageConverters().add(0, mappingJacksonHttpMessageConverter());
	    return rest;
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter() throws Exception {
	    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	    converter.setObjectMapper(myObjectMapper());
	    return converter;
	}

	@Bean
	public ObjectMapper myObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper;
	}
	
	@Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(new Locale("in"));
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }
 
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
    
}

package com.sg.emoney.core.aspect;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.AuditActivityLog;
import com.sg.emoney.core.model.core.entity.AuditEntity;
import com.sg.emoney.core.model.core.entity.BaseEntity;
import com.sg.emoney.core.model.core.entity.Merchant;
import com.sg.emoney.core.model.core.entity.TrxCode;
import com.sg.emoney.core.model.core.repository.AuditActivityLogRepository;
import com.sg.emoney.core.model.core.repository.AuditEntityRepository;
import com.sg.emoney.core.model.message.ConfRestResponse;
import com.sg.emoney.core.model.message.RestRequest;
import com.sg.emoney.core.model.message.RestResponse;
import com.sg.emoney.core.tools.Util;

@Aspect
public class AuditTrail {

	@Autowired
    private Environment env;
	
	@Autowired
	AuditEntityRepository auditEntityRepository;
	
	@Autowired
	AuditActivityLogRepository auditActivityLogRepository;

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory
			.getLogger("auditcontroller");
	org.slf4j.Logger LOG_SERVICE = LoggerFactory.getLogger("auditservice");
	org.slf4j.Logger LOG_MESSAGE = LoggerFactory.getLogger("auditmessage");

	@Before("execution(* com.sg.emoney.core.controller.*.*(..))")
	public void logController(JoinPoint joinPoint) throws Exception {
		LOG_CONTROLLER.info("before method => "
				+ joinPoint.getSignature().getName() + " Class: "
				+ joinPoint.getTarget().getClass().getSimpleName());
	}
	
	@Before("execution(* com.sg.emoney.core.endpoint.*.*(..)) && args(request)")
	public void logEndpoint(JoinPoint joinPoint, Object request) throws Exception {
		LOG_CONTROLLER.info("before method => "
				+ joinPoint.getSignature().getName() + " Class: "
				+ joinPoint.getTarget().getClass().getSimpleName());
		ObjectWriter objectWriter = new ObjectMapper()
		.writerWithDefaultPrettyPrinter();

		String req = objectWriter.writeValueAsString(request);
		LOG_MESSAGE.info("before method => "
				+ joinPoint.getSignature().getName() + "\n object request => \n"  + req);
	}

	@Before("execution(* com.sg.emoney.core.service.*.*(..))")
	public void logServiceAccess(JoinPoint joinPoint) throws Exception {
		LOG_SERVICE.info("before method => "
				+ joinPoint.getSignature().getName() + ". Class: "
				+ joinPoint.getTarget().getClass().getSimpleName());
	}

	@Before("execution(* com.sg.emoney.core.service.*.save(..)) && args(bean)")
	public void setAttribute(JoinPoint joinPoint, Object bean) throws Exception {

		Class<? extends Object> class1 = bean.getClass();
		Field createDate = null;
		Field createBy = null;
		Class<?> superclass = class1.getSuperclass();

		if (superclass.equals(BaseEntity.class)) {
			createDate = superclass.getDeclaredField("createDate");
			createBy = superclass.getDeclaredField("createBy");
		} else {
			superclass = superclass.getSuperclass();
			createDate = superclass.getDeclaredField("createDate");
			createBy = superclass.getDeclaredField("createBy");
		}

		createDate.setAccessible(true);
		createBy.setAccessible(true);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
		createDate.set(bean, new Date());
		createBy.set(bean, new String(username));
	}

	@Before("execution(* com.sg.emoney.core.service.*.saveBatch(..)) && args(beans)")
	public void setAttributeSaveBatch(JoinPoint joinPoint, List<Object> beans) throws Exception {
		for (Object bean : beans) {
			Class<? extends Object> class1 = bean.getClass();
			Field createDate = null;
			Field createBy = null;
			Class<?> superclass = class1.getSuperclass();

			if (superclass.equals(BaseEntity.class)) {
				createDate = superclass.getDeclaredField("createDate");
				createBy = superclass.getDeclaredField("createBy");
			} else {
				superclass = superclass.getSuperclass();
				createDate = superclass.getDeclaredField("createDate");
				createBy = superclass.getDeclaredField("createBy");
			}

			createDate.setAccessible(true);
			createBy.setAccessible(true);
			
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		    String username = auth.getName(); //get logged in username
			createDate.set(bean, new Date());
			createBy.set(bean, new String(username));
		}
	}

	@Before("execution(* com.sg.emoney.core.service.*.save(..)) && args(bean)")
	public void auditObjectSave(JoinPoint joinPoint, Object bean) throws Exception {

		String id = getId(bean);
		String classBean = bean.getClass().toString();
		
		ObjectWriter objectWriter = new ObjectMapper()
				.writerWithDefaultPrettyPrinter();
		String valueBean = objectWriter.writeValueAsString(bean);
		String act = new String();
		act = joinPoint.getSignature().getName().toUpperCase();
		if (act.equals(AuditEntity.ACTION.SAVE.name())) {
			if (id == null) {
				act = AuditEntity.ACTION.CREATE.name();
			} else {
				act = AuditEntity.ACTION.UPDATE.name();
			}
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
		AuditEntity ae = new AuditEntity(valueBean, id, classBean, act,
				username);
		auditEntityRepository.save(ae);

	}
	
	@Before("execution(* com.sg.emoney.core.service.*.delete(..)) && args(bean)")
	public void auditObjectDel(JoinPoint joinPoint, Object bean) throws Exception {

		String id = getId(bean);
		String classBean = bean.getClass().toString();
		
		ObjectWriter objectWriter = new ObjectMapper()
				.writerWithDefaultPrettyPrinter();
		String valueBean = objectWriter.writeValueAsString(bean);
		String act = new String();
		act = joinPoint.getSignature().getName().toUpperCase();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
		AuditEntity ae = new AuditEntity(valueBean, id, classBean, act,
				username);
		auditEntityRepository.save(ae);

	}
	
	@Before("execution(* com.sg.emoney.core.service.*.saveBatch(..)) && args(beans)")
	public void auditObjectSaveBatch(JoinPoint joinPoint, List<Object> beans) throws Exception {
		for (Object bean : beans) {
			String id = getId(bean);
			String classBean = bean.getClass().toString();
			
			ObjectWriter objectWriter = new ObjectMapper()
					.writerWithDefaultPrettyPrinter();
			String valueBean = objectWriter.writeValueAsString(bean);
			String act = new String();
			act = joinPoint.getSignature().getName().toUpperCase();
			if (id == null) {
				act = AuditEntity.ACTION.CREATE.name();
			} else {
				act = AuditEntity.ACTION.UPDATE.name();
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		    String username = auth.getName(); //get logged in username
			AuditEntity ae = new AuditEntity(valueBean, id, classBean, act,
					username);
			auditEntityRepository.save(ae);
		}
	}
	
	private String getId(Object bean) throws Exception  {
		String id = new String();
		if(bean instanceof Account){
			if (PropertyUtils.getProperty(bean, "accountNo") != null) {
				id = PropertyUtils.getProperty(bean, "accountNo").toString();
			} 			
		}else if(bean instanceof Merchant || bean instanceof TrxCode){
			if (PropertyUtils.getProperty(bean, "code") != null) {
				id = PropertyUtils.getProperty(bean, "code").toString();
			} 	
		}
		else if(PropertyUtils.getProperty(bean, "id") != null){
			id = PropertyUtils.getProperty(bean, "id").toString();
		}
		else{
			id = null;
		}
		return id;
	}
	
	@AfterThrowing(pointcut = "execution(* com.sg.emoney.core.service.*.*(..))", throwing = "e")
	public void serviceAfterThrowing(JoinPoint joinPoint, Throwable e) {
		String methodName = joinPoint.getSignature().getName();
		String stuff = joinPoint.getSignature().toString();
		String arguments = Arrays.toString(joinPoint.getArgs());
		LOG_SERVICE.info("After throwing method => " + methodName
				+ " with arguments " + arguments + "\nand the full toString: "
				+ stuff + "\nthe exception is: " + e.getMessage(), e);
	}

	@AfterThrowing(pointcut = "execution(* com.sg.emoney.core.endpoint.*.*(..))", throwing = "e")
	public void controllerAfterThrowing(JoinPoint joinPoint, Throwable e) {
		String methodName = joinPoint.getSignature().getName();
		String stuff = joinPoint.getSignature().toString();
		String arguments = Arrays.toString(joinPoint.getArgs());
		LOG_CONTROLLER.info("After throwing method => " + methodName
				+ " with arguments " + arguments + "\nand the full toString: "
				+ stuff + "\nthe exception is: " + e.getMessage(), e);
	}
	
	@Transactional("prodTransactionManager")
	@AfterReturning(pointcut = "(execution(* com.sg.emoney.core.endpoint.VASEndpoint.*(..))) && args(request)", returning = "result")
	public Object afterReturningInterceptor(JoinPoint joinPoint,
			Object request, Object result) throws IOException {
		LOG_CONTROLLER.info("after return method => "
				+ joinPoint.getSignature().getName() + " Class: "
				+ joinPoint.getTarget().getClass().getSimpleName());
		RestRequest restRequest = (RestRequest) request;
		RestResponse restResponse = (RestResponse) result;
		if (!restResponse.getResponseCode().equals(BaseConstant.SUCCESS_RESPONSE_CODE)){
			String message = env.getProperty("RC_" + restResponse.getResponseCode());
			restResponse.setMessage(message);
			result = restResponse;
		}
		
		String methodName = joinPoint.getSignature().getName();
		ObjectWriter objectWriter = new ObjectMapper()
				.writerWithDefaultPrettyPrinter();

		String req = objectWriter.writeValueAsString(request);
		String resp = objectWriter.writeValueAsString(result);
		AuditActivityLog aal = new AuditActivityLog(req, resp);
		
		aal.setReferenceID(restRequest.getReferenceID());
		aal.setMethodName(methodName);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
		aal.setCreateBy(username);
		aal.setCreateDate(new Date());
		auditActivityLogRepository.save(aal);
		LOG_MESSAGE.info("after return method => "
				+ joinPoint.getSignature().getName() + "\n object response => \n"  + resp);
		return result;
	}
	
	@Transactional("prodTransactionManager")
	@AfterReturning(pointcut = "(execution(* com.sg.emoney.core.endpoint.VASEndpoint.*(..))) && args(servletRequest,request)", returning = "result")
	public Object afterReturningInterceptor(JoinPoint joinPoint,
			HttpServletRequest servletRequest, Object request, Object result) throws IOException{
		return afterReturningInterceptor(joinPoint, request, result);
	}
	
	@Transactional("prodTransactionManager")
	@AfterReturning(pointcut = "(execution(* com.sg.emoney.core.endpoint.VASConfigurationController.*(..))) && args(request)", returning = "result")
	public Object afterReturnThrowingInterceptorConf(JoinPoint joinPoint,
			Object request, Object result) throws IOException {
		LOG_CONTROLLER.info("after return method => "
				+ joinPoint.getSignature().getName() + " Class: "
				+ joinPoint.getTarget().getClass().getSimpleName());
		ConfRestResponse restResponse = (ConfRestResponse) result;
		if (!restResponse.getResponseCode().equals(BaseConstant.SUCCESS_RESPONSE_CODE)){
			String message = env.getProperty("RC_" + restResponse.getResponseCode());
			restResponse.setMessage(message);
			result = restResponse;
		}
		
		String methodName = joinPoint.getSignature().getName();
		ObjectWriter objectWriter = new ObjectMapper()
				.writerWithDefaultPrettyPrinter();

		String req = objectWriter.writeValueAsString(request);
		String resp = objectWriter.writeValueAsString(result);
		AuditActivityLog aal = new AuditActivityLog(req, resp);
		
		aal.setMethodName(methodName);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
		aal.setCreateBy(username);
		aal.setCreateDate(new Date());
		auditActivityLogRepository.save(aal);
		LOG_MESSAGE.info("after return method => "
				+ joinPoint.getSignature().getName() + "\n object response => \n"  + resp);
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional("prodTransactionManager")
	@AfterReturning(pointcut = "(execution(* com.sg.emoney.core.endpoint.BaseController.*(..))) && args(request,error,..)", returning = "result")
	public Object afterReturnThrowing(JoinPoint joinPoint,
			HttpServletRequest request, Exception error, Object result)
			throws IOException {
		LOG_CONTROLLER.info("after return method => "
				+ joinPoint.getSignature().getName() + " Class: "
				+ joinPoint.getTarget().getClass().getSimpleName());
		RestResponse restResponse = (RestResponse) result;
		if (!restResponse.getResponseCode().equals(BaseConstant.SUCCESS_RESPONSE_CODE)){
			String message = env.getProperty("RC_" + restResponse.getResponseCode());
			restResponse.setMessage(message);
			result = restResponse;
		}
		
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = request.getReader();
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		String req = sb.toString();
		Class clazz = (Class) request.getAttribute(BaseConstant.REQUEST_CLASS);
		RestRequest restReq = (RestRequest)new ObjectMapper().readValue(req,clazz);
		String methodName = joinPoint.getSignature().getName();
		ObjectWriter objectWriter = new ObjectMapper()
				.writerWithDefaultPrettyPrinter();
		
		String refId = restReq.getReferenceID();
		restResponse.setReferenceID(refId);

		String resp = objectWriter.writeValueAsString(result);
		String reqs = objectWriter.writeValueAsString(restReq);
		AuditActivityLog aal = new AuditActivityLog(reqs, resp);
		
		aal.setReferenceID(refId);
		aal.setMethodName(methodName);
		if(error.getClass().toString().contains("siefe.vas")){
			aal.setErrorMessage(error.getMessage());
		}else{
			aal.setErrorMessage(Util.getStackTrace(error));
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName(); //get logged in username
		aal.setCreateBy(username);
		aal.setCreateDate(new Date());
		auditActivityLogRepository.save(aal);
		LOG_MESSAGE.info("after return method => "
				+ joinPoint.getSignature().getName() + "\n object response => \n"  + resp);
		return result;

	}

}
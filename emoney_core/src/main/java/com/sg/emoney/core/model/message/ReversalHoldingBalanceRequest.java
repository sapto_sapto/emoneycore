package com.sg.emoney.core.model.message;


public class ReversalHoldingBalanceRequest extends RestRequest {

	/**
system
	 */
	private static final long serialVersionUID = 4692954048006973174L;

	private String purposeId;

	public String getPurposeId() {
		return purposeId;
	}

	public void setPurposeId(String purposeId) {
		this.purposeId = purposeId;
	}
}

package com.sg.emoney.core.model.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class AuditEntity extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer id;
	
	
	/**
	 * value object to json
	 */
	@Lob 
	@Column(length=1000)
	String object;
	/**
	 * primary key old object
	 */
	String primaryKey;
	
	/**
	 * example: com.mlpt.siefe.cbs.model.entity
	 */
	String classEntity;
	
	/**
	 * delete, update, save
	 */
	String action;
	
	public static enum ACTION{CREATE, UPDATE, DELETE, SAVE, DELETEBATCH, SAVEBATCH};

	public AuditEntity(){
		
	}
	
	/**
	 * 
	 * @param object
	 * @param primaryKey
	 * @param classEntity
	 * @param action
	 */
	public AuditEntity(String object, String primaryKey, String classEntity, String action,String createdBy){
		this.object=object;
		this.primaryKey = primaryKey;
		this.classEntity=classEntity;
		this.action=action;
		this.createBy = createdBy;
		this.createDate= new Date();
	}
}

package com.sg.emoney.core.model.message;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class ConfRestResponse implements Serializable {

	/**
system
	 */
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	protected String username;
	protected String responseCode;
	protected String message;

	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}

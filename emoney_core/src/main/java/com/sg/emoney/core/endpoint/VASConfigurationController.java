package com.sg.emoney.core.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sg.emoney.core.model.message.AddMerchantRequest;
import com.sg.emoney.core.model.message.AddMerchantResponse;
import com.sg.emoney.core.model.message.AddTrxCodeRequest;
import com.sg.emoney.core.model.message.AddTrxCodeResponse;
import com.sg.emoney.core.model.message.DeleteMerchantRequest;
import com.sg.emoney.core.model.message.DeleteMerchantResponse;
import com.sg.emoney.core.model.message.DeleteTrxCodeRequest;
import com.sg.emoney.core.model.message.DeleteTrxCodeResponse;
import com.sg.emoney.core.service.MerchantService;
import com.sg.emoney.core.service.TrxCodeService;

@RestController
@RequestMapping("/conf/")
public class VASConfigurationController extends BaseController {
	/**
system
	 */
	
	@Autowired
	MerchantService merchantService;
	
	@Autowired
	TrxCodeService trxCodeService;
	
	/**
	 * Add Merchant
	 */
	@RequestMapping(value="/VAConfAddMerchant", method = RequestMethod.POST)
	@ResponseBody
	public AddMerchantResponse addMerchant(@RequestBody AddMerchantRequest addMerchantRequest){
		return merchantService.saveMerchant(addMerchantRequest);
	}
	
	/**
	 * Delete Merchant
	 */
	@RequestMapping(value="/VAConfDeleteMerchant", method = RequestMethod.POST)
	@ResponseBody
	public DeleteMerchantResponse deleteMerchant(@RequestBody DeleteMerchantRequest deleteMerchantRequest){
		return merchantService.deleteMerchant(deleteMerchantRequest);
	}
	
	/**
	 * Add Transaction Code
	 */
	@RequestMapping(value="/VAConfAddTrxCode", method = RequestMethod.POST)
	@ResponseBody
	public AddTrxCodeResponse addTrxCode(@RequestBody AddTrxCodeRequest addTrxCodeRequest){
		return trxCodeService.saveTrxCode(addTrxCodeRequest);
	}
	
	/**
	 * Delete Transaction Code
	 */
	@RequestMapping(value="/VAConfDeleteTrxCode", method = RequestMethod.POST)
	@ResponseBody
	public DeleteTrxCodeResponse deleteTrxCode(@RequestBody DeleteTrxCodeRequest deleteTrxCodeRequest){
		return trxCodeService.deleteTrxCode(deleteTrxCodeRequest);
	}
}

package com.sg.emoney.core.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.constant.FrontControllerConstant;
import com.sg.emoney.core.exception.LimitCreditException;
import com.sg.emoney.core.exception.TransactionException;
import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.Balance;
import com.sg.emoney.core.model.core.entity.HoldingBalance;
import com.sg.emoney.core.model.core.entity.Merchant;
import com.sg.emoney.core.model.core.entity.TrxCode;
import com.sg.emoney.core.model.core.entity.Account.ACCOUNT_LEVEL;
import com.sg.emoney.core.model.core.entity.Balance.BALANCE_STATUS;
import com.sg.emoney.core.model.core.entity.HoldingBalance.BALANCEHOLD_STATUS;
import com.sg.emoney.core.model.core.repository.CurrencyRateRepository;
import com.sg.emoney.core.model.core.repository.MerchantRepository;
import com.sg.emoney.core.model.core.repository.TrxCodeRepository;
import com.sg.emoney.core.model.message.AccountInquiryRequest;
import com.sg.emoney.core.model.message.AccountInquiryResponse;
import com.sg.emoney.core.model.message.AccountTransferRequest;
import com.sg.emoney.core.model.message.AccountTransferResponse;
import com.sg.emoney.core.model.message.BalanceRequest;
import com.sg.emoney.core.model.message.BalanceResponse;
import com.sg.emoney.core.model.message.CancelHoldingBalanceRequest;
import com.sg.emoney.core.model.message.CancelHoldingBalanceResponse;
import com.sg.emoney.core.model.message.HistoryByDateRequest;
import com.sg.emoney.core.model.message.HistoryByDateResponse;
import com.sg.emoney.core.model.message.HistoryRequest;
import com.sg.emoney.core.model.message.HistoryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryByDateRequest;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryByDateResponse;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryRequest;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceInquiryRequest;
import com.sg.emoney.core.model.message.HoldingBalanceInquiryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseRequest;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseResponse;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseTopUpRequest;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseTopUpResponse;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateRequest;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateResponse;
import com.sg.emoney.core.model.message.ReversalHoldingBalanceRequest;
import com.sg.emoney.core.model.message.ReversalHoldingBalanceResponse;
import com.sg.emoney.core.model.message.ReversalTransactionRequest;
import com.sg.emoney.core.model.message.ReversalTransactionResponse;
import com.sg.emoney.core.model.message.Transaction;
import com.sg.emoney.core.model.message.TransactionCorDResponse;
import com.sg.emoney.core.model.message.TransactionHoldResponse;
import com.sg.emoney.core.model.message.TransactionUpdateRequest;
import com.sg.emoney.core.model.message.TransactionUpdateResponse;
import com.sg.emoney.core.model.message.TransferOnUsRequestExec;
import com.sg.emoney.core.model.message.TransferOnUsRequestInq;
import com.sg.emoney.core.model.message.TransferOnUsResponseExec;
import com.sg.emoney.core.model.message.TransferOnUsResponseInq;
import com.sg.emoney.core.model.message.TransferOtherRequestExec;
import com.sg.emoney.core.model.message.TransferOtherResponseExec;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateRequest.TRXHOLD_TYPE;
import com.sg.emoney.core.model.message.TransactionUpdateRequest.TRX_TYPE;
import com.sg.emoney.core.tools.ServiceUtil;

@Service
public class TransactionServiceImpl implements TransactionService{

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private HoldingBalanceService holdingBalanceService;
	
	@Autowired
	private CurrencyRateRepository currencyRateRepository;
	
	@Autowired
	private BalanceService balanceService;
	
	@Autowired
	private MerchantRepository merchantRepository;
	
	@Autowired
	private TrxCodeRepository trxCodeRepository;
	
	@Value("${balance.reg.max}")
	private BigDecimal maxRegBalance;
	
	@Value("${balance.reg.min}")
	private BigDecimal minRegBalance;
	
	@Value("${balance.basic.max}")
	private BigDecimal maxBasicBalance;
	
	@Value("${balance.basic.min}")
	private BigDecimal minBasicBalance;
	
	@Value("${balance.corp.max}")
	private BigDecimal maxCorpBalance;
	
	@Value("${balance.corp.min}")
	private BigDecimal minCorpBalance;
	
	@Value("${balance.platinum.min}")
	private BigDecimal minPlatinumBalance;
	
	@Transactional(readOnly = true)
	public BalanceResponse inquiryBalance(BalanceRequest balanceRequest) {
		BalanceResponse balanceResponse = new BalanceResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(balanceRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			balanceResponse.setAccountID(accountInquiryResponse.getAccountNo());
			balanceResponse.setNominal(accountInquiryResponse.getNominal());
			balanceResponse.setLastUpdate(new Date());
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		balanceResponse.setCif(balanceRequest.getCif());
		balanceResponse.setReferenceID(balanceRequest.getReferenceID());
		balanceResponse.setResponseCode(rc);
		return balanceResponse;
	}

	@Transactional(readOnly = true)
	public HistoryResponse inquiryHistory(HistoryRequest historyRequest) {
		HistoryResponse historyResponse = new HistoryResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(historyRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			List<Balance> balances = balanceService.findByAccountWithPaging(historyRequest.getQty(), historyRequest.getOrder(),
					historyRequest.getCif());
			historyResponse.setTransactions(this.getHistoryTransaction(balances));
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		historyResponse.setCif(historyRequest.getCif());
		historyResponse.setReferenceID(historyRequest.getReferenceID());
		historyResponse.setResponseCode(rc);
		return historyResponse;
	}
	
	@Transactional(readOnly = true)
	private List<Transaction> getHistoryTransaction(
			List<Balance> balances) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (Balance balance : balances) {
			BigDecimal nominal = new BigDecimal(0);
			if (balance.getType().equals(TRX_TYPE.CREDIT.toString())){
				nominal = balance.getCreditAmount();
			} else if (balance.getType().equals(TRX_TYPE.DEBET.toString())){
				nominal = balance.getDebetAmount();
			}
			Transaction transaction = new Transaction(
					balance.getRefnum(),
					balance.getType(),
					nominal.toString(), 
					balance.getCreateDate(),
					balance.getDescription()
			);
			transactions.add(transaction);
		}
		return transactions;
	}

	@Transactional(readOnly = true)
	public HistoryByDateResponse inquiryHistoryByDate(
			HistoryByDateRequest historyRequest) {
		HistoryByDateResponse historyResponse = new HistoryByDateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(historyRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			List<Balance> balances = balanceService.findByAccountWithPaging(historyRequest.getQty(), historyRequest.getOrder(),
					historyRequest.getDateFrom(), historyRequest.getDateTo(),historyRequest.getCif());
			historyResponse.setTransactions(this.getHistoryTransaction(balances));
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		historyResponse.setCif(historyRequest.getCif());
		historyResponse.setReferenceID(historyRequest.getReferenceID());
		historyResponse.setResponseCode(rc);
		return historyResponse;
	}

	@Transactional(readOnly = false, rollbackFor=Exception.class)
	@Override
	public TransactionUpdateResponse updateTransaction(
			TransactionUpdateRequest transactionUpdateRequest) throws LimitCreditException {
		TransactionUpdateResponse transactionUpdateResponse = new TransactionUpdateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (transactionUpdateRequest.getType().equals(TRX_TYPE.CREDIT.toString())){
			TransactionCorDResponse transactionCorDResponse = this.creditAccount(transactionUpdateRequest);
			if (transactionCorDResponse.getResponseCode().equals(rc)){
				transactionUpdateResponse.setAccountName(transactionCorDResponse.getAccountName());
				balanceService.save(transactionCorDResponse.getBalance());
			} else {
				rc = transactionCorDResponse.getResponseCode();
			}
		} else if (transactionUpdateRequest.getType().equals(TRX_TYPE.DEBET.toString())){
			TransactionCorDResponse transactionCorDResponse = this.debetAccount(transactionUpdateRequest);
			if (transactionCorDResponse.getResponseCode().equals(rc)){
				transactionUpdateResponse.setAccountName(transactionCorDResponse.getAccountName());
				balanceService.save(transactionCorDResponse.getBalance());
			} else {
				rc = transactionCorDResponse.getResponseCode();
			}
		} else {
			rc = FrontControllerConstant.INVALID_ARGUMENT;
		}
		transactionUpdateResponse.setCif(transactionUpdateRequest.getCif());
		transactionUpdateResponse.setReferenceID(transactionUpdateRequest.getReferenceID());
		transactionUpdateResponse.setResponseCode(rc);
		return transactionUpdateResponse;
	}

	@Transactional(readOnly = true)
	private TransactionCorDResponse debetAccount(
			TransactionUpdateRequest transactionUpdateRequest) {
		TransactionCorDResponse transactionCorDResponse = new TransactionCorDResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(transactionUpdateRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionCorDResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionCorDResponse;
		}
		transactionCorDResponse.setAccountName(accountInquiryResponse.getName());
		BigDecimal availableBalance = new BigDecimal(accountInquiryResponse.getNominal());
		Account account = accountService.findByAccountNo(transactionUpdateRequest.getCif());
		BigDecimal oldBalance = account.getBalance();
		String oldCurrency = account.getCurrency();
		BigDecimal debetAmount = new BigDecimal(transactionUpdateRequest.getAmount());
		debetAmount = debetAmount.divide(new BigDecimal(100));
		String debetCurrency = transactionUpdateRequest.getCurrency();
		
		if (debetAmount.compareTo(new BigDecimal(0)) == -1){
			rc = FrontControllerConstant.RC_NEGATIVE_BALANCE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		
		if (availableBalance.compareTo(debetAmount) == -1 && !accountInquiryResponse.getLevel().equals(Account.ACCOUNT_LEVEL.CORPORATE.toString())){
			rc = FrontControllerConstant.BALANCE_NOT_ENOUGH;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		BigDecimal conversionRate = currencyRateRepository.findConvertionRate(debetCurrency,oldCurrency);
		if (conversionRate == null){
			rc = FrontControllerConstant.NOT_FOUND_CURRENCY_RATE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		debetAmount = debetAmount.multiply(conversionRate);
		BigDecimal currentBalance = oldBalance.subtract(debetAmount);
		String currentBalanceCheck = this.currentBalanceCheck(currentBalance, accountInquiryResponse.getLevel());
		if (!currentBalanceCheck.equals(rc)){
			rc = currentBalanceCheck;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		account.setBalance(currentBalance);
		String currentCurrency = account.getCurrency();
		String description = BaseConstant.DEBET_CODE +" "+transactionUpdateRequest.getDescription();
		String refnum = transactionUpdateRequest.getReferenceID();
		Merchant merchant = merchantRepository.findOne(transactionUpdateRequest.getMerchantCode());
		if (merchant == null) {
			rc = FrontControllerConstant.NOT_FOUND_MERCHANT_CODE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		TrxCode trxCode = trxCodeRepository.findOne(transactionUpdateRequest.getTrxCode());
		if (trxCode == null) {
			rc = FrontControllerConstant.NOT_FOUND_TRX_CODE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		Balance balance = new Balance(
				oldBalance,
				oldCurrency,
				currentBalance,
				currentCurrency,
				new BigDecimal(0),
				currentCurrency,
				debetAmount,
				debetCurrency,
				BALANCE_STATUS.NEW.toString(),
				refnum,
				description,
				account,
				transactionUpdateRequest.getType(),
				merchant,
				trxCode
		);
		
		transactionCorDResponse.setResponseCode(rc);
		transactionCorDResponse.setBalance(balance);
		return transactionCorDResponse;
	}

	private String currentBalanceCheck(BigDecimal currentBalance, String level) {
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		BigDecimal maxBalance = maxBasicBalance;
		BigDecimal minBalance = minBasicBalance;
		if (level.equals(ACCOUNT_LEVEL.REGULAR.toString())){
			maxBalance = maxRegBalance;
			minBalance = minRegBalance;
		}else if (level.equals(ACCOUNT_LEVEL.REGULAR.toString())){
			minBalance = minPlatinumBalance;
		} else if (level.equals(ACCOUNT_LEVEL.CORPORATE.toString())){
			maxBalance = maxCorpBalance;
			minBalance = minCorpBalance;
		}
		
		if(!level.equals(ACCOUNT_LEVEL.PLATINUM.toString()) && !level.equals(ACCOUNT_LEVEL.CORPORATE.toString())){
			if (currentBalance.compareTo(maxBalance) == 1){
				rc = FrontControllerConstant.RC_EXCEED_MAX_BALANCE;
			}
		}
		
		if (currentBalance.compareTo(minBalance) == -1){
			rc = FrontControllerConstant.RC_LESS_MIN_BALANCE;
		}
		return rc;
	}

	@Transactional(readOnly = true)
	private TransactionCorDResponse creditAccount(
			TransactionUpdateRequest transactionUpdateRequest) throws LimitCreditException {
		TransactionCorDResponse transactionCorDResponse = new TransactionCorDResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(transactionUpdateRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionCorDResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionCorDResponse;
		}
		
		BigDecimal creditAmount = new BigDecimal(transactionUpdateRequest.getAmount());
		creditAmount = creditAmount.divide(new BigDecimal(100));
		
		//check limit credit max per month
		if(!transactionUpdateRequest.getDescription().contains(Balance.BALANCE_STATUS.REVERSAL.toString())
				&& balanceService.isMonthlyLimitCreditExceeded(accountInquiryRequest.getCif(), accountInquiryResponse.getLevel(), creditAmount)){
			//to rollback transaction
			throw new LimitCreditException();
		}
		
		transactionCorDResponse.setAccountName(accountInquiryResponse.getName());
		Account account = accountService.findByAccountNo(transactionUpdateRequest.getCif());
		BigDecimal oldBalance = account.getBalance();
		String oldCurrency = account.getCurrency();
		
		String creditCurrency = transactionUpdateRequest.getCurrency();
		if (creditAmount.compareTo(new BigDecimal(0)) == -1){
			rc = FrontControllerConstant.RC_NEGATIVE_BALANCE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		BigDecimal conversionRate = currencyRateRepository.findConvertionRate(creditCurrency,oldCurrency);
		if (conversionRate == null){
			rc = FrontControllerConstant.NOT_FOUND_CURRENCY_RATE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		creditAmount = creditAmount.multiply(conversionRate);
		BigDecimal currentBalance = oldBalance.add(creditAmount);
		String currentBalanceCheck = this.currentBalanceCheck(currentBalance, accountInquiryResponse.getLevel());
		if (!currentBalanceCheck.equals(rc)){
			rc = currentBalanceCheck;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		account.setBalance(currentBalance);
		String currentCurrency = account.getCurrency();
		String description = BaseConstant.CREDIT_CODE +" "+transactionUpdateRequest.getDescription();
		String refnum = transactionUpdateRequest.getReferenceID();
		Merchant merchant = merchantRepository.findOne(transactionUpdateRequest.getMerchantCode());
		if (merchant == null) {
			rc = FrontControllerConstant.NOT_FOUND_MERCHANT_CODE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		TrxCode trxCode = trxCodeRepository.findOne(transactionUpdateRequest.getTrxCode());
		if (trxCode == null) {
			rc = FrontControllerConstant.NOT_FOUND_TRX_CODE;
			transactionCorDResponse.setResponseCode(rc);
			return transactionCorDResponse;
		}
		Balance balance = new Balance(
				oldBalance,
				oldCurrency,
				currentBalance,
				currentCurrency,
				creditAmount,
				creditCurrency,
				new BigDecimal(0),
				currentCurrency,
				BALANCE_STATUS.NEW.toString(),
				refnum,
				description,
				account,
				transactionUpdateRequest.getType(),
				merchant,
				trxCode
		);
		
		transactionCorDResponse.setResponseCode(rc);
		transactionCorDResponse.setBalance(balance);
		return transactionCorDResponse;
	}

	@Transactional(readOnly = true)
	@Override
	public TransferOnUsResponseInq transferOnUsInquiry(
			TransferOnUsRequestInq transferOnUsRequestInq) {
		TransferOnUsResponseInq transferOnUsResponseInq = new TransferOnUsResponseInq();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Source Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(transferOnUsRequestInq.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			transferOnUsResponseInq.setAccountFromName(accountInquiryResponse.getName());
			// Inquiry Account Source Request
			accountInquiryRequest.setCif(transferOnUsRequestInq.getAccountTo());
			accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
			if (accountInquiryResponse.getResponseCode().equals(rc)){
				transferOnUsResponseInq.setAccountTo(accountInquiryResponse.getAccountNo());
				transferOnUsResponseInq.setAccountToName(accountInquiryResponse.getName());
				transferOnUsResponseInq.setAmount(transferOnUsRequestInq.getAmount());
				transferOnUsResponseInq.setCurrency(transferOnUsRequestInq.getCurrency());
				transferOnUsResponseInq.setType(transferOnUsRequestInq.getType());
				transferOnUsResponseInq.setDescription(transferOnUsRequestInq.getDescription());
			} else {
				rc = accountInquiryResponse.getResponseCode();
				rc = this.mapRCforAccountTo(rc);
			}
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		transferOnUsResponseInq.setCif(transferOnUsRequestInq.getCif());
		transferOnUsResponseInq.setReferenceID(transferOnUsRequestInq.getReferenceID());
		transferOnUsResponseInq.setResponseCode(rc);
		return transferOnUsResponseInq;
	}

	private String mapRCforAccountTo(String rc) {
		if (rc.equals(FrontControllerConstant.RC_BLOCKED_ACCOUNT)){
			return FrontControllerConstant.RC_BLOCKED_ACCOUNT_TO;
		} else if (rc.equals(FrontControllerConstant.RC_CLOSED_ACCOUNT)){
			return FrontControllerConstant.RC_CLOSED_ACCOUNT_TO;
		} else if (rc.equals(FrontControllerConstant.RC_INVALID_ACCOUNT)){
			return FrontControllerConstant.RC_INVALID_ACCOUNT_TO;
		} else if (rc.equals(FrontControllerConstant.RC_EXCEED_MAX_BALANCE)){
			return FrontControllerConstant.RC_EXCEED_MAX_BALANCE_TO;
		} else {
			return rc;
		}
	}

	@Transactional(readOnly = false, rollbackFor=Exception.class)
	@Override
	public TransferOnUsResponseExec transferOnUsExecute(
			TransferOnUsRequestExec transferOnUsRequestExec) throws LimitCreditException, TransactionException {
		TransferOnUsResponseExec transferOnUsResponseExec = new TransferOnUsResponseExec();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Debet account source
		TransactionUpdateRequest transactionUpdateRequest = new TransactionUpdateRequest();
		transactionUpdateRequest.setCif(transferOnUsRequestExec.getCif());
		transactionUpdateRequest.setType(TRX_TYPE.DEBET.toString());
		transactionUpdateRequest.setCurrency(transferOnUsRequestExec.getCurrency());
		transactionUpdateRequest.setAmount(transferOnUsRequestExec.getAmount());
		transactionUpdateRequest.setDescription(transferOnUsRequestExec.getDescription()+" TO "+transferOnUsRequestExec.getAccountToName().toUpperCase());
		transactionUpdateRequest.setReferenceID(transferOnUsRequestExec.getReferenceID());
		transactionUpdateRequest.setMerchantCode(BaseConstant.MERCHANT_CODE_DEFAULT);
		transactionUpdateRequest.setTrxCode(BaseConstant.TRX_CODE_DEFAULT);
		TransactionCorDResponse transactionCorDResponse = this.debetAccount(transactionUpdateRequest);
		if (transactionCorDResponse.getResponseCode().equals(rc)){
			Balance balanceDebet = transactionCorDResponse.getBalance();
			balanceDebet.setStatus(BALANCE_STATUS.POSTED.toString());
			// Credit account source
			transactionUpdateRequest.setCif(transferOnUsRequestExec.getAccountTo());
			transactionUpdateRequest.setType(TRX_TYPE.CREDIT.toString());
			transactionUpdateRequest.setDescription(transferOnUsRequestExec.getDescription()+" FR "+transferOnUsRequestExec.getAccountFromName().toUpperCase());
			transactionCorDResponse = this.creditAccount(transactionUpdateRequest);
			if (transactionCorDResponse.getResponseCode().equals(rc)){
				Balance balanceCredit = transactionCorDResponse.getBalance();
				balanceCredit.setStatus(BALANCE_STATUS.POSTED.toString());
				// save debet and credit balance
				List<Balance> blncIterable = new ArrayList<Balance>();
				blncIterable.add(balanceDebet);
				blncIterable.add(balanceCredit);
				balanceService.saveBatch(blncIterable);
//				balanceService.save(balanceDebet);
//				balanceService.save(balanceCredit);
			} else {
				rc = transactionCorDResponse.getResponseCode();
				rc = this.mapRCforAccountTo(rc);
				throw new TransactionException(rc);
			}
		} else {
			rc = transactionCorDResponse.getResponseCode();
		}
		
		transferOnUsResponseExec.setCif(transferOnUsRequestExec.getCif());
		transferOnUsResponseExec.setReferenceID(transferOnUsRequestExec.getReferenceID());
		transferOnUsResponseExec.setResponseCode(rc);
		return transferOnUsResponseExec;
	}
	
	@Transactional(readOnly = false, rollbackFor=Exception.class)
	@Override
	public TransferOtherResponseExec transferOtherExecute(
			TransferOtherRequestExec otherRequestExec)
			throws LimitCreditException, TransactionException, Exception {
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		TransferOtherResponseExec otherResponseExec = new TransferOtherResponseExec();
		TransferOnUsRequestExec onUsRequestExec = new TransferOnUsRequestExec();
		ServiceUtil.cloneObject(otherRequestExec, onUsRequestExec);
		//for transfer user to corp
		//TransferOnUsResponseExec onUsResponseExec = transferOnUsExecute(onUsRequestExec);
		
		Map<String,String> accountResponses = new HashMap<>();
		//if(onUsResponseExec.getResponseCode().equals(rc)){
			for (AccountTransferRequest acct : otherRequestExec.getAccountTransfers()) {
				//for admin purpose
				String srcName = accountService.getNameByAccountNo(acct.getAccountSrc());
				String destName = accountService.getNameByAccountNo(acct.getAccountDest());
				onUsRequestExec.setAccountFromName(srcName);
				onUsRequestExec.setAccountTo(acct.getAccountDest());
				onUsRequestExec.setAccountToName(destName);
				onUsRequestExec.setAmount(acct.getAmount());
				onUsRequestExec.setCif(acct.getAccountSrc());
				TransferOnUsResponseExec adminResponseExec = transferOnUsExecute(onUsRequestExec);
				if(!adminResponseExec.getResponseCode().equals(rc)){
					rc = adminResponseExec.getResponseCode();
					throw new TransactionException(rc);
				}
				AccountTransferResponse acctResp = new AccountTransferResponse();
				ServiceUtil.cloneObject(acct, acctResp);
				acctResp.setAccountDestName(destName);
				acctResp.setAccountSrcName(srcName);
				accountResponses.put(acct.getAccountSrc(), srcName);
				accountResponses.put(acct.getAccountDest(), destName);
			}
//		}else{
//			rc = onUsResponseExec.getResponseCode();
//			throw new TransactionException(rc);
//		}
//		
		otherResponseExec.setAccountResponses(accountResponses);
		otherResponseExec.setCif(otherRequestExec.getCif());
		otherResponseExec.setResponseCode(rc);
		otherResponseExec.setReferenceID(otherRequestExec.getReferenceID());
		
		return otherResponseExec;
	}

	@Transactional(readOnly = false)
	@Override
	public ReversalTransactionResponse reversalTransaction(
			ReversalTransactionRequest reversalTransactionRequest) throws Exception {
		ReversalTransactionResponse reversalTransactionResponse = new ReversalTransactionResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		List<Balance> balaceReversals = balanceService.findAllbyRefNo(reversalTransactionRequest.getReferenceID());
		TransactionUpdateRequest transactionUpdateRequest = new TransactionUpdateRequest();
		TransactionCorDResponse transactionCorDResponse = new TransactionCorDResponse();
		if (balaceReversals.size() > 0){
			for (Balance balance : balaceReversals) {
				if (balance.getType().equals(TRX_TYPE.CREDIT.toString())){
					transactionUpdateRequest.setCif(balance.getAccount().getAccountNo());
					transactionUpdateRequest.setType(TRX_TYPE.DEBET.toString());
					transactionUpdateRequest.setCurrency(balance.getCreditCurrency());
					transactionUpdateRequest.setAmount(balance.getCreditAmount().multiply(new BigDecimal(100)).toString());
					transactionUpdateRequest.setDescription(balance.getDescription()+" "+BALANCE_STATUS.REVERSAL.toString());
					transactionUpdateRequest.setReferenceID(balance.getRefnum());
					transactionUpdateRequest.setMerchantCode(balance.getMerchant().getCode());
					transactionUpdateRequest.setTrxCode(balance.getTrxCode().getCode());
					transactionCorDResponse = this.debetAccount(transactionUpdateRequest);
					if (transactionCorDResponse.getResponseCode().equals(rc)){
						Balance balanceRev = transactionCorDResponse.getBalance();
						balanceRev.setStatus(BALANCE_STATUS.REVERSAL.toString());
						balance.setStatus(BALANCE_STATUS.REVERSAL.toString());
						List<Balance> blncIterable = new ArrayList<Balance>();
						blncIterable.add(balanceRev);
						blncIterable.add(balance);
						balanceService.saveBatch(blncIterable);
//						balanceService.save(balanceRev);
//						balanceService.save(balance);
					} else {
						rc = transactionCorDResponse.getResponseCode();
					}
				} else if (balance.getType().equals(TRX_TYPE.DEBET.toString())){
					transactionUpdateRequest.setCif(balance.getAccount().getAccountNo());
					transactionUpdateRequest.setType(TRX_TYPE.CREDIT.toString());
					transactionUpdateRequest.setCurrency(balance.getDebetCurrency());
					transactionUpdateRequest.setAmount(balance.getDebetAmount().multiply(new BigDecimal(100)).toString());
					transactionUpdateRequest.setDescription(balance.getDescription()+" "+BALANCE_STATUS.REVERSAL.toString());
					transactionUpdateRequest.setReferenceID(balance.getRefnum());
					transactionUpdateRequest.setMerchantCode(balance.getMerchant().getCode());
					transactionUpdateRequest.setTrxCode(balance.getTrxCode().getCode());
					transactionCorDResponse = this.creditAccount(transactionUpdateRequest);
					if (transactionCorDResponse.getResponseCode().equals(rc)){
						Balance balanceRev = transactionCorDResponse.getBalance();
						balanceRev.setStatus(BALANCE_STATUS.REVERSAL.toString());
						balance.setStatus(BALANCE_STATUS.REVERSAL.toString());
						List<Balance> blncIterable = new ArrayList<Balance>();
						blncIterable.add(balanceRev);
						blncIterable.add(balance);
						balanceService.saveBatch(blncIterable);
//						balanceService.save(balanceRev);
//						balanceService.save(balance);
					} else {
						rc = transactionCorDResponse.getResponseCode();
					}
				} else {
					rc = FrontControllerConstant.INVALID_ARGUMENT;
				}
			}
		} else {
			rc = FrontControllerConstant.REVERSAL_NOT_FOUND;
		}
		reversalTransactionResponse.setCif(reversalTransactionRequest.getCif());
		reversalTransactionResponse.setReferenceID(reversalTransactionRequest.getReferenceID());
		reversalTransactionResponse.setResponseCode(rc);
		return reversalTransactionResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public HoldingBalanceUpdateResponse updateHoldingBalance(
			HoldingBalanceUpdateRequest holdingBalanceUpdateRequest) {
		HoldingBalanceUpdateResponse holdingBalanceUpdateResponse = new HoldingBalanceUpdateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (holdingBalanceUpdateRequest.getType().equals(TRXHOLD_TYPE.HOLD.toString())){
			TransactionHoldResponse transactionHoldResponse = this.holdBalance(holdingBalanceUpdateRequest);
			if (transactionHoldResponse.getResponseCode().equals(rc)){
				holdingBalanceUpdateResponse.setAccountName(transactionHoldResponse.getAccountName());
				holdingBalanceService.save(transactionHoldResponse.getHoldingBalance());
			} else {
				rc = transactionHoldResponse.getResponseCode();
			}
		} else {
			rc = FrontControllerConstant.INVALID_ARGUMENT;
		}
		holdingBalanceUpdateResponse.setCif(holdingBalanceUpdateRequest.getCif());
		holdingBalanceUpdateResponse.setReferenceID(holdingBalanceUpdateRequest.getReferenceID());
		holdingBalanceUpdateResponse.setResponseCode(rc);
		return holdingBalanceUpdateResponse;
	}

	@Transactional(readOnly = true)
	private TransactionHoldResponse holdBalance(
			HoldingBalanceUpdateRequest holdingBalanceUpdateRequest) {
		TransactionHoldResponse transactionHoldResponse = new TransactionHoldResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(holdingBalanceUpdateRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionHoldResponse;
		}
		
		HoldingBalance holdingBalances = holdingBalanceService.findbyRefNo(holdingBalanceUpdateRequest.getReferenceID(), holdingBalanceUpdateRequest.getCif());
		if(holdingBalances != null){
			transactionHoldResponse.setResponseCode(FrontControllerConstant.RC_REFCODE_ALREADY_EXISTS);
			return transactionHoldResponse;
		}
		
		transactionHoldResponse.setAccountName(accountInquiryResponse.getName());
		BigDecimal availableBalance = new BigDecimal(accountInquiryResponse.getNominal());
		BigDecimal debetAmount = new BigDecimal(holdingBalanceUpdateRequest.getAmount());
		debetAmount = debetAmount.divide(new BigDecimal(100));
		String debetCurrency = holdingBalanceUpdateRequest.getCurrency();
		
		if (debetAmount.compareTo(new BigDecimal(0)) == -1){
			rc = FrontControllerConstant.RC_NEGATIVE_BALANCE;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		
		if (availableBalance.compareTo(debetAmount) == -1){
			rc = FrontControllerConstant.BALANCE_NOT_ENOUGH;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		Account account = accountService.findByAccountNo(holdingBalanceUpdateRequest.getCif());
		BigDecimal conversionRate = currencyRateRepository.findConvertionRate(debetCurrency, account.getCurrency());
		if (conversionRate == null){
			rc = FrontControllerConstant.NOT_FOUND_CURRENCY_RATE;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		debetAmount = debetAmount.multiply(conversionRate);
		BigDecimal currentBalance = availableBalance.subtract(debetAmount);
		String currentBalanceCheck = this.currentBalanceCheck(currentBalance, accountInquiryResponse.getLevel());
		if (!currentBalanceCheck.equals(rc)){
			rc = currentBalanceCheck;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		String description = holdingBalanceUpdateRequest.getDescription();
		String refnum = holdingBalanceUpdateRequest.getReferenceID();
		Merchant merchant = merchantRepository.findOne(holdingBalanceUpdateRequest.getMerchantCode());
		if (merchant == null) {
			rc = FrontControllerConstant.NOT_FOUND_MERCHANT_CODE;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		TrxCode trxCode = trxCodeRepository.findOne(holdingBalanceUpdateRequest.getTrxCode());
		if (trxCode == null) {
			rc = FrontControllerConstant.NOT_FOUND_TRX_CODE;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		HoldingBalance holdingBalance = new HoldingBalance(
				debetAmount,
				debetCurrency,
				refnum,
				description,
				account,
				holdingBalanceUpdateRequest.getPurposeId(),
				merchant,
				trxCode,
				BALANCEHOLD_STATUS.HOLD.toString()
			);
		
		transactionHoldResponse.setResponseCode(rc);
		transactionHoldResponse.setHoldingBalance(holdingBalance);
		return transactionHoldResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public CancelHoldingBalanceResponse cancelHoldingBalance(
			CancelHoldingBalanceRequest cancelHoldingBalanceRequest) throws ParseException {
		CancelHoldingBalanceResponse cancelHoldingBalanceResponse = new CancelHoldingBalanceResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		HoldingBalance holdingBalance = holdingBalanceService.findbyRefNo(cancelHoldingBalanceRequest.getReferenceID(),
				cancelHoldingBalanceRequest.getCif());
		if (holdingBalance != null){
			holdingBalance.setStatus(BALANCEHOLD_STATUS.CANCELED.toString());
			holdingBalanceService.save(holdingBalance);
		} else {
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
		}
		cancelHoldingBalanceResponse.setCif(cancelHoldingBalanceRequest.getCif());
		cancelHoldingBalanceResponse.setReferenceID(cancelHoldingBalanceRequest.getReferenceID());
		cancelHoldingBalanceResponse.setResponseCode(rc);
		return cancelHoldingBalanceResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public HoldingBalanceReleaseResponse releaseHoldingBalance(
			HoldingBalanceReleaseRequest holdingBalanceReleaseRequest) {
		HoldingBalanceReleaseResponse holdingBalanceReleaseResponse = new HoldingBalanceReleaseResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		TransactionHoldResponse transactionHoldResponse = this.releaseBalance(holdingBalanceReleaseRequest);
		if (transactionHoldResponse.getResponseCode().equals(rc)){
			holdingBalanceReleaseResponse.setAccountName(transactionHoldResponse.getAccountName());
			Iterable<HoldingBalance> saveHoldBalances = transactionHoldResponse.getHoldingBalances();
			holdingBalanceService.saveBatch(saveHoldBalances);
			HoldingBalance holdingBalance = transactionHoldResponse.getHoldingBalances().get(0);
			Transaction trx = new Transaction(
					holdingBalance.getRefnum(),
					holdingBalance.getStatus(),
					holdingBalance.getDebetAmount().toString(), 
					holdingBalance.getCreateDate(),
					holdingBalance.getDescription()
			);
			holdingBalanceReleaseResponse.setTransaction(trx);
		} else {
			rc = transactionHoldResponse.getResponseCode();
		}
		
		holdingBalanceReleaseResponse.setCif(holdingBalanceReleaseRequest.getCif());
		holdingBalanceReleaseResponse.setReferenceID(holdingBalanceReleaseRequest.getReferenceID());
		holdingBalanceReleaseResponse.setResponseCode(rc);
		return holdingBalanceReleaseResponse;
	}

	@Transactional(readOnly = true)
	private TransactionHoldResponse releaseBalance(
			HoldingBalanceReleaseRequest holdingBalanceReleaseRequest) {
		TransactionHoldResponse transactionHoldResponse = new TransactionHoldResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(holdingBalanceReleaseRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionHoldResponse;
		}
		transactionHoldResponse.setAccountName(accountInquiryResponse.getName());
		
		HoldingBalance holdingBalances = holdingBalanceService.findbyRefNo(holdingBalanceReleaseRequest.getReferenceID(), holdingBalanceReleaseRequest.getCif());
//				findAll(holdingBalanceReleaseRequest.getCif(),
//				holdingBalanceReleaseRequest.getPurposeId());
		if (holdingBalances == null) {
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}else if(!holdingBalances.getStatus().equals(HoldingBalance.BALANCEHOLD_STATUS.HOLD.toString())){
			rc = FrontControllerConstant.RC_HOLD_BALANCE_ALREADY_RELEASED;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		List<HoldingBalance> upadateHoldingBalances = new ArrayList<HoldingBalance>();
		holdingBalances.setStatus(BALANCEHOLD_STATUS.RELEASED.toString());
		upadateHoldingBalances.add(holdingBalances);
		transactionHoldResponse.setHoldingBalances(upadateHoldingBalances);
		transactionHoldResponse.setResponseCode(rc);
		return transactionHoldResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public HoldingBalanceReleaseResponse releaseHoldingBalanceTrf(
			HoldingBalanceReleaseRequest holdingBalanceReleaseRequest) {
		HoldingBalanceReleaseResponse holdingBalanceReleaseResponse = new HoldingBalanceReleaseResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		TransactionHoldResponse transactionHoldResponse = this.releaseBalanceTrf(holdingBalanceReleaseRequest);
		if (transactionHoldResponse.getResponseCode().equals(rc)){
			holdingBalanceReleaseResponse.setAccountName(transactionHoldResponse.getAccountName());
			holdingBalanceService.save(transactionHoldResponse.getHoldingBalance());
			balanceService.save(transactionHoldResponse.getBalance());
		} else {
			rc = transactionHoldResponse.getResponseCode();
		}
		
		holdingBalanceReleaseResponse.setCif(holdingBalanceReleaseRequest.getCif());
		holdingBalanceReleaseResponse.setReferenceID(holdingBalanceReleaseRequest.getReferenceID());
		holdingBalanceReleaseResponse.setResponseCode(rc);
		return holdingBalanceReleaseResponse;
	}

	@Transactional(readOnly = true)
	private TransactionHoldResponse releaseBalanceTrf(
			HoldingBalanceReleaseRequest holdingBalanceReleaseRequest) {
		TransactionHoldResponse transactionHoldResponse = new TransactionHoldResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(holdingBalanceReleaseRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionHoldResponse;
		}
		transactionHoldResponse.setAccountName(accountInquiryResponse.getName());
		
		HoldingBalance holdingBalance = holdingBalanceService.findbyRefNo(holdingBalanceReleaseRequest.getReferenceID(),
				holdingBalanceReleaseRequest.getCif());
		if (holdingBalance == null) {
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		holdingBalance.setStatus(BALANCEHOLD_STATUS.RELEASED.toString());
		
		TransactionUpdateRequest transactionUpdateRequest = new TransactionUpdateRequest();
		transactionUpdateRequest.setCif(holdingBalance.getAccount().getAccountNo());
		transactionUpdateRequest.setType(TRX_TYPE.DEBET.toString());
		transactionUpdateRequest.setCurrency(holdingBalance.getDebetCurrency());
		transactionUpdateRequest.setAmount(holdingBalance.getDebetAmount().multiply(new BigDecimal(100)).toString());
		transactionUpdateRequest.setDescription(holdingBalance.getDescription().toUpperCase());
		transactionUpdateRequest.setReferenceID(holdingBalance.getRefnum());
		transactionUpdateRequest.setMerchantCode(BaseConstant.MERCHANT_CODE_DEFAULT);
		transactionUpdateRequest.setTrxCode(BaseConstant.TRX_CODE_DEFAULT);
		TransactionCorDResponse transactionCorDResponse = this.debetAccount(transactionUpdateRequest);
		if (!transactionCorDResponse.getResponseCode().equals(rc)){
			rc = transactionCorDResponse.getResponseCode();
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		
		transactionHoldResponse.setHoldingBalance(holdingBalance);
		transactionHoldResponse.setBalance(transactionCorDResponse.getBalance());
		transactionHoldResponse.setResponseCode(rc);
		return transactionHoldResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public ReversalHoldingBalanceResponse reversalHoldingBalance(
			ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) {
		ReversalHoldingBalanceResponse reversalHoldingBalanceResponse = new ReversalHoldingBalanceResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		TransactionHoldResponse transactionHoldResponse = this.reversalBalance(reversalHoldingBalanceRequest);
		if (transactionHoldResponse.getResponseCode().equals(rc)){
			Iterable<HoldingBalance> saveHoldBalances = transactionHoldResponse.getHoldingBalances();
			holdingBalanceService.saveBatch(saveHoldBalances);
		} else {
			rc = transactionHoldResponse.getResponseCode();
		}
		reversalHoldingBalanceResponse.setCif(reversalHoldingBalanceRequest.getCif());
		reversalHoldingBalanceResponse.setReferenceID(reversalHoldingBalanceRequest.getReferenceID());
		reversalHoldingBalanceResponse.setResponseCode(rc);
		return reversalHoldingBalanceResponse;
	}

	@Transactional(readOnly = true)
	private TransactionHoldResponse reversalBalance(
			ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) {
		TransactionHoldResponse transactionHoldResponse = new TransactionHoldResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(reversalHoldingBalanceRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionHoldResponse;
		}
		transactionHoldResponse.setAccountName(accountInquiryResponse.getName());
		
		List<HoldingBalance> holdingBalances = holdingBalanceService.findAll(reversalHoldingBalanceRequest.getCif(),
				reversalHoldingBalanceRequest.getPurposeId(), BALANCEHOLD_STATUS.RELEASED.toString());
		if (holdingBalances.size() == 0) {
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		List<HoldingBalance> upadateHoldingBalances = new ArrayList<HoldingBalance>();
		for (HoldingBalance holdingBalance : holdingBalances) {
			holdingBalance.setStatus(BALANCEHOLD_STATUS.HOLD.toString());
			upadateHoldingBalances.add(holdingBalance);
		}
		transactionHoldResponse.setHoldingBalances(upadateHoldingBalances);
		transactionHoldResponse.setResponseCode(rc);
		return transactionHoldResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public ReversalHoldingBalanceResponse reversalHoldingBalanceTrf(
			ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) throws LimitCreditException {
		ReversalHoldingBalanceResponse reversalBalanceResponse = new ReversalHoldingBalanceResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		TransactionHoldResponse transactionHoldResponse = this.reversalBalanceTrf(reversalHoldingBalanceRequest);
		if (transactionHoldResponse.getResponseCode().equals(rc)){
			List<Balance> blncIterable = new ArrayList<Balance>();
			blncIterable.add(transactionHoldResponse.getBalance());
			blncIterable.add(transactionHoldResponse.getBalanceRev());
			balanceService.saveBatch(blncIterable);
			holdingBalanceService.save(transactionHoldResponse.getHoldingBalance());
//			balanceService.save(transactionHoldResponse.getBalance());
//			balanceService.save(transactionHoldResponse.getBalanceRev());
		} else {
			rc = transactionHoldResponse.getResponseCode();
		}
		
		reversalBalanceResponse.setCif(reversalHoldingBalanceRequest.getCif());
		reversalBalanceResponse.setReferenceID(reversalHoldingBalanceRequest.getReferenceID());
		reversalBalanceResponse.setResponseCode(rc);
		return reversalBalanceResponse;
	}

	@Transactional(readOnly = true)
	private TransactionHoldResponse reversalBalanceTrf(
			ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) throws LimitCreditException {
		TransactionHoldResponse transactionHoldResponse = new TransactionHoldResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(reversalHoldingBalanceRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (!accountInquiryResponse.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountInquiryResponse.getResponseCode());
			return transactionHoldResponse;
		}
		transactionHoldResponse.setAccountName(accountInquiryResponse.getName());
		
		HoldingBalance holdingBalance = holdingBalanceService.findbyRefNo(reversalHoldingBalanceRequest.getReferenceID(),
				reversalHoldingBalanceRequest.getCif());
		if (holdingBalance == null) {
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		holdingBalance.setStatus(BALANCEHOLD_STATUS.HOLD.toString());
		
		List<Balance> balances = balanceService.findAllbyRefNo(reversalHoldingBalanceRequest.getReferenceID());
		if (balances.size() == 0){
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		
		Balance balance = balances.get(0);
		TransactionUpdateRequest transactionUpdateRequest = new TransactionUpdateRequest();
		TransactionCorDResponse transactionCorDResponse = new TransactionCorDResponse();
		transactionUpdateRequest.setCif(balance.getAccount().getAccountNo());
		transactionUpdateRequest.setType(TRX_TYPE.CREDIT.toString());
		transactionUpdateRequest.setCurrency(balance.getDebetCurrency());
		transactionUpdateRequest.setAmount(balance.getDebetAmount().multiply(new BigDecimal(100)).toString());
		transactionUpdateRequest.setDescription(balance.getDescription()+" "+BALANCE_STATUS.REVERSAL.toString());
		transactionUpdateRequest.setReferenceID(balance.getRefnum());
		transactionUpdateRequest.setMerchantCode(balance.getMerchant().getCode());
		transactionUpdateRequest.setTrxCode(balance.getTrxCode().getCode());
		transactionCorDResponse = this.creditAccount(transactionUpdateRequest);
		if (!transactionCorDResponse.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		
		Balance balanceRev = transactionCorDResponse.getBalance();
		balanceRev.setStatus(BALANCE_STATUS.REVERSAL.toString());
		balance.setStatus(BALANCE_STATUS.REVERSAL.toString());
		transactionHoldResponse.setBalanceRev(balanceRev);
		transactionHoldResponse.setBalance(balance);
		transactionHoldResponse.setHoldingBalance(holdingBalance);
		transactionHoldResponse.setBalance(transactionCorDResponse.getBalance());
		transactionHoldResponse.setResponseCode(rc);
		return transactionHoldResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public HoldingBalanceHistoryResponse inquiryHoldingBalanceHistory(
			HoldingBalanceHistoryRequest historyRequest) {
		HoldingBalanceHistoryResponse historyResponse = new HoldingBalanceHistoryResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(historyRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			List<HoldingBalance> hBalances = holdingBalanceService.findByAccountWithPaging(historyRequest.getQty(), historyRequest.getOrder(),
					historyRequest.getCif(), historyRequest.getPurposeId());
			historyResponse.setTransactions(this.getHistoryHoldingBalanceTransaction(hBalances));
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		historyResponse.setCif(historyRequest.getCif());
		historyResponse.setReferenceID(historyRequest.getReferenceID());
		historyResponse.setResponseCode(rc);
		return historyResponse;
	}

	@Transactional(readOnly = true)
	private List<Transaction> getHistoryHoldingBalanceTransaction(
			List<HoldingBalance> hBalances) {
		List<Transaction> transactions = new ArrayList<Transaction>();
		for (HoldingBalance hBalance : hBalances) {
			Transaction transaction = new Transaction(
					hBalance.getRefnum(),
					hBalance.getStatus(),
					hBalance.getDebetAmount().toString(), 
					hBalance.getCreateDate(),
					hBalance.getDescription()
			);
			transactions.add(transaction);
		}
		return transactions;
	}

	@Transactional(readOnly = false)
	@Override
	public HoldingBalanceHistoryByDateResponse inquiryHoldingBalanceHistoryByDate(
			HoldingBalanceHistoryByDateRequest historyRequest) {
		HoldingBalanceHistoryByDateResponse historyResponse = new HoldingBalanceHistoryByDateResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account Request
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(historyRequest.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			List<HoldingBalance> hBalances = holdingBalanceService.findByAccountWithPaging(historyRequest.getQty(), historyRequest.getOrder(),
					historyRequest.getDateFrom(), historyRequest.getDateTo(), historyRequest.getCif(), historyRequest.getPurposeId());
			historyResponse.setTransactions(this.getHistoryHoldingBalanceTransaction(hBalances));
		} else {
			rc = accountInquiryResponse.getResponseCode();
		}
		historyResponse.setCif(historyRequest.getCif());
		historyResponse.setReferenceID(historyRequest.getReferenceID());
		historyResponse.setResponseCode(rc);
		return historyResponse;
	}

	@Transactional(readOnly = false)
	@Override
	public HoldingBalanceReleaseTopUpResponse releaseHoldingBalanceTrfTopUp(
			HoldingBalanceReleaseTopUpRequest holdingBalanceReleaseRequest) throws LimitCreditException {
		HoldingBalanceReleaseTopUpResponse holdingBalanceReleaseResponse = new HoldingBalanceReleaseTopUpResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		TransactionHoldResponse transactionHoldResponse = this.releaseBalanceTrfTopUp(holdingBalanceReleaseRequest);
		if (transactionHoldResponse.getResponseCode().equals(rc)){
			holdingBalanceReleaseResponse.setAccountName(transactionHoldResponse.getAccountName());
			List<Balance> blncIterable = new ArrayList<Balance>();
			blncIterable.add(transactionHoldResponse.getBalance());
			blncIterable.add(transactionHoldResponse.getBalanceRev());
			balanceService.saveBatch(blncIterable);
			holdingBalanceService.save(transactionHoldResponse.getHoldingBalance());
//			balanceService.save(transactionHoldResponse.getBalance());
//			balanceService.save(transactionHoldResponse.getBalanceRev());
		} else {
			rc = transactionHoldResponse.getResponseCode();
		}
		
		holdingBalanceReleaseResponse.setCif(holdingBalanceReleaseRequest.getCif());
		holdingBalanceReleaseResponse.setReferenceID(holdingBalanceReleaseRequest.getReferenceID());
		holdingBalanceReleaseResponse.setResponseCode(rc);
		return holdingBalanceReleaseResponse;
	}

	@Transactional(readOnly = true)
	private TransactionHoldResponse releaseBalanceTrfTopUp(
			HoldingBalanceReleaseTopUpRequest holdingBalanceReleaseRequest) throws LimitCreditException {
		TransactionHoldResponse transactionHoldResponse = new TransactionHoldResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		// Inquiry Account From Request
		AccountInquiryRequest accountFromReq = new AccountInquiryRequest();
		accountFromReq.setCif(holdingBalanceReleaseRequest.getCif());
		AccountInquiryResponse accountFromRes = accountService.inquiryAccount(accountFromReq);
		if (!accountFromRes.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountFromRes.getResponseCode());
			return transactionHoldResponse;
		}
		transactionHoldResponse.setAccountName(accountFromRes.getName());
		// Inquiry Account To Request
		AccountInquiryRequest accountTo = new AccountInquiryRequest();
		accountTo.setCif(holdingBalanceReleaseRequest.getAccountTo());
		AccountInquiryResponse accountToRes = accountService.inquiryAccount(accountTo);
		if (!accountToRes.getResponseCode().equals(rc)){
			transactionHoldResponse.setResponseCode(accountToRes.getResponseCode());
			return transactionHoldResponse;
		}
		HoldingBalance holdingBalance = holdingBalanceService.findbyRefNo(holdingBalanceReleaseRequest.getReferenceID(),
				holdingBalanceReleaseRequest.getCif());
		if (holdingBalance == null) {
			rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		holdingBalance.setStatus(BALANCEHOLD_STATUS.RELEASED.toString());
		
		// Debet account source
		TransactionUpdateRequest transactionUpdateRequest = new TransactionUpdateRequest();
		transactionUpdateRequest.setCif(accountFromRes.getAccountNo());
		transactionUpdateRequest.setType(TRX_TYPE.DEBET.toString());
		transactionUpdateRequest.setCurrency(holdingBalance.getDebetCurrency());
		transactionUpdateRequest.setAmount(holdingBalance.getDebetAmount().multiply(new BigDecimal(100)).toString());
		transactionUpdateRequest.setDescription(holdingBalance.getDescription()+" TO "+accountToRes.getName().toUpperCase());
		transactionUpdateRequest.setReferenceID(holdingBalance.getRefnum());
		transactionUpdateRequest.setMerchantCode(BaseConstant.MERCHANT_CODE_DEFAULT);
		transactionUpdateRequest.setTrxCode(BaseConstant.TRX_CODE_DEFAULT);
		TransactionCorDResponse transactionCorDResponse = this.debetAccount(transactionUpdateRequest);
		if (transactionCorDResponse.getResponseCode().equals(rc)){
			Balance balanceDebet = transactionCorDResponse.getBalance();
			balanceDebet.setStatus(BALANCE_STATUS.POSTED.toString());
			// Credit account source
			transactionUpdateRequest.setCif(accountToRes.getAccountNo());
			transactionUpdateRequest.setType(TRX_TYPE.CREDIT.toString());
			transactionUpdateRequest.setDescription(holdingBalance.getDescription()+" FR "+accountFromRes.getName().toUpperCase());
			transactionCorDResponse = this.creditAccount(transactionUpdateRequest);
			if (transactionCorDResponse.getResponseCode().equals(rc)){
				Balance balanceCredit = transactionCorDResponse.getBalance();
				balanceCredit.setStatus(BALANCE_STATUS.POSTED.toString());
				// save debet and credit balance
				transactionHoldResponse.setBalance(balanceDebet);
				transactionHoldResponse.setBalanceRev(balanceCredit);
			} else {
				rc = transactionCorDResponse.getResponseCode();
				rc = this.mapRCforAccountTo(rc);
				transactionHoldResponse.setResponseCode(rc);
				return transactionHoldResponse;
			}
		} else {
			rc = transactionCorDResponse.getResponseCode();
			transactionHoldResponse.setResponseCode(rc);
			return transactionHoldResponse;
		}
		
		transactionHoldResponse.setHoldingBalance(holdingBalance);
		transactionHoldResponse.setResponseCode(rc);
		return transactionHoldResponse;
	}

	@Override
	public HoldingBalanceInquiryResponse inquiryHoldingBalanceByRefID(
			HoldingBalanceInquiryRequest request) {
		HoldingBalanceInquiryResponse response = new HoldingBalanceInquiryResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		AccountInquiryRequest accountInquiryRequest = new AccountInquiryRequest();
		accountInquiryRequest.setCif(request.getCif());
		AccountInquiryResponse accountInquiryResponse = accountService.inquiryAccount(accountInquiryRequest);
		if (accountInquiryResponse.getResponseCode().equals(rc)){
			HoldingBalance holdingBalance = holdingBalanceService.findbyRefNo(request.getReferenceID(), request.getCif());
			if(holdingBalance == null){
				rc = FrontControllerConstant.RC_TRX_NOT_FOUND;
			}else{
				Transaction trx = new Transaction(
						holdingBalance.getRefnum(),
						holdingBalance.getStatus(),
						holdingBalance.getDebetAmount().toString(), 
						holdingBalance.getCreateDate(),
						holdingBalance.getDescription()
				);
				response.setTransaction(trx);
			}
		}else{
			rc = accountInquiryResponse.getResponseCode();
		}
		response.setCif(request.getCif());
		response.setReferenceID(request.getReferenceID());
		response.setResponseCode(rc);
		return response;
	}
}

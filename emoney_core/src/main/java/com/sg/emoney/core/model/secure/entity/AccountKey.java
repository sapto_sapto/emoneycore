package com.sg.emoney.core.model.secure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.sg.emoney.core.model.core.entity.BaseEntity;

@Entity
public class AccountKey extends BaseEntity{
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	private String id;
	
	@Column
	private String exponentPrivate;
	@Column
	private String modulusPrivate;
	@Column
	private String account;
	
	public AccountKey() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AccountKey(String account, String modulusPrivate, 
			String exponentPrivate) {
		super();
		this.account = account;
		this.exponentPrivate = exponentPrivate;
		this.modulusPrivate = modulusPrivate;
		
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getExponentPrivate() {
		return exponentPrivate;
	}
	public void setExponentPrivate(String exponentPrivate) {
		this.exponentPrivate = exponentPrivate;
	}
	public String getModulusPrivate() {
		return modulusPrivate;
	}
	public void setModulusPrivate(String modulusPrivate) {
		this.modulusPrivate = modulusPrivate;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

	
}

package com.sg.emoney.core.exception;

public class LimitCreditException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LimitCreditException() {
		super("Monthly limit credit exceeded");
	}
	
	public LimitCreditException(String message) {
		super(message);
	}
	
}

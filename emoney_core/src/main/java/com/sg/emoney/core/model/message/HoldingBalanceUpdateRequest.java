package com.sg.emoney.core.model.message;


public class HoldingBalanceUpdateRequest extends TransactionUpdateRequest {

	/**
system
	 */
	private static final long serialVersionUID = -388304712671429228L;
	
	private String purposeId;
	
	public enum TRXHOLD_TYPE{HOLD, EXECUTE}

	public String getPurposeId() {
		return purposeId;
	}

	public void setPurposeId(String purposeId) {
		this.purposeId = purposeId;
	}
	
	
}

package com.sg.emoney.core.model.core.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sg.emoney.core.tools.Util;

@Entity
public class Account extends BaseEntity{
	
	
	@Id
	private String accountNo;
	@Column
	private BigDecimal balance;
	@Column
	private String currency;
	@Column
	private String phoneNo;
	@Column
	private String level;
	
	/**
	 * ACTIVE,BLOCKED,REGISTERD,CLOSED
	 */
	@Column
	private String status;
	
	public static enum ACCOUNT_STATUS{ACTIVE,BLOCKED,CLOSED};
	
	@Column
	private String email;
	/**
	 * Birth of date
	 */
	@Column
	private String bod;
	@JsonIgnore
	@Transient
	private Date birthOfDate;
	@Column
	private String sex;
	@Column
	private String name;
	@Column
	private String maritalStatus;
	@Column
	private String job;
	@Column
	private String religion;
	@Column
	private String identityNo;
	@Column
	private String identityType;
	@Column
	private String motherMaidenName ;
	@Column
	private String address;
	@Column
	private String city;
	@Column
	private String province;
	@Column
	private String country;
	@Column
	private String postalCode;
	@Column(length=2000000)
	@Lob @Basic(fetch=FetchType.LAZY)
	private byte[] photo;
	@Column(length=2000000)
	@Lob @Basic(fetch=FetchType.LAZY)
	private byte[] photoCardID;
	@Column(length=2000000)
	@Lob @Basic(fetch=FetchType.LAZY)
	private byte[] signature; 
	
	
//	@Transient
//	private Money money;

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

//	 public Money getMoney() {
//         if (money == null) {
//             money = Money.of(CurrencyUnit.of(currency), balance);
//         }
//
//         return money;
//     }
//
//	public void setMoney(Money money) {
//		this.money = money;
//	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBod() {
		return bod;
	}

	public void setBod(String bod) {
		this.bod = bod;
	}

	public Date getBirthOfDate() {
		birthOfDate = Util.formatDate(bod);
		return birthOfDate;
	}

	public void setBirthOfDate(Date birthOfDate) {
		this.birthOfDate = birthOfDate;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getMotherMaidenName() {
		return motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public byte[] getPhotoCardID() {
		return photoCardID;
	}

	public void setPhotoCardID(byte[] photoCardID) {
		this.photoCardID = photoCardID;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public enum ACCOUNT_LEVEL{BASIC, REGULAR, CORPORATE, PLATINUM};

}

package com.sg.emoney.core.model.message;

import java.util.List;

import com.sg.emoney.core.model.core.entity.Balance;
import com.sg.emoney.core.model.core.entity.HoldingBalance;

public class TransactionHoldResponse extends RestResponse {

	/**
system
	 */
	private static final long serialVersionUID = -685510797875979640L;

	private String accountName;
	
	private HoldingBalance holdingBalance;
	
	private List<HoldingBalance> holdingBalances;
	
	private Balance balance;
	
	private Balance balanceRev;

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public HoldingBalance getHoldingBalance() {
		return holdingBalance;
	}

	public void setHoldingBalance(HoldingBalance holdingBalance) {
		this.holdingBalance = holdingBalance;
	}

	public List<HoldingBalance> getHoldingBalances() {
		return holdingBalances;
	}

	public void setHoldingBalances(List<HoldingBalance> holdingBalances) {
		this.holdingBalances = holdingBalances;
	}

	public Balance getBalance() {
		return balance;
	}

	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	public Balance getBalanceRev() {
		return balanceRev;
	}

	public void setBalanceRev(Balance balanceRev) {
		this.balanceRev = balanceRev;
	}

	
}

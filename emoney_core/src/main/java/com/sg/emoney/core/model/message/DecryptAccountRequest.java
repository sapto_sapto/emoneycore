package com.sg.emoney.core.model.message;

import com.sg.emoney.core.model.core.entity.Account;

public class DecryptAccountRequest extends RestRequest {

	/**
system
	 */
	private static final long serialVersionUID = -4267312726050565867L;
	Account account;
	
	public DecryptAccountRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DecryptAccountRequest(Account account) {
		super();
		this.account = account;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	
}

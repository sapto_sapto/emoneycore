package com.sg.emoney.core.model.message;

public class HoldingBalanceInquiryResponse extends RestResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Transaction transaction;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	
}

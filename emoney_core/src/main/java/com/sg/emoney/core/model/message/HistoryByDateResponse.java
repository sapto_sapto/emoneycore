package com.sg.emoney.core.model.message;

import java.util.List;

public class HistoryByDateResponse extends RestResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Transaction> transactions;

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	
}

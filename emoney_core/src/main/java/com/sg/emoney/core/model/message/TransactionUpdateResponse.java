package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class TransactionUpdateResponse extends RestResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountName;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
}

package com.sg.emoney.core.endpoint;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.exception.LimitCreditException;
import com.sg.emoney.core.exception.TransactionException;
import com.sg.emoney.core.model.message.AccountInquiryRequest;
import com.sg.emoney.core.model.message.AccountInquiryResponse;
import com.sg.emoney.core.model.message.AccountUpdateRequest;
import com.sg.emoney.core.model.message.AccountUpdateResponse;
import com.sg.emoney.core.model.message.BalanceRequest;
import com.sg.emoney.core.model.message.BalanceResponse;
import com.sg.emoney.core.model.message.CancelHoldingBalanceRequest;
import com.sg.emoney.core.model.message.CancelHoldingBalanceResponse;
import com.sg.emoney.core.model.message.CorporateRegistrationRequest;
import com.sg.emoney.core.model.message.EmailUpdateRequest;
import com.sg.emoney.core.model.message.EmailUpdateResponse;
import com.sg.emoney.core.model.message.HistoryByDateRequest;
import com.sg.emoney.core.model.message.HistoryByDateResponse;
import com.sg.emoney.core.model.message.HistoryRequest;
import com.sg.emoney.core.model.message.HistoryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryByDateRequest;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryByDateResponse;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryRequest;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceInquiryRequest;
import com.sg.emoney.core.model.message.HoldingBalanceInquiryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseRequest;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseResponse;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseTopUpRequest;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseTopUpResponse;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateRequest;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateResponse;
import com.sg.emoney.core.model.message.LevelUpdateRequest;
import com.sg.emoney.core.model.message.LevelUpdateResponse;
import com.sg.emoney.core.model.message.PhoneNoUpdateRequest;
import com.sg.emoney.core.model.message.PhoneNoUpdateResponse;
import com.sg.emoney.core.model.message.RegistrationRequest;
import com.sg.emoney.core.model.message.RegistrationResponse;
import com.sg.emoney.core.model.message.RestRequest;
import com.sg.emoney.core.model.message.RestResponse;
import com.sg.emoney.core.model.message.ReversalHoldingBalanceRequest;
import com.sg.emoney.core.model.message.ReversalHoldingBalanceResponse;
import com.sg.emoney.core.model.message.ReversalTransactionRequest;
import com.sg.emoney.core.model.message.ReversalTransactionResponse;
import com.sg.emoney.core.model.message.TransactionUpdateRequest;
import com.sg.emoney.core.model.message.TransactionUpdateResponse;
import com.sg.emoney.core.model.message.TransferOnUsRequestExec;
import com.sg.emoney.core.model.message.TransferOnUsRequestInq;
import com.sg.emoney.core.model.message.TransferOnUsResponseExec;
import com.sg.emoney.core.model.message.TransferOnUsResponseInq;
import com.sg.emoney.core.model.message.TransferOtherRequestExec;
import com.sg.emoney.core.model.message.TransferOtherResponseExec;
import com.sg.emoney.core.service.AccountService;
import com.sg.emoney.core.service.TransactionService;

@RestController
@RequestMapping("/api/")
public class VASEndpoint extends BaseController{
	
	//TO DO Exception Handler
	
	/**
system
	 * for handle request use several method below:
	 * 1. POST for CREATE and READ
	 * 2. PUT for UPDATE
	 */

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionService transactionService;

	/**
	 * 
	 * @param balanceRequest
	 * @return
	 */
	@RequestMapping(value="/VAInquiryBalance", method = RequestMethod.POST)
	@ResponseBody
	public BalanceResponse inquiryBalance(
			@RequestBody BalanceRequest balanceRequest) {
		return transactionService.inquiryBalance(balanceRequest);
	};

	/**
	 * History based on count
	 */
	@RequestMapping(value="/VAInquiryHistory", method = RequestMethod.POST)
	@ResponseBody
	public HistoryResponse inquiryHistory(
			@RequestBody HistoryRequest historyRequest) {
		return transactionService.inquiryHistory(historyRequest);
	};

	/**
	 * History based on date
	 */
	@RequestMapping(value="/VAInquiryHistoryByDate", method = RequestMethod.POST)
	@ResponseBody
	public HistoryByDateResponse inquiryHistoryByDate(
			@RequestBody HistoryByDateRequest historyByDateRequest) {
		return transactionService.inquiryHistoryByDate(historyByDateRequest);
	};

	/**
	 * update status rekening
	 */
	@RequestMapping(value="/VAUpdateStatus", method = RequestMethod.PUT)
	@ResponseBody
	public AccountUpdateResponse updateStatus(
			@RequestBody AccountUpdateRequest accountUpdateRequest) {
		return accountService.updateStatusAccount(accountUpdateRequest);
	};

	/**
	 * D/C each transaction
	 * @throws LimitCreditException 
	 */
	@RequestMapping(value="/VAUpdateTransaction", method = RequestMethod.POST)
	@ResponseBody
	public TransactionUpdateResponse updateTransaction(HttpServletRequest request,
			@RequestBody TransactionUpdateRequest transactionUpdateRequest) throws LimitCreditException {
		request.setAttribute(BaseConstant.REQUEST_CLASS, TransactionUpdateRequest.class);
		return transactionService.updateTransaction(transactionUpdateRequest);
	};
	
	/**
	 * Corporate Registration
	 * @param registrationRequest
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	@RequestMapping(value="/VACorporateRegistration", method = RequestMethod.POST)
	@ResponseBody
	public RegistrationResponse corporateRegistration(
			@RequestBody CorporateRegistrationRequest registrationRequest) throws NoSuchAlgorithmException {
		return accountService.corporateRegister(registrationRequest);
	};
	
	/**
	 * Customer Registration
	 * @param registrationRequest
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	@RequestMapping(value="/VARegistration", method = RequestMethod.POST)
	@ResponseBody
	public RegistrationResponse registration(
			@RequestBody RegistrationRequest registrationRequest) throws NoSuchAlgorithmException {
		return accountService.register(registrationRequest);
	};
	
	/**
	 * 
	 * @param levelUpdateRequest
	 * @return
	 */
	@RequestMapping(value="/VAUpdateLevel", method = RequestMethod.PUT)
	@ResponseBody
	public LevelUpdateResponse updateLevel(
			@RequestBody LevelUpdateRequest levelUpdateRequest) {
		return accountService.updateLevel(levelUpdateRequest);
	}

	/**
	 * 
	 * @param phoneNoUpdateRequest
	 * @return
	 */
	@RequestMapping(value="/VAUpdatePhoneNo", method = RequestMethod.PUT)
	@ResponseBody
	public PhoneNoUpdateResponse updatePhoneNo(
			@RequestBody PhoneNoUpdateRequest phoneNoUpdateRequest) {
		return accountService.updatePhoneNo(phoneNoUpdateRequest);
	}

	/**
	 * 
	 * @param emailUpdateRequest
	 * @return
	 */
	@RequestMapping(value="/VAUpdateEmail", method = RequestMethod.PUT)
	@ResponseBody
	public EmailUpdateResponse updateEmail(
			@RequestBody EmailUpdateRequest emailUpdateRequest) {
		return accountService.updateEmail(emailUpdateRequest);
	}

	/**
	 * 
	 * @param accountInquiryRequest
	 * @return
	 */
	@RequestMapping(value="/VAAccountInquiry", method = RequestMethod.POST)
	@ResponseBody
	public AccountInquiryResponse inquiryAccount(
			@RequestBody AccountInquiryRequest accountInquiryRequest) {
		return accountService.inquiryAccountFull(accountInquiryRequest);
	}
	
	/**
	 * transfer on us inquiry
	 */
	@RequestMapping(value="/VATransferOnUsInquiry", method = RequestMethod.POST)
	@ResponseBody
	public TransferOnUsResponseInq transferOnUsInquiry(
			@RequestBody TransferOnUsRequestInq transferOnUsRequestInq) {
		return transactionService.transferOnUsInquiry(transferOnUsRequestInq);
	};
	
	/**
	 * transfer on us execute
	 * @throws LimitCreditException 
	 * @throws TransactionException 
	 */
	@RequestMapping(value="/VATransferOnUsExecute", method = RequestMethod.POST)
	@ResponseBody
	public TransferOnUsResponseExec transferOnUsExecute(HttpServletRequest request,
			@RequestBody TransferOnUsRequestExec transferOnUsRequestExec) throws LimitCreditException, TransactionException {
		request.setAttribute(BaseConstant.REQUEST_CLASS, TransferOnUsRequestExec.class);
		return transactionService.transferOnUsExecute(transferOnUsRequestExec);
	};
	
	/**
	 * transfer other execute
	 * @throws LimitCreditException 
	 * @throws TransactionException 
	 */
	@RequestMapping(value="/VATransferOtherExecute", method = RequestMethod.POST)
	@ResponseBody
	public TransferOtherResponseExec transferOtherExecute(HttpServletRequest request,
			@RequestBody TransferOtherRequestExec transferOtherRequestExec) throws LimitCreditException, TransactionException, Exception {
		request.setAttribute(BaseConstant.REQUEST_CLASS, TransferOtherRequestExec.class);
		return transactionService.transferOtherExecute(transferOtherRequestExec);
	};
	
	/**
	 * reversal
	 * @throws Exception 
	 */
	@RequestMapping(value="/VAReversalTransaction", method = RequestMethod.POST)
	@ResponseBody
	public ReversalTransactionResponse reversalTransaction(
			@RequestBody ReversalTransactionRequest reversalTransactionRequest) throws Exception {
		return transactionService.reversalTransaction(reversalTransactionRequest);
	};
	
	/**
	 * Holding Balance
	 */
	@RequestMapping(value="/VAUpdateHoldingBalance", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceUpdateResponse updateHoldingBalance(
			@RequestBody HoldingBalanceUpdateRequest holdingBalanceUpdateRequest) {
		return transactionService.updateHoldingBalance(holdingBalanceUpdateRequest);
	};
	
	/**
	 * Release Holding Balance
	 */
	@RequestMapping(value="/VAReleaseHoldingBalance", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceReleaseResponse releaseHoldingBalance(
			@RequestBody HoldingBalanceReleaseRequest holdingBalanceReleaseRequest) {
		return transactionService.releaseHoldingBalance(holdingBalanceReleaseRequest);
	};
	
	/**
	 * Release Withdraw Holding Balance transfer
	 */
	@RequestMapping(value="/VAReleaseHoldingBalanceTrf", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceReleaseResponse releaseHoldingBalanceTrf(
			@RequestBody HoldingBalanceReleaseRequest holdingBalanceReleaseRequest) {
		return transactionService.releaseHoldingBalanceTrf(holdingBalanceReleaseRequest);
	};
	
	/**
	 * Release Topup Holding Balance transfer
	 * @throws LimitCreditException 
	 */
	@RequestMapping(value="/VAReleaseHoldingBalanceTrfTopUp", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceReleaseTopUpResponse releaseHoldingBalanceTrfTopUp(
			@RequestBody HoldingBalanceReleaseTopUpRequest holdingBalanceReleaseRequest) throws LimitCreditException {
		return transactionService.releaseHoldingBalanceTrfTopUp(holdingBalanceReleaseRequest);
	};
	
	/**
	 * cancel holding balance
	 * @throws Exception 
	 */
	@RequestMapping(value="/VACancelHoldingBalance", method = RequestMethod.POST)
	@ResponseBody
	public CancelHoldingBalanceResponse cancelHoldingBalance(
			@RequestBody CancelHoldingBalanceRequest cancelHoldingBalanceRequest) throws Exception {
		return transactionService.cancelHoldingBalance(cancelHoldingBalanceRequest);
	};
	
	/**
	 * reversal holding balance
	 * @throws Exception 
	 */
	@RequestMapping(value="/VAReversalHoldingBalance", method = RequestMethod.POST)
	@ResponseBody
	public ReversalHoldingBalanceResponse reversalHoldingBalance(
			@RequestBody ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) throws Exception {
		return transactionService.reversalHoldingBalance(reversalHoldingBalanceRequest);
	};
	
	/**
	 * reversal holding balance transfer
	 * @throws Exception 
	 */
	@RequestMapping(value="/VAReversalHoldingBalanceTrf", method = RequestMethod.POST)
	@ResponseBody
	public ReversalHoldingBalanceResponse reversalHoldingBalanceTrf(
			@RequestBody ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) throws Exception {
		return transactionService.reversalHoldingBalanceTrf(reversalHoldingBalanceRequest);
	};
	
	/**
	 * Holding Balance History based on count
	 */
	@RequestMapping(value="/VAInquiryHoldingBalanceHistory", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceHistoryResponse inquiryHoldingBalanceHistory(
			@RequestBody HoldingBalanceHistoryRequest historyRequest) {
		return transactionService.inquiryHoldingBalanceHistory(historyRequest);
	};

	/**
	 * Holding Balance History based on date
	 */
	@RequestMapping(value="/VAInquiryHoldingBalanceHistoryByDate", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceHistoryByDateResponse inquiryHoldingBalanceHistoryByDate(
			@RequestBody HoldingBalanceHistoryByDateRequest historyByDateRequest) {
		return transactionService.inquiryHoldingBalanceHistoryByDate(historyByDateRequest);
	};
	
	/**
	 * Holding Balance Inquiry based on refID
	 */
	@RequestMapping(value="/VAInquiryHoldingBalanceByRefID", method = RequestMethod.POST)
	@ResponseBody
	public HoldingBalanceInquiryResponse inquiryHoldingBalanceByRefID(@RequestBody HoldingBalanceInquiryRequest balanceInquiryRequest){
		return transactionService.inquiryHoldingBalanceByRefID(balanceInquiryRequest);
	}
	
	/**
	 * Migrate Data
	 */
	@RequestMapping(value="/VAMigrateData", method = RequestMethod.POST)
	@ResponseBody
	public RestResponse migrateData(@RequestBody RestRequest request){
		return accountService.migrateData(request);
	}
}

package com.sg.emoney.core.model.message;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class RestRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	protected String cif;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	protected String referenceID;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String trxCode;
	
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	public String getTrxCode() {
		return trxCode;
	}
	public void setTrxCode(String trxCode) {
		this.trxCode = trxCode;
	}
	
	
}

package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;


public class LevelUpdateRequest extends DetailRegistrationRequest {

	/**
system
	 */
	private static final long serialVersionUID = 562700584303148273L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String level;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String birthOfDate;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String sex;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String name;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String email;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String phoneNo;
	
	public enum ACCOUNT_LEVEL{BASIC, REGULAR, PLATIINUM};

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getBirthOfDate() {
		return birthOfDate;
	}

	public void setBirthOfDate(String birthOfDate) {
		this.birthOfDate = birthOfDate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	
}

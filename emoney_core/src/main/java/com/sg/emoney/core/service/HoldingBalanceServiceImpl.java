package com.sg.emoney.core.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.model.BalanceSpesifications;
import com.sg.emoney.core.model.core.entity.Account;
import com.sg.emoney.core.model.core.entity.HoldingBalance;
import com.sg.emoney.core.model.core.repository.HoldingBalanceRepository;

@Service
public class HoldingBalanceServiceImpl implements HoldingBalanceService {
	@Autowired
	private HoldingBalanceRepository holdingBalanceRepository;
	
	@Autowired
	private AccountService accountService;
	
	@Override
	public BigDecimal getSumHoldingBalance(String accountNo) {
		BigDecimal sumHoldBalance = holdingBalanceRepository.getSumHoldingBalance(accountNo);
		if (sumHoldBalance == null){
			sumHoldBalance = new BigDecimal(0);
		}
		return sumHoldBalance;
	}

	@Transactional(isolation= Isolation.SERIALIZABLE)
	@Override
	public void save(HoldingBalance holdingBalance) {
		holdingBalanceRepository.save(holdingBalance);
	}

	@Transactional(readOnly = false)
	@Override
	public void delete(HoldingBalance holdingBalance) {
		holdingBalanceRepository.delete(holdingBalance);
	}

	@Override
	public HoldingBalance findbyRefNo(String referenceID, String accountNo) {
		return holdingBalanceRepository.findByRefNo(referenceID, accountNo);
	}

	@Override
	public BigDecimal getSumHoldingBalance(String accountNo, String purposeId) {
		BigDecimal sumHoldBalance = holdingBalanceRepository.getSumHoldingBalance(accountNo, purposeId);
		if (sumHoldBalance == null){
			sumHoldBalance = new BigDecimal(0);
		}
		return sumHoldBalance;
	}

	@Override
	public List<HoldingBalance> findAll(String accountNo, String purposeId) {
		return holdingBalanceRepository.findAll(accountNo, purposeId);
	}

	@Transactional(isolation= Isolation.SERIALIZABLE)
	@Override
	public void saveBatch(Iterable<HoldingBalance> saveHoldBalances) {
		holdingBalanceRepository.save(saveHoldBalances);
	}

	@Override
	public List<HoldingBalance> findAll(String accountNo, String purposeId,
			String status) {
		return holdingBalanceRepository.findAll(accountNo, purposeId, status);
	}

	@Override
	public List<HoldingBalance> findByAccountWithPaging(int qty, int order,
			String accountNo, String purposeId) {
		final Account account = accountService.findByAccountNo(accountNo);
		Page<HoldingBalance> balancesPage = holdingBalanceRepository.findAll(BalanceSpesifications.holdingBalanceByAccountSpec(account, purposeId),
				BalanceSpesifications.pageBalance(order, qty));
		return  balancesPage.getContent();
	}

	@Override
	public List<HoldingBalance> findByAccountWithPaging(int qty, int order,
			Date dateFrom, Date dateTo, String accountNo, String purposeId) {
		final Account account = accountService.findByAccountNo(accountNo);
		Page<HoldingBalance> balancesPage = holdingBalanceRepository.findAll(BalanceSpesifications.holdingBalanceBySubmitDate(dateFrom, dateTo, account, purposeId),
				BalanceSpesifications.pageBalance(order, qty));
		return  balancesPage.getContent();
	}

}

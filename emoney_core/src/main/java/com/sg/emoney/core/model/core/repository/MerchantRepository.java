package com.sg.emoney.core.model.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.Merchant;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, String> {

}

package com.sg.emoney.core.service;

import com.sg.emoney.core.model.message.AddMerchantRequest;
import com.sg.emoney.core.model.message.AddMerchantResponse;
import com.sg.emoney.core.model.message.DeleteMerchantRequest;
import com.sg.emoney.core.model.message.DeleteMerchantResponse;

public interface MerchantService {

	AddMerchantResponse saveMerchant(AddMerchantRequest addMerchantRequest);

	DeleteMerchantResponse deleteMerchant(
			DeleteMerchantRequest deleteMerchantRequest);

}

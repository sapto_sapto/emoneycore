package com.sg.emoney.core.model.core.entity;

import java.math.BigDecimal;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel( HoldingBalance.class )
public class HoldingBalance_ extends BaseEntity_{
	public static volatile SingularAttribute<HoldingBalance, String> id;
	public static volatile SingularAttribute<HoldingBalance, BigDecimal> debetAmount;
	public static volatile SingularAttribute<HoldingBalance, String> debetCurrency;
	public static volatile SingularAttribute<HoldingBalance, String> status;
	public static volatile SingularAttribute<HoldingBalance, String> refnum;
	public static volatile SingularAttribute<HoldingBalance, String> description;
	public static volatile SingularAttribute<HoldingBalance, String> purposeId;
	public static volatile SingularAttribute<HoldingBalance, Account> account;
	public static volatile SingularAttribute<HoldingBalance, Merchant> merchant;
	public static volatile SingularAttribute<HoldingBalance, TrxCode> trxCode;
}

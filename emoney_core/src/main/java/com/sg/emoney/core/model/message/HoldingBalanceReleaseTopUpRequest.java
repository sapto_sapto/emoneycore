package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class HoldingBalanceReleaseTopUpRequest extends
		HoldingBalanceReleaseRequest {

	/**
system
	 */
	private static final long serialVersionUID = -8476203260265977615L;

	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String accountTo;

	public String getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}
	
}

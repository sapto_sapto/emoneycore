package com.sg.emoney.core.model.message;

public class HistoryRequest extends RestRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * total data each request (size)
	 */
	private int qty;
	
	/**
	 * order in row (page)
	 */
	private int order;
	

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	
}

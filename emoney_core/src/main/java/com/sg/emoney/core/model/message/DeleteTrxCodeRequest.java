package com.sg.emoney.core.model.message;

import java.util.List;

public class DeleteTrxCodeRequest extends ConfRestRequest {

	/**
system
	 */
	private static final long serialVersionUID = -2463791758427498335L;

	private List<String> codes;

	public List<String> getCodes() {
		return codes;
	}

	public void setCodes(List<String> codes) {
		this.codes = codes;
	}
}

package com.sg.emoney.core.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.sg.emoney.core.model.core.entity.Balance;

public interface BalanceService {

	void save(Balance balance);

	List<Balance> findByAccountWithPaging(int qty, int order, String cif);

	List<Balance> findByAccountWithPaging(int qty, int order, Date dateFrom,
			Date dateTo, String cif);

	List<Balance> findAllbyRefNo(String referenceID);

	void saveBatch(List<Balance> blncIterable);

	boolean isMonthlyLimitCreditExceeded(String accountNo, String level, BigDecimal amount);
}

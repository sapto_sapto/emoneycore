package com.sg.emoney.core.model;

public class ErrorField {
	private String fieldName;
	private String message;
	private Boolean global;
	
	public ErrorField(String fieldName, String message) {
		super();
		this.fieldName = fieldName;
		this.message = message;
		this.global = false;
	}
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldname) {
		this.fieldName = fieldname;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getGlobal() {
		this.global = false;
		return global;
	}

	public void setGlobal(Boolean global) {
		this.global = global;
	}
	
}

package com.sg.emoney.core.service;

import com.sg.emoney.core.model.secure.entity.AccountKey;

public interface AccountPrivateKeyService {

	void save(AccountKey accPrivateKey);

	AccountKey findByAccountNo(String accountNo);

}

package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class TransferOnUsRequestInq extends RestRequest {

	/**
system
	 */
	private static final long serialVersionUID = -1462054580429946031L;
	private String type;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String accountTo;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String amount;
	private String description;
	private String currency;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccountTo() {
		return accountTo;
	}
	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}

package com.sg.emoney.core.model.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class AccountPublicKey extends BaseEntity{

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	private String id;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Account account;
	
	@Column
	private String modulusKey;
	
	@Column
	private String exponentKey;

	public AccountPublicKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountPublicKey(Account account, String modulusKey,
			String exponentKey) {
		super();
		this.account = account;
		this.modulusKey = modulusKey;
		this.exponentKey = exponentKey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getModulusKey() {
		return modulusKey;
	}

	public void setModulusKey(String modulusKey) {
		this.modulusKey = modulusKey;
	}

	public String getExponentKey() {
		return exponentKey;
	}

	public void setExponentKey(String exponentKey) {
		this.exponentKey = exponentKey;
	}
	
	
}

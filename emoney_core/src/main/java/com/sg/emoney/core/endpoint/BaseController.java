package com.sg.emoney.core.endpoint;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sg.emoney.core.constant.FrontControllerConstant;
import com.sg.emoney.core.exception.LimitCreditException;
import com.sg.emoney.core.exception.TransactionException;
import com.sg.emoney.core.model.message.RestResponse;
/**
 * 
system
 *
 */

@Controller
public class BaseController {	
	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditcontroller");
	
	@ExceptionHandler(MethodArgumentNotValidException.class)//(ValidationException.class)
    @ResponseBody
    public RestResponse handleMethodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException error) {
		LOG_CONTROLLER.error("Request: " + request.getRequestURL() + " raised " + error.getMessage());
        RestResponse restResponse = new RestResponse();
        restResponse.setResponseCode(FrontControllerConstant.INVALID_ARGUMENT);
        
        return restResponse;
    }
	
	@ExceptionHandler(Exception.class)
    @ResponseBody
    public RestResponse handleMethodRuntimeException(HttpServletRequest request, Exception error) {
		LOG_CONTROLLER.error("Request: " + request.getRequestURL() + " raised " + error.getMessage());
        RestResponse restResponse = new RestResponse();
        restResponse.setResponseCode(FrontControllerConstant.ERROR_RUNTIME);

        return restResponse;
    }
	
	@ExceptionHandler(LimitCreditException.class)
	@ResponseBody
	public RestResponse handleLimitCreditException(HttpServletRequest request, Exception error){
		LOG_CONTROLLER.error("Request: " + request.getRequestURL() + " raised " + error.getMessage());
        RestResponse restResponse = new RestResponse();
        restResponse.setResponseCode(FrontControllerConstant.RC_LIMIT_CREDIT_EXCEEDED);

        return restResponse;
	}
	
	@ExceptionHandler(TransactionException.class)
	@ResponseBody
	public RestResponse handleTransactionException(HttpServletRequest request, Exception error){
		LOG_CONTROLLER.error("Request: " + request.getRequestURL() + " raised " + error.getMessage());
        RestResponse restResponse = new RestResponse();
        String errMessage[] = error.getMessage().split(":");
        String rc = (errMessage.length > 1)?errMessage[1].trim() : FrontControllerConstant.ERROR_RUNTIME;
        
        restResponse.setResponseCode(rc);

        return restResponse;
	}
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	@ResponseBody
	public String home(){
		return "It Works !!! ";
	}
}

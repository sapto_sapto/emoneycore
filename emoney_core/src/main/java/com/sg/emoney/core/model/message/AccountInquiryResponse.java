package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;


public class AccountInquiryResponse extends RestResponse {

	/**
system
	 */
	private static final long serialVersionUID = 4072598445585565575L;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountNo;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String name;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String email;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String phoneNo;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String bod;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String sex;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String nominal;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String level;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String maritalStatus;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String job;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String religion;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String identityNo;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String identityType;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String motherMaidenName ;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String address;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String city;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String province;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String country;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String postalCode;
	
	private String photo;
	
	private String photoCardID;
	
	private String signature; 

	public AccountInquiryResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountInquiryResponse(String accountNo, String name, String email,
			String phoneNo, String bod, String sex, String nominal, String level) {
		super();
		this.accountNo = accountNo;
		this.name = name;
		this.email = email;
		this.phoneNo = phoneNo;
		this.bod = bod;
		this.sex = sex;
		this.nominal = nominal;
		this.level = level;
	}
	
	

	public AccountInquiryResponse(String accountNo, String name, String email,
			String phoneNo, String bod, String sex, String nominal,
			String level, String maritalStatus, String job, String religion,
			String identityNo, String identityType, String motherMaidenName,
			String address, String city, String province, String country,
			String postalCode, String photo, String photoCardID,
			String signature) {
		super();
		this.accountNo = accountNo;
		this.name = name;
		this.email = email;
		this.phoneNo = phoneNo;
		this.bod = bod;
		this.sex = sex;
		this.nominal = nominal;
		this.level = level;
		this.maritalStatus = maritalStatus;
		this.job = job;
		this.religion = religion;
		this.identityNo = identityNo;
		this.identityType = identityType;
		this.motherMaidenName = motherMaidenName;
		this.address = address;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
		this.photo = photo;
		this.photoCardID = photoCardID;
		this.signature = signature;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhoneNo() {
		return phoneNo;
	}
	
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	public String getBod() {
		return bod;
	}
	
	public void setBod(String bod) {
		this.bod = bod;
	}
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getNominal() {
		return nominal;
	}
	
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getMotherMaidenName() {
		return motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPhotoCardID() {
		return photoCardID;
	}

	public void setPhotoCardID(String photoCardID) {
		this.photoCardID = photoCardID;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	
}

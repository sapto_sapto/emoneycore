package com.sg.emoney.core.model.core.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.Balance;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, String>, JpaSpecificationExecutor<Balance>{

	@Query("SELECT bl FROM Balance bl WHERE bl.refnum = :refnum and bl.status != 'REVERSAL'")
	List<Balance> findByRefNo(@Param(value = "refnum") String refnum);
	
	@Query("SELECT bl FROM Balance bl WHERE bl.refnum = :refnum AND bl.status != 'REVERSAL'"
			+ " AND bl.createDate >= :startDate AND bl.createDate <= :endDate")
	List<Balance> findByRefNo(@Param(value = "refnum") String refnum, 
			@Param(value = "startDate") Date startDate, @Param(value = "endDate") Date endDate);
	
	@Query("SELECT SUM(bl.creditAmount) FROM Balance bl WHERE bl.account.accountNo=:accountNo AND bl.createDate > :month " +
			"AND bl.status != 'REVERSAL' AND bl.type='CREDIT'")
	BigDecimal getSumCreditAmountByAccountNoAndMonth(@Param(value="accountNo")String accountNo, @Param(value="month")Date month);
}

package com.sg.emoney.core.model.message;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class AccountTransferResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountSrc;
	private String accountSrcName;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String accountDest;
	private String accountDestName;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String amount;
	
	public String getAccountSrc() {
		return accountSrc;
	}
	public void setAccountSrc(String accountSrc) {
		this.accountSrc = accountSrc;
	}
	public String getAccountSrcName() {
		return accountSrcName;
	}
	public void setAccountSrcName(String accountSrcName) {
		this.accountSrcName = accountSrcName;
	}
	public String getAccountDest() {
		return accountDest;
	}
	public void setAccountDest(String accountDest) {
		this.accountDest = accountDest;
	}
	public String getAccountDestName() {
		return accountDestName;
	}
	public void setAccountDestName(String accountDestName) {
		this.accountDestName = accountDestName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
}

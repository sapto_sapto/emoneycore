package com.sg.emoney.core.model.message;

public class HoldingBalanceReleaseResponse extends TransactionUpdateResponse {

	/**
system
	 */
	private static final long serialVersionUID = 7066447972962170614L;

	private Transaction transaction;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
}

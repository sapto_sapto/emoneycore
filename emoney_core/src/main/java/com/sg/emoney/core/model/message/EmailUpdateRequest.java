package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class EmailUpdateRequest extends RestRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

	
	
}

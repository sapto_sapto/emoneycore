package com.sg.emoney.core.model.core.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class HoldingBalance extends BaseEntity {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	private String id;

	@Column
	@NotNull
	private BigDecimal debetAmount;
	@Column
	@NotNull
	@NotEmpty
	private String debetCurrency;
	@Column
	@NotNull
	@NotEmpty
	private String refnum;
	@Column
	private String description;
	
	@Column
	@NotNull
	@NotEmpty
	private String purposeId;
	
	public HoldingBalance() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public HoldingBalance(
			BigDecimal debetAmount, String debetCurrency,
			String refnum, String description, Account account, String purposeId, Merchant merchant, TrxCode trxCode, String status) {
		super();
		this.debetAmount = debetAmount;
		this.debetCurrency = debetCurrency;
		this.refnum = refnum;
		this.description = description;
		this.account = account;
		this.purposeId = purposeId;
		this.merchant = merchant;
		this.trxCode = trxCode;
		this.status = status;
	}

	@ManyToOne(cascade=CascadeType.ALL)
	private Account account;
	
	@ManyToOne
	@NotNull
	private Merchant merchant;
	
	@ManyToOne
	@NotNull
	private TrxCode trxCode;
	
	@Column
	@NotNull
	@NotEmpty
	private String status;
	
	public static enum BALANCEHOLD_STATUS{CANCELED, HOLD, RELEASED};

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getDebetAmount() {
		return debetAmount;
	}

	public void setDebetAmount(BigDecimal debetAmount) {
		this.debetAmount = debetAmount;
	}

	public String getDebetCurrency() {
		return debetCurrency;
	}

	public void setDebetCurrency(String debetCurrency) {
		this.debetCurrency = debetCurrency;
	}

	public String getRefnum() {
		return refnum;
	}

	public void setRefnum(String refnum) {
		this.refnum = refnum;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getPurposeId() {
		return purposeId;
	}

	public void setPurposeId(String purposeId) {
		this.purposeId = purposeId;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public TrxCode getTrxCode() {
		return trxCode;
	}

	public void setTrxCode(TrxCode trxCode) {
		this.trxCode = trxCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

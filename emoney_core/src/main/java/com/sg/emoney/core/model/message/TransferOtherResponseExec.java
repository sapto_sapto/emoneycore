package com.sg.emoney.core.model.message;

import java.util.Map;

public class TransferOtherResponseExec extends RestResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, String> accountResponses;

	public Map<String, String> getAccountResponses() {
		return accountResponses;
	}

	public void setAccountResponses(
			Map<String, String> accountResponses) {
		this.accountResponses = accountResponses;
	}

}

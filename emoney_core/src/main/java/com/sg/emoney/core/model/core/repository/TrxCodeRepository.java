package com.sg.emoney.core.model.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.TrxCode;

@Repository
public interface TrxCodeRepository extends JpaRepository<TrxCode, String> {

}

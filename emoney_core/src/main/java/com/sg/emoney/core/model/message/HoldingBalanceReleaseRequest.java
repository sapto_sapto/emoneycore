package com.sg.emoney.core.model.message;


public class HoldingBalanceReleaseRequest extends RestRequest {

	/**
system
	 */
	private static final long serialVersionUID = 2345568183776392059L;
	
	private String purposeId;

	public String getPurposeId() {
		return purposeId;
	}

	public void setPurposeId(String purposeId) {
		this.purposeId = purposeId;
	}
	
}

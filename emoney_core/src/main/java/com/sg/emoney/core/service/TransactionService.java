package com.sg.emoney.core.service;

import com.sg.emoney.core.exception.LimitCreditException;
import com.sg.emoney.core.exception.TransactionException;
import com.sg.emoney.core.model.message.BalanceRequest;
import com.sg.emoney.core.model.message.BalanceResponse;
import com.sg.emoney.core.model.message.CancelHoldingBalanceRequest;
import com.sg.emoney.core.model.message.CancelHoldingBalanceResponse;
import com.sg.emoney.core.model.message.HistoryByDateRequest;
import com.sg.emoney.core.model.message.HistoryByDateResponse;
import com.sg.emoney.core.model.message.HistoryRequest;
import com.sg.emoney.core.model.message.HistoryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryByDateRequest;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryByDateResponse;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryRequest;
import com.sg.emoney.core.model.message.HoldingBalanceHistoryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceInquiryRequest;
import com.sg.emoney.core.model.message.HoldingBalanceInquiryResponse;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseRequest;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseResponse;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseTopUpRequest;
import com.sg.emoney.core.model.message.HoldingBalanceReleaseTopUpResponse;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateRequest;
import com.sg.emoney.core.model.message.HoldingBalanceUpdateResponse;
import com.sg.emoney.core.model.message.ReversalHoldingBalanceRequest;
import com.sg.emoney.core.model.message.ReversalHoldingBalanceResponse;
import com.sg.emoney.core.model.message.ReversalTransactionRequest;
import com.sg.emoney.core.model.message.ReversalTransactionResponse;
import com.sg.emoney.core.model.message.TransactionUpdateRequest;
import com.sg.emoney.core.model.message.TransactionUpdateResponse;
import com.sg.emoney.core.model.message.TransferOnUsRequestExec;
import com.sg.emoney.core.model.message.TransferOnUsRequestInq;
import com.sg.emoney.core.model.message.TransferOnUsResponseExec;
import com.sg.emoney.core.model.message.TransferOnUsResponseInq;
import com.sg.emoney.core.model.message.TransferOtherRequestExec;
import com.sg.emoney.core.model.message.TransferOtherResponseExec;

public interface TransactionService {

	BalanceResponse inquiryBalance(BalanceRequest balanceRequest);

	HistoryResponse inquiryHistory(HistoryRequest historyRequest);

	HistoryByDateResponse inquiryHistoryByDate(
			HistoryByDateRequest historyByDateRequest);

	TransactionUpdateResponse updateTransaction(
			TransactionUpdateRequest transactionUpdateRequest) throws LimitCreditException;

	TransferOnUsResponseInq transferOnUsInquiry(
			TransferOnUsRequestInq transferOnUsRequestInq);

	TransferOnUsResponseExec transferOnUsExecute(
			TransferOnUsRequestExec transferOnUsRequestExec) throws LimitCreditException, TransactionException;
	
	TransferOtherResponseExec transferOtherExecute(
			TransferOtherRequestExec transferOnUsRequestExec) throws LimitCreditException, TransactionException, Exception;

	ReversalTransactionResponse reversalTransaction(
			ReversalTransactionRequest reversalTransactionRequest) throws Exception;

	HoldingBalanceUpdateResponse updateHoldingBalance(
			HoldingBalanceUpdateRequest holdingBalanceUpdateRequest);

	CancelHoldingBalanceResponse cancelHoldingBalance(
			CancelHoldingBalanceRequest cancelHoldingBalanceRequest) throws Exception;

	HoldingBalanceReleaseResponse releaseHoldingBalance(
			HoldingBalanceReleaseRequest holdingBalanceReleaseRequest);

	HoldingBalanceReleaseResponse releaseHoldingBalanceTrf(
			HoldingBalanceReleaseRequest holdingBalanceReleaseRequest);

	ReversalHoldingBalanceResponse reversalHoldingBalance(
			ReversalHoldingBalanceRequest reversalHoldingBalanceRequest);

	ReversalHoldingBalanceResponse reversalHoldingBalanceTrf(
			ReversalHoldingBalanceRequest reversalHoldingBalanceRequest) throws LimitCreditException;

	HoldingBalanceHistoryResponse inquiryHoldingBalanceHistory(HoldingBalanceHistoryRequest historyRequest);

	HoldingBalanceHistoryByDateResponse inquiryHoldingBalanceHistoryByDate(
			HoldingBalanceHistoryByDateRequest historyByDateRequest);

	HoldingBalanceReleaseTopUpResponse releaseHoldingBalanceTrfTopUp(
			HoldingBalanceReleaseTopUpRequest holdingBalanceReleaseRequest) throws LimitCreditException;
	
	HoldingBalanceInquiryResponse inquiryHoldingBalanceByRefID(HoldingBalanceInquiryRequest request);
	
}

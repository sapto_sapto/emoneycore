package com.sg.emoney.core.model.core.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.CurrencyRate;

@Repository
public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, String>{

	@Query("SELECT curr.conversionRate FROM CurrencyRate curr WHERE curr.currencyFrom = :currencyFrom and curr.currencyTo = :currencyTo")
	BigDecimal findConvertionRate(@Param("currencyFrom") String currencyFrom, @Param("currencyTo") String currencyTo);

	@Query("SELECT curr.inversionRate FROM CurrencyRate curr WHERE curr.currencyFrom = :currencyFrom and curr.currencyTo = :currencyTo")
	BigDecimal findInversionRateRate(@Param("currencyFrom") String currencyFrom, @Param("currencyTo") String currencyTo);
}

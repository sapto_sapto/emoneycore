package com.sg.emoney.core.model.message;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sg.emoney.core.security.ResponseEncryptionSerializer;

public class Transaction {
	
	/**
	 * REFERENCEID, DEBET/CREDIT, NOMINAL, DATE, DESC ,RC, MESSAGE
	 */
	
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String referenceID;
	private String type;
	@JsonSerialize(using=ResponseEncryptionSerializer.class)
	private String nominal;
	private Date transactionDate;
	private String description;
	
	
	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Transaction(String referenceID, String type, String nominal,
			Date transactionDate, String description) {
		super();
		this.referenceID = referenceID;
		this.type = type;
		this.nominal = nominal;
		this.transactionDate = transactionDate;
		this.description = description;
	}
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	public String getType() {
		return type;
	}
	public void setType(String debetCredit) {
		this.type = debetCredit;
	}
	public String getNominal() {
		return nominal;
	}
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public enum TRX_TYPE { DEBET, CREDIT};
	
}

package com.sg.emoney.core.model.message;

import java.util.List;

public class TransferOtherRequestExec extends TransferOnUsRequestExec {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<AccountTransferRequest> accountTransfers;

	public List<AccountTransferRequest> getAccountTransfers() {
		return accountTransfers;
	}

	public void setAccountTransfers(List<AccountTransferRequest> accountTransfers) {
		this.accountTransfers = accountTransfers;
	}
}

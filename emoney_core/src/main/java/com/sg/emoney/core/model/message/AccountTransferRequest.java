package com.sg.emoney.core.model.message;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class AccountTransferRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String accountSrc;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String accountDest;
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String amount;
	
	public String getAccountSrc() {
		return accountSrc;
	}
	public void setAccountSrc(String accountSrc) {
		this.accountSrc = accountSrc;
	}
	public String getAccountDest() {
		return accountDest;
	}
	public void setAccountDest(String accountDest) {
		this.accountDest = accountDest;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
}

package com.sg.emoney.core.model.core.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Balance extends BaseEntity {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	private String id;

	@Column
	@NotNull
	private BigDecimal oldBalance;
	@Column
	@NotNull
	@NotEmpty
	private String oldCurrency;
	@Column
	@NotNull
	private BigDecimal currentBalance;
	@Column
	@NotEmpty
	@NotNull
	private String currentCurrency;
	@Column
	@NotNull
	private BigDecimal creditAmount;
	@Column
	@NotNull
	@NotEmpty
	private String creditCurrency;
	@Column
	@NotNull
	private BigDecimal debetAmount;
	@Column
	@NotNull
	@NotEmpty
	private String debetCurrency;
	@Column
	@NotNull
	@NotEmpty
	private String status;
	@Column
	@NotNull
	@NotEmpty
	private String refnum;
	@Column
	private String description;
	
	@Column
	@NotNull
	@NotEmpty
	private String type;
	
	@ManyToOne
	@NotNull
	private Merchant merchant;
	
	@ManyToOne
	@NotNull
	private TrxCode trxCode;

	public static enum BALANCE_STATUS{NEW,REVERSAL,POSTED};
	
	public Balance() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Balance(BigDecimal oldBalance, String oldCurrency,
			BigDecimal currentBalance, String currentCurrency,
			BigDecimal creditAmount, String creditCurrency,
			BigDecimal debetAmount, String debetCurrency, String status,
			String refnum, String description, Account account, String type, Merchant merchant, TrxCode trxCode) {
		super();
		this.oldBalance = oldBalance;
		this.oldCurrency = oldCurrency;
		this.currentBalance = currentBalance;
		this.currentCurrency = currentCurrency;
		this.creditAmount = creditAmount;
		this.creditCurrency = creditCurrency;
		this.debetAmount = debetAmount;
		this.debetCurrency = debetCurrency;
		this.status = status;
		this.refnum = refnum;
		this.description = description;
		this.account = account;
		this.type = type;
		this.merchant = merchant;
		this.trxCode = trxCode;
	}



	@ManyToOne(cascade=CascadeType.ALL)
	private Account account;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getOldBalance() {
		return oldBalance;
	}

	public void setOldBalance(BigDecimal oldBalance) {
		this.oldBalance = oldBalance;
	}

	public String getOldCurrency() {
		return oldCurrency;
	}

	public void setOldCurrency(String oldCurrency) {
		this.oldCurrency = oldCurrency;
	}

	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getCurrentCurrency() {
		return currentCurrency;
	}

	public void setCurrentCurrency(String currentCurrency) {
		this.currentCurrency = currentCurrency;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getCreditCurrency() {
		return creditCurrency;
	}

	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}

	public BigDecimal getDebetAmount() {
		return debetAmount;
	}

	public void setDebetAmount(BigDecimal debetAmount) {
		this.debetAmount = debetAmount;
	}

	public String getDebetCurrency() {
		return debetCurrency;
	}

	public void setDebetCurrency(String debetCurrency) {
		this.debetCurrency = debetCurrency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRefnum() {
		return refnum;
	}

	public void setRefnum(String refnum) {
		this.refnum = refnum;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public TrxCode getTrxCode() {
		return trxCode;
	}

	public void setTrxCode(TrxCode trxCode) {
		this.trxCode = trxCode;
	}

	
}

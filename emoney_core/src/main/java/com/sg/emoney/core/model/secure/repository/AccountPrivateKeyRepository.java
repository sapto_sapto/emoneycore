package com.sg.emoney.core.model.secure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.secure.entity.AccountKey;

@Repository(value="accountPrivateKeyRepository")
public interface AccountPrivateKeyRepository extends JpaRepository<AccountKey, String>{

	@Query("SELECT key FROM AccountKey key WHERE key.account = :account")
	AccountKey findByAccountNo(@Param(value = "account") String account);

}

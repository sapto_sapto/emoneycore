package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;

public class AddTrxCodeRequest extends ConfRestRequest {

	/**
system
	 */
	private static final long serialVersionUID = -9219437993783679334L;
	
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String code;
	
	private String name;
	
	private String description;
	
	private String status;

	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String action;
	
	public static enum TRXCODE_ACTION{ADD, EDIT};
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
}

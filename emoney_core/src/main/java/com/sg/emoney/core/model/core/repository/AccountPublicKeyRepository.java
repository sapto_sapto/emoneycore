package com.sg.emoney.core.model.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.AccountPublicKey;

@Repository
public interface AccountPublicKeyRepository extends JpaRepository<AccountPublicKey, String>{

	@Query("SELECT key FROM AccountPublicKey key WHERE key.account.accountNo = :accountNo")
	AccountPublicKey findByAccount(@Param("accountNo") String accountNo);

}

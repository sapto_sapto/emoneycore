package com.sg.emoney.core.model.core.repository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sg.emoney.core.model.core.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, String>{

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public <S extends Account> S save(S entity);
	
	@Query("SELECT ac.name FROM Account ac where ac.accountNo=:accountNo")
	public String getNameByAccountNo(@Param(value="accountNo")String accountNo);
	
	@Query("SELECT ac FROM Account ac where ac.status != 'CLOSED'")
	public List<Account> getAll();
}

package com.sg.emoney.core.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sg.emoney.core.constant.BaseConstant;
import com.sg.emoney.core.constant.FrontControllerConstant;
import com.sg.emoney.core.model.core.entity.TrxCode;
import com.sg.emoney.core.model.core.repository.TrxCodeRepository;
import com.sg.emoney.core.model.message.AddTrxCodeRequest;
import com.sg.emoney.core.model.message.AddTrxCodeResponse;
import com.sg.emoney.core.model.message.DeleteTrxCodeRequest;
import com.sg.emoney.core.model.message.DeleteTrxCodeResponse;
import com.sg.emoney.core.model.message.AddTrxCodeRequest.TRXCODE_ACTION;

@Service
public class TrxCodeServiceImpl implements TrxCodeService {
	@Autowired
	TrxCodeRepository trxCodeRepository;

	@Transactional(readOnly = false)
	@Override
	public AddTrxCodeResponse saveTrxCode(AddTrxCodeRequest addTrxCodeRequest) {
		AddTrxCodeResponse addTrxCodeResponse = new AddTrxCodeResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (addTrxCodeRequest.getChannel().equals(BaseConstant.CHANNEL_BACKOFFICE)){
			if (addTrxCodeRequest.getAction().equals(TRXCODE_ACTION.ADD.toString())){
				if (!isExist(addTrxCodeRequest.getCode())){
					TrxCode trxCode = new TrxCode(
							addTrxCodeRequest.getCode(),
							addTrxCodeRequest.getName(),
							addTrxCodeRequest.getDescription(),
							addTrxCodeRequest.getStatus()
						);
					trxCode.setCreateBy(addTrxCodeRequest.getUsername());
					trxCode.setCreateDate(new Date());
					trxCodeRepository.save(trxCode);
				} else {
					rc = FrontControllerConstant.RC_CODE_EXIST;
				}
			} else if (addTrxCodeRequest.getAction().equals(TRXCODE_ACTION.EDIT.toString())){
				TrxCode trxCode = new TrxCode(
						addTrxCodeRequest.getCode(),
						addTrxCodeRequest.getName(),
						addTrxCodeRequest.getDescription(),
						addTrxCodeRequest.getStatus()
					);
				trxCode.setCreateBy(addTrxCodeRequest.getUsername());
				trxCode.setCreateDate(new Date());
				trxCodeRepository.save(trxCode);
			} else {
				rc = FrontControllerConstant.INVALID_ARGUMENT;
			}
		} else {
			rc = FrontControllerConstant.RC_IGNORED_REQUEST;
		}
		
		addTrxCodeResponse.setUsername(addTrxCodeRequest.getUsername());
		addTrxCodeResponse.setResponseCode(rc);
		return addTrxCodeResponse;
	}

	private boolean isExist(String code) {
		TrxCode trxCode = trxCodeRepository.findOne(code);
		if (trxCode != null){
			return true;
		}
		return false;
	}

	@Transactional(readOnly = false)
	@Override
	public DeleteTrxCodeResponse deleteTrxCode(
			DeleteTrxCodeRequest deleteTrxCodeRequest) {
		DeleteTrxCodeResponse delTrxCodeResponse = new DeleteTrxCodeResponse();
		String rc = BaseConstant.SUCCESS_RESPONSE_CODE;
		if (deleteTrxCodeRequest.getChannel().equals(BaseConstant.CHANNEL_BACKOFFICE)){
			Iterable<String> codes = deleteTrxCodeRequest.getCodes();
			List<TrxCode> trxCodes = trxCodeRepository.findAll(codes);
			Iterable<TrxCode> tIterable = trxCodes;
			trxCodeRepository.delete(tIterable);
		} else {
			rc = FrontControllerConstant.RC_IGNORED_REQUEST;
		}
		
		delTrxCodeResponse.setUsername(deleteTrxCodeRequest.getUsername());
		delTrxCodeResponse.setResponseCode(rc);
		return delTrxCodeResponse;
	}

}

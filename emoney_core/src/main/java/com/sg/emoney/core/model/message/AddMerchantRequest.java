package com.sg.emoney.core.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sg.emoney.core.security.RequestDecryptionDeserializer;


public class AddMerchantRequest extends ConfRestRequest {

	/**
system
	 */
	private static final long serialVersionUID = 3044243812372269504L;
	
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String code;
	
	private String name;
	
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String accountSource;
	
	private String status;
	
	@JsonDeserialize(using=RequestDecryptionDeserializer.class)
	private String action;
	
	public static enum MERCHANT_ACTION{ADD, EDIT};

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountSource() {
		return accountSource;
	}

	public void setAccountSource(String accountSource) {
		this.accountSource = accountSource;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
}
